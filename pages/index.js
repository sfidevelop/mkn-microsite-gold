import React, {useState, useEffect} from "react" 
import { useUser } from 'lib/hooks'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from "next/router";
import * as API from "services/api"
import SplashScreen from "components/splashScreen"
import ErrorScreen from "components/errorScreen"
import Banner from "components/dashboard/banner";
import Script from 'next/script'
import CenterMode from "components/dashboard/newbanner-3";

export default function Index() {
    useUser({ redirect_to: '/logined/dashboard', redirect_if_found: true })
    
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loading_page: true,
            error_page: false
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    
    const getMerchant = async() => {
        let res = "";
        if(window.location.search!=""){
            let queryStr = window.location.search.substring(1);
            let keyValue = queryStr.split("=");
            res = keyValue[1];
        }
        localStorage.setItem("utm_source", res);
        let objLocalState = localState;
        if (localStorage.getItem("merchantSet") != "true") {
            try {
                var response = await API.Get('/merchant', 'code='+process.env.NEXT_PUBLIC_CODE+'&client_code='+process.env.NEXT_PUBLIC_CLIENTKEY);
            }
            catch(error) {
                objLocalState.loading_page = false;
                objLocalState.error_page = true;
                setLocalState({...objLocalState, ...localState});
                return true;
            }

            if(response.code == "000") {
                localStorage.setItem("merchantSet", "true");
                localStorage.setItem("merchantcode", process.env.NEXT_PUBLIC_CODE);
                localStorage.setItem("merchantClientcode", process.env.NEXT_PUBLIC_CLIENTKEY);
                localStorage.setItem("merchantName", response.data.merchant.name);
                localStorage.setItem("merchantLogo", response.data.merchant.logo);

                objLocalState.loading_page = false;
                objLocalState.error_page = false;
                setLocalState({...objLocalState, ...localState});
                return true;
            }
            else {
                objLocalState.loading_page = false;
                objLocalState.error_page = true;
                setLocalState({...objLocalState, ...localState});
                return true;
            }
        }
        else {
            objLocalState.loading_page = false;
            objLocalState.error_page = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }
    };
    useEffect(() => {
        getMerchant();
    }, []);

    return (
        <>
            <Script async src="https://www.googletagmanager.com/gtag/js?id=G-X104MZPFR9"></Script>
            <Script id="google-analytics">
                {`window.dataLayer = window.dataLayer || [];`}
                {`function gtag(){dataLayer.push(arguments);}`}
                {`gtag('js', new Date());`}
                {`gtag('config', 'G-X104MZPFR9');`}
            </Script>
            {localState.loading_page
                ?
                <SplashScreen />
                :
                <>
                    {localState.error_page
                        ?
                        <ErrorScreen />
                        :
                        <div className="sm:container mx-auto p-4">
                            <div className="text-center mt-20">
                                <div className="align-middle">
                                    <div className="mt-1 align-middle">
                                        <Image src="/img/logo_kayya.png"
                                        width="123"
                                        height="36" />
                                    </div>
                                </div>

                                <div className="mt-5">
                                    <CenterMode />
                                </div>
                                <div>
                                    <Image src="/img/support-logo.png"
                                            width="750"
                                            height="220" />
                                </div>
                                <div className="mt-20 flex">
                                    <div className="w-full">
                                        <Link href="login">
                                            <a className="button-secondary block">
                                                Login
                                            </a>
                                        </Link>
                                    </div>
                                    <div className="w-full ml-5">
                                        <Link href="signup">
                                            <a className="button-primary block">
                                                Register
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </>                
            }
        </>
    )
}
