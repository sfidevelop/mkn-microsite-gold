import React, {useState, useEffect} from "react"
import { useUser } from 'lib/hooks'
import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api"
import * as errorCode from "services/error_codes"

export default function Signup() {
    useUser({ redirect_to: '/logined/dashboard', redirect_if_found: true })

    const router = useRouter();
    const [today, setToday] = useState(new Date().toISOString().split('T')[0]);
    const maxDate = new Date();
    maxDate.setFullYear(maxDate.getFullYear() - 17);
    const minDate = new Date();
    minDate.setFullYear(minDate.getFullYear() - 100);
    const [maxDateStr, setMaxDateStr] = useState(maxDate.toISOString().split('T')[0]);
    const [minDateStr, setMinDateStr] = useState(minDate.toISOString().split('T')[0]);
    const initialLocalState=()=>{
        return {
            loading:false, 
            input_email:"", input_password:"", input_fullname:"", input_birthdate:'', input_gender:"",
            handphone:"",
            error_message:"", isError: false,
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const signUp = async(e) => {
        e.preventDefault();
        let objLocalState = localState;
        objLocalState.loading = true;
        localStorage.setItem('signup_email', localState.input_email);
        setLocalState({...objLocalState, ...localState});
        localState.utm_source = localStorage.getItem("utm_source");
        // now the signup page can get the item of utm_source from index.js(the very beginning page).
        // console.log("signup", localState.utm_source);
        
        try {
            var response = await API.Post('/auth/signup', {
                code : process.env.NEXT_PUBLIC_CODE,
                client_code : process.env.NEXT_PUBLIC_CLIENTKEY,
                fullname : localState.input_fullname,
                email : localState.input_email,
                password : localState.input_password,
                gender : localState.input_gender,
                birthdate : localState.input_birthdate,
                utm_source: localState.utm_source,
                handphone:localState.handphone
            });
        }
        catch(error) {            
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = error;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.loading = false;
            objLocalState.isError = false;
            setLocalState({...objLocalState, ...localState});
            router.push({
                pathname: "login"
            });
        }
        else {
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
    }
    const [errorMessage, setErrorMessage] = useState('');
    const [abletoregphone, setAbletoregphone] = useState(false);
    const [abletoregdate, setAbletoregdate] = useState(false);
    const handleChangePhone = (e) => {
        // 在这里可以调用您想要执行的函数或逻辑
        const handphone = e.target.value.replace(/\D/g, '');
        const consecutivePattern = /(.)\1{5,}/; // 匹配连续6位相同的数字
        if (handphone.length < 9 || consecutivePattern.test(handphone)) {
            setErrorMessage('Periksa kembali nomor ponsel Anda');
            setAbletoregphone(false);
        }
        else{
            setErrorMessage('');
            setAbletoregphone(true);
        }
        
        };
    
    // check whether the date is valid, if not, give the error message.
    const [errorMessageDate, setErrorMessageDate] = useState('');
    const handleChangeDate = (e) => {
        if(e.target.value > maxDateStr || e.target.value < minDateStr) {
            setErrorMessageDate('Tanggal Lahir invlaid, masukkan Tanggal yang Benar.');
            setAbletoregdate(false);
        } 
        else {
            setErrorMessageDate('');
            setAbletoregdate(true);
        }

    };
    
   
    

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <Link href="/">
                        <a>
                            <Image src="/img/back.webp" 
                                height="20"
                                width="20"
                                alt="back" />
                        </a>
                    </Link>
                    <div className="text-center">
                        <Image src={process.env.NEXT_PUBLIC_FAVICON}
                            height="21.5"
                            width="20"
                            alt="Nyata" />
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center heading5">
                    Buat Akunmu
                </div>
                <form onSubmit={(e)=>signUp(e)}>
                    <div className="mt-20">
                        <label>Nama Lengkap</label>
                        <input type="text" 
                            placeholder="Cth: Finkie Setiawan"
                            className="border rounded p-3 w-full"
                            autoFocus={true}
                            required={true}
                            value={localState.input_fullname}
                            onInput={(e) => setLocalState({...localState, input_fullname:e.target.value})} />
                    </div>

                    <div className="mt-2">
                        <label>Nomor Ponsel (Whatsapp)</label>
                        <div className="font-bold ml-1 text-red-500">{errorMessage}</div>
                        <div className="grid grid-cols-12 items-center">
                            <span className="font-bold p-1">+62</span>
                            <input type="tel" 
                                        id="phoneNumberInput"
                                        className="border p-3 rounded col-span-11"
                                        placeholder="Cth: 8xxxxxxxx"
                                        autoFocus={true}
                                        required={true}
                                        maxLength={15}
                                        value={localState.handphone}
                                        onInput={(e)=>{setLocalState({...localState, handphone:e.target.value});handleChangePhone(e);}} />
                        </div>
                    </div>

                    <div className="mt-2">
                        <label>Email</label>
                        <input type="email" 
                            placeholder="Cth: nama@email.com"
                            className="border rounded p-3 w-full" 
                            value={localState.input_email}
                            required={true}
                            onInput={(e) => setLocalState({...localState, input_email:e.target.value})} />
                    </div>

                    <div className="mt-2">
                        <label>Password (Minimal 6 karakter)</label>
                        <input type="password" 
                            placeholder="•••••••••••••"
                            className="border rounded p-3 w-full" 
                            value={localState.input_password}
                            required={true}
                            onInput={(e) => setLocalState({...localState, input_password:e.target.value})} />
                    </div>

                    <div className="mt-2">
                        <label>Jenis Kelamin</label>
                        <select className="border rounded p-3 w-full bg-white"
                            value={localState.input_gender}
                            required={true}
                            onChange={(e) => setLocalState({...localState, input_gender:e.target.value})}>
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="male">Laki-laki</option>
                            <option value="female">Perempuan</option>
                        </select>
                    </div>

                    <div className="mt-2">
                        <label>Tanggal Lahir</label>
                        <div className="font-bold ml-1 text-red-500">{errorMessageDate}</div>
                        <input type="date" 
                            placeholder="Masukan Tanggal Lahir"
                            className="border rounded p-3 w-full" 
                            value={localState.input_birthdate}
                            required={true}
                            min={minDateStr}
                            max={maxDateStr}
                            onInput={(e) => {setLocalState({...localState, input_birthdate:e.target.value});handleChangeDate(e);}} />
                    </div>

                    {localState.isError && 
                        <div className="mt-2 bg-banner border p-3 rounded text-gray-500">
                            <FontAwesomeIcon icon={faCircleExclamation}
                                className="mr-1" />
                            {localState.error_message}
                        </div>
                    }

                    <div className="mt-5">
                        {localState.loading
                            ?
                            <button className="button-primary w-full"
                                disabled="disabled">
                                <Image src="/img/loading.png"
                                    alt="loading"
                                    height={12}
                                    width={12}
                                    className="animate-spin" />
                                <span className="ml-2">Loading ...</span>
                            </button>
                            :                
                            <button className="button-primary w-full"
                                disabled={!abletoregphone || !abletoregdate}                        
                                type="submit">
                                Buat Akun
                            </button>                
                        }
                    </div>
                </form>

                <div className="mt-3 text-center">
                    <Link href="/login">
                        <a className="button-link w-full block">
                            Sudah Punya Akun?
                        </a>
                    </Link>
                </div>

                <div className="inline text-center">
                    <p>Powered by KAYYA</p>
                </div>
            </div>
        </>
    )
}
