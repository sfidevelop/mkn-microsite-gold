import React, {useState, useEffect} from "react";
import Link from 'next/link';
import { useRouter } from "next/router";
import { useUser } from 'lib/hooks'
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import Promo from "components/dashboard/promos";
import FooterPage from "components/footerPage";
import ErrorScreen from "components/errorScreen";

export default function Dashboard() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true,
            customer: {
                name: "",
                handphone: ""
            },
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        setLocalState(initialLocalState);

        let objLocalState = localState;
        try {
            var response = await API.Get('/customer', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") {
            objLocalState.customer.fullname = response.data.customer.fullname;
            objLocalState.customer.handphone = response.data.customer.handphone;
        }
        else objLocalState.errorPageShow = true;

        setLocalState({...objLocalState, ...localState});
        return true;
    }
    useEffect(() => {
        getUser();
    }, []);

    return (
        <>
        {localState.errorPageShow 
        ? <ErrorScreen /> 
        : 
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <HeaderPage title="logo" />
                    <div className="sm:container mx-auto p-4 custom-pb-70 custom-pt-70">                        
                        <div className="mt-24">
                            <Promo />
                        </div>

                        <div className="mt-10 mb-5">
                            <Link href="/logined/index">
                                <a className="button-primary w-full block">
                                    Masuk
                                </a>
                            </Link>     
                        </div>
                    </div>

                    <FooterPage selected="home" />
                </>
            }
        </>
        }
            
        </>

    )
}
