import React, {useState, useEffect, useRef} from "react";
import { useUser } from 'lib/hooks'
import Image from 'next/image';
import { useRouter } from 'next/router';
import Resizer from "react-image-file-resizer"
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import ErrorScreen from "components/errorScreen";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api";
import * as errorCode from "services/error_codes"

export default function Profile() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true, loadingProcess: false,
            isError: false, errorMessage:"",
            customer: {
                fullname : "",
                email : "",
                handphone : "",
                birthdate : "",
                gender : "",
                photo : "/img/person.jpg",
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const inputFile = useRef(null);
    const getUser = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.customer.photo = "/img/person.jpg";
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/customer', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.customer.fullname = response.data.customer.fullname;
            objLocalState.customer.email = response.data.customer.email;
            objLocalState.customer.handphone = response.data.customer.handphone;
            objLocalState.customer.birthdate = response.data.customer.birthdate;
            objLocalState.customer.gender = response.data.customer.gender;
            if(response.data.customer.photo != "") objLocalState.customer.photo = response.data.customer.photo;
            objLocalState.customer.handphone = response.data.customer.handphone;
            objLocalState.loadingPage = false;
        }     
        else objLocalState.errorPageShow = true;

        setLocalState({...objLocalState, ...localState});
        return true;
    }
    const showImage = (e) => {
        let objLocalState = localState;
        if(e.target.files[0]) {
            try {
                Resizer.imageFileResizer(
                    e.target.files[0],
                    1000,
                    1000,
                    "JPEG",
                    100,
                    0,
                    (uri) => {
                        objLocalState.customer.photo = uri;
                        setLocalState({...objLocalState, ...localState});
                    },
                    "base64"
                );         
            } 
            catch (err) {
                objLocalState.customer.photo = "/img/person.jpg";
                setLocalState({...objLocalState, ...localState});
            }
        }
        else {
            objLocalState.customer.photo = "/img/person.jpg";
            setLocalState({...objLocalState, ...localState});
        }
    }
    const save = async(e) => {
        e.preventDefault();
        let objLocalState = localState;
        objLocalState.loadingProcess = true;
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/customer/save', {
                code : process.env.NEXT_PUBLIC_CODE,
                client_code : process.env.NEXT_PUBLIC_CLIENTKEY,
                token : localStorage.getItem("logined_token"),
                handphone : localState.customer.handphone,
                birthdate : localState.customer.birthdate,
                gender : localState.customer.gender,
                photo : (localState.customer.photo == "/img/person.jpg" ? "" : localState.customer.photo)
            });
        }
        catch(error) {                
            objLocalState.loadingLogout = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            router.back();
            return true;
        }
        else {
            objLocalState.loadingProcess = false;
            objLocalState.isError = true;
            objLocalState.errorMessage = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
    }
    useEffect(() => {
        getUser();
    }, []);

    return (
        <>
            {localState.errorPageShow && <ErrorScreen />}
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>     
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="20"
                                        width="20"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Profil
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">   
                        <form onSubmit={(e)=>save(e)}>
                            <div className="text-center">
                                <div className="text-center">
                                    <Image src={localState.customer.photo}
                                        height="150"
                                        width="150"
                                        className="rounded-full"
                                        alt="Person" />
                                </div>

                                <div className="absolute w-full left-0">
                                    <input type="file"
                                        className="hidden"
                                        ref={inputFile} 
                                        onChange={(e)=>showImage(e)} />

                                    <button className="bg-gray-10-75 rounded-full mx-auto block flex"
                                        style={{height:150,width:150, marginTop:-156}}
                                        onClick={()=>inputFile.current.click()}
                                        type="button">
                                        <div className="text-center self-center flex-auto">
                                            <Image src="/img/edit.png"
                                                alt="Edit Photo"
                                                height={24}
                                                width={24} />
                                        </div>
                                    </button>
                                </div>
                            </div>

                            <div className="mt-5">
                                <label>Nama Lengkap</label>
                                <input type="text" 
                                    className="border p-3 rounded w-full"
                                    placeholder="Full Name"
                                    value={localState.customer.fullname}
                                    disabled="disabled" />
                            </div>

                            <div className="mt-5">
                                <label>No.Hp</label>
                                <input type="tel" 
                                    className="border p-3 rounded w-full"
                                    placeholder={localState.customer.handphone == "" ? "Cth: 8xxxxxxxx" : localState.customer.handphone}
                                    value={localState.customer.handphone}
                                    onInput={(e)=>setLocalState({...localState, customer:{...localState.customer, handphone:e.target.value}})} />
                            </div>

                            <div className="mt-5">
                                <label>Email</label>
                                <input type="email" 
                                    className="border p-3 rounded w-full"
                                    placeholder="Email"
                                    disabled="disabled"
                                    value={localState.customer.email} />
                            </div>

                            <div className="mt-5">
                                <label>Tanggal Lahir</label>
                                <input type="date" 
                                    className="border p-3 rounded w-full"
                                    placeholder="Birthdate"
                                    value={localState.customer.birthdate}
                                    onInput={(e)=>setLocalState({...localState, customer:{...localState.customer, birthdate:e.target.value}})} />
                            </div>

                            <div className="mt-5">
                                <label>Jenis Kelamin</label>
                                <select className="border p-3 rounded w-full bg-white"
                                    value={localState.customer.gender}
                                    onChange={(e)=>setLocalState({...localState, customer:{...localState.customer, gender:e.target.value}})} >
                                    <option value="">Gender</option>
                                    <option value="male">Laki-laki</option>
                                    <option value="female">Perempuan</option>
                                </select>
                            </div>
                            {localState.isError && 
                                <div className="mt-5 bg-red-7 p-3 rounded text-white">
                                    <FontAwesomeIcon icon={faCircleExclamation}
                                        className="mr-1" />
                                    {localState.errorMessage}
                                </div>
                            }
                            <div className="mt-10">
                                {localState.loadingProcess
                                    ?
                                    <button className="button-primary w-full"
                                        disabled="disabled"
                                        type="button">
                                        <Image src="/img/loading.png"
                                            alt="loading"
                                            height={16}
                                            width={16}
                                            className="animate-spin" />
                                        <span className="ml-3">Loading ...</span>
                                    </button>
                                    :      
                                    <button className="button-primary w-full"
                                        type="submit">
                                        Simpan
                                    </button>
                                }
                            </div>
                        </form>
                    </div>
                </>
            }            
        </>
    )
}
