import React, {useState} from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api";

export default function ResetPassword() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingProcess: false,
            current_password: "",
            new_password: "",
            confirm_password: "",
            isError:false,
            error_message: "",
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const save = async(e) => {
        e.preventDefault();
        let objLocalState = localState;
        objLocalState.loadingProcess = true;
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/account/reset-password', {
                code : process.env.NEXT_PUBLIC_CODE,
                client_code : process.env.NEXT_PUBLIC_CLIENTKEY,
                token : localStorage.getItem("logined_token"),
                current_password : localState.current_password,
                new_password : localState.new_password,
                confirm_password : localState.confirm_password,
            });
        }
        catch(error) {                
            objLocalState.loadingProcess = false;
            objLocalState.isError = true;
            objLocalState.error_message = "Gagal memproses data"; 
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            router.back();
            return true;
        }
        else {
            objLocalState.loadingProcess = false;
            objLocalState.isError = true;
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
    }

    return (
        <>
            <div className="sm:container mx-auto p-4 w-full">
                <div className="flex justify-between items-center">
                    <div>
                        <Link href="/logined/account">
                            <a>
                            <Image src="/img/back.webp" 
                                height="20"
                                width="20"
                                alt="back" />
                            </a>
                        </Link>
                    </div>

                    <div className="subtitle1 text-sm text-center">
                        Reset Password
                    </div>
                    <div>
                        <></>
                    </div>
                </div>

                <form onSubmit={(e)=>save(e)}>
                    <div className="mt-20">
                        <label>Password Lama</label>
                        <div className="mb-2">
                            <input type="password" 
                                placeholder="Password Lama"
                                className="border rounded p-3 w-full"
                                onInput={(e)=>setLocalState({...localState, current_password:e.target.value})}
                                value={localState.current_password}
                                required={true}
                                autoComplete="false" />
                        </div>

                        <label>Password Baru</label>
                        <div className="mb-2">
                            <input type="password" 
                                placeholder="Password Baru"
                                className="border rounded p-3 w-full"
                                onInput={(e)=>setLocalState({...localState, new_password:e.target.value})}
                                value={localState.new_password}
                                required={true}
                                autoComplete="false" />
                        </div>

                        <label>Konfirmasi Password Baru</label>
                        <div className="mb-2">
                            <input type="password" 
                                placeholder="Konfirmasi Password Baru"
                                className="border rounded p-3 w-full"
                                onInput={(e)=>setLocalState({...localState, confirm_password:e.target.value})}
                                value={localState.confirm_password}
                                required={true}
                                autoComplete="false" />
                        </div>
                        {localState.isError && 
                            <div className="mt-2 bg-red-7 p-3 rounded text-white">
                                <FontAwesomeIcon icon={faCircleExclamation}
                                    className="mr-1" />
                                {localState.error_message}
                            </div>
                        }
                        <div className="mt-5">
                            {localState.loadingProcess
                                ?
                                <button className="button-primary w-full"
                                    type="button"
                                    disabled="disabled">
                                    <Image src="/img/loading.png"
                                        alt="loading"
                                        height={16}
                                        width={16}
                                        className="animate-spin" />
                                    <span className="ml-3">Loading ...</span>
                                </button>
                                :      
                                <button className="button-primary w-full"
                                    type="submit">
                                    Submit
                                </button>
                            }
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}
