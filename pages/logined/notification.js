import React, {useState, useEffect} from "react";
import { useUser } from 'lib/hooks'
import Image from 'next/image';
import { useRouter } from "next/router";
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import ErrorScreen from "components/errorScreen";

export default function Notification() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })
    
    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true,
            notifications: [],
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getNotifications = async() => {        
        setLocalState(initialLocalState);

        let objLocalState = localState;
        try {
            var response = await API.Get('/notification', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.notifications = response.data.notifications;
            objLocalState.loadingPage = false;
        }
        else objLocalState.errorPageShow = true;
        setLocalState({...objLocalState, ...localState});
        return true;
    }
    useEffect(() => {
        getNotifications();
    }, []);

    return (
        <>
            {localState.errorPageShow && <ErrorScreen />}
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="20"
                                        width="20"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Notification
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto custom-pt-70">
                        {localState.notifications.length > 0 
                            ?
                            <div className="divide-y">
                                {localState.notifications.map((item, index) => {
                                    return (
                                        <div key={index}
                                            className={"py-3 px-4 " + (item.isread == "0" && "bg-gray-2")}>
                                            <div className="subtitle2">{item.title}</div>
                                            <div>{new Date(item.created_at).toLocaleString()}</div>
                                            <div className="mt-1">{item.content}</div>
                                        </div>
                                    )
                                })}
                            </div>
                            :
                            <div className="text-center mt-10">
                                Tidak ada notifikasi baru
                            </div>
                        }
                    </div>
                </>
            }
        </>
    )
}
