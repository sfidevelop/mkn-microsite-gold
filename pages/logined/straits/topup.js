import React, {useState, useEffect} from "react";
import Image from 'next/image';
import { useRouter } from 'next/router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";

export default function Topup() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getAccount = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.account = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.account = response.data.straits_account;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const copyVABCA = () => {
        navigator.clipboard.writeText(localState.account.bca_va);
        toast("Nomor Virtual Akun berhasil disalin");
    }
    const copyVACIMB = () => {
        navigator.clipboard.writeText(localState.account.cimb_va);
        toast("Nomor Virtual Akun berhasil disalin");
    }
    useEffect(() => {
        getAccount();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <ToastContainer />
                    <div className="fixed bg-white w-full z-10">
                        <div className="sm:container p-4 mx-auto">
                            <div className="grid grid-cols-3 items-center">  
                                <div className="text-left">
                                    <button type="button" 
                                        onClick={() => router.back()}>
                                        <Image src="/img/back.webp" 
                                            height="20"
                                            width="20"
                                            alt="back" />
                                    </button>
                                </div>
                                <div className="text-center subtitle1">
                                    Top Up
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">
                        <div className="color-red">*Deposit minimal Rp500.000, - untuk mulai trading di KAYYA</div>
                        <div className="py-3">
                            <div className="subtitle1 mb-1">BCA</div>
                            <div className="bg-gray-2 p-3 rounded drop-shadow">
                                <div>Nomor Virtual Akun</div>
                                <div>
                                    <span className="mr-1 subtitle2">{localState.account.bca_va}</span>
                                    <button onClick={() => copyVABCA()}>
                                        <Image src="/img/ico-copy.png" 
                                            height="16"
                                            width="16"
                                            alt="copy"
                                            title="Copy Virtual Account" />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="py-3">
                            <div className="subtitle1 mb-1">CIMB Niaga</div>
                            <div className="bg-gray-2 p-3 rounded drop-shadow">
                                <div>Nomor Virtual Akun</div>
                                <div>
                                    <span className="mr-1 subtitle2">{localState.account.cimb_va}</span>
                                    <button onClick={() => copyVACIMB()}>
                                        <Image src="/img/ico-copy.png" 
                                            height="16"
                                            width="16"
                                            alt="copy"
                                            title="Copy Virtual Account" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div  className="KBI">
                         <Image src="/img/diawasioleh.png" width="455%" height="180%" />
                    </div>
                    
                </>
            }
        </>
    )
}
