import React, {useState, useEffect} from "react";
import WithdrawInput from "components/straits/withdraw/input";
import WithdrawConfirmation from "components/straits/withdraw/confirmation";

export default function Withdraw() {
    const initialLocalState = () => {
        return {
            usd: 0,
            page: "withdrawInput"
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const changePage = (page, usd) => {
        setLocalState({
           ...localState,
            page: page,
            usd: usd
        });
    }

    return (
        <>
            {localState.page === "withdrawInput" 
                ? <WithdrawInput changePage={changePage} />
                : <WithdrawConfirmation changePage={changePage} propsState={localState} />
            }
        </>
    )
}
