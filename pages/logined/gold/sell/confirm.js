import React, {useState, useEffect} from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'

export default function SellConfirm() {
    const router = useRouter()
    return (
        <>
            <div className="sm:container mx-auto p-4 custom-pb-86">
                <div className="grid grid-cols-3 items-center">
                    <div>
                        <button type="button" 
                            onClick={() => router.back()}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="font-epilogue font-bold text-xl text-center">
                        Confirmation
                    </div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Weight</label>
                    <div className="text-right">0.1 gr</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Unit Price</label>
                    <div className="text-right">Rp. 800.000/gr</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Sell Amount</label>
                    <div className="text-right">Rp. 80.000</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Service Fee</label>
                    <div className="text-right">Rp.5.000</div>
                </div>

                <hr className="mt-5" />

                <div className="mt-5 grid grid-cols-2">
                    <label>Total Payment</label>
                    <div className="text-right font-medium">Rp.75.000</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Bank</label>
                    <div className="text-right font-medium">BCA</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Bank Account Name</label>
                    <div className="text-right font-medium">Daniel Sebastian</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Bank Account Number</label>
                    <div className="text-right font-medium">5270103130</div>
                </div>

                <div className="mt-10">
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full">
                        Proceed
                    </button>
                </div>
            </div>
        </>
    )
}
