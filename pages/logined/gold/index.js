import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { AdvancedRealTimeChart } from "react-ts-tradingview-widgets";
import TotalGold from "components/gold/totalGold";
import * as API from "services/api";
import LoadingPage from "components/loadingPage";

export default function Gold() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            account: null,
            customer:{},
            ktp:{},
            npwp:{},
            price:{
                buy: 0,
                sell: 0
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getGoldPrice = async() => {
        let objLocalState = localState;
        objLocalState.price = {buy:0, sell:0};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/gold/price', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}`);
        }
        catch(error) {            
            router.back();
            return true;
        }

        if(response.code == "000") {
            objLocalState.price.buy = parseFloat(response.data.price.buy);
            objLocalState.price.sell = parseFloat(response.data.price.sell);
            setLocalState({...objLocalState, ...localState});            
        }
        else router.back();
        return true;
    }
    const getGoldAccount = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.account = null;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/gold', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {            
            router.back();
            return true;
        }

        if(response.code == "000") {
            objLocalState.account = response.data.gold_account;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }
        else {
            router.back();
            return true;
        }
    }
    const getUser = async() => {
        let objLocalState = localState;
        objLocalState.customer = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/customer', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.back();
        }

        if(response.code == "000") {
            objLocalState.customer = response.data.customer;
            setLocalState({...objLocalState, ...localState});            
        }     
        else router.back();   
        return true;
    }
    const getKTP = async() => {
        let objLocalState = localState;
        objLocalState.customer = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/gold/ktp', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.back();
        }

        if(response.code == "000") {
            objLocalState.ktp = response.data.ktp;
            setLocalState({...objLocalState, ...localState});
        }     
        else router.back();   
        return true;
    }
    const getNPWP = async() => {
        let objLocalState = localState;
        objLocalState.customer = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/gold/npwp', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.back();
        }

        if(response.code == "000") {
            objLocalState.npwp = response.data.npwp;
            setLocalState({...objLocalState, ...localState});
        }     
        else router.back();   
        return true;
    }
    useEffect(() => {
        getGoldPrice();
        getGoldAccount();
        getUser();
        getKTP();
        getNPWP();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <div className="sm:container mx-auto p-4 custom-pb-86">
                    <div className="grid grid-cols-3 items-center">                    
                        <Link href="/logined/dashboard"
                            className="self-center">
                            <a>
                                <Image src="/img/icon-home.png" 
                                    height="24"
                                    width="24"
                                    alt="home" />
                            </a>
                        </Link>

                        <div className="font-epilogue font-bold text-xl text-center">
                            Emas
                        </div>
                    </div>

                    <div className="mt-10">
                        {localState.account == null 
                            ?
                            <div className="bg-red-7 p-3 text-white rounded-lg">
                                <div>Please verify your account to access this feature</div>                                
                            </div>
                            : 
                            (localState.ktp.status ?? null) != "verified"
                            ?
                            <div className="bg-red-7 p-3 text-white rounded-lg">
                                <div>Your account is under review</div>                                
                            </div>
                            :
                            <TotalGold gold={localState.account.balance} />
                            
                        }                   
                    </div>
                    
                    {localState.account != null && 
                        <div className="grid grid-cols-2 gap-3 mt-5">
                            <Link href={((localState.ktp.status ?? null) == "verified") ? "gold/buy" : "#"}>
                                <a className="w-full bg-green-7 text-white text-center rounded-lg p-2">
                                    <div className="font-bold">Beli</div>
                                    <div className="text-sm">IDR {(localState.price.buy).toLocaleString()}</div>
                                </a>
                            </Link>

                            <Link href={((localState.ktp.status ?? null) == "verified") ? "gold/sell" : "#"}>
                                <a className="w-full bg-red-7 text-white text-center rounded-lg p-2">
                                    <div className="font-bold">Jual</div>
                                    <div className="text-sm">IDR {(localState.price.sell).toLocaleString()}</div>
                                </a>
                            </Link>
                        </div>
                    }

                    <div className="mt-5">
                        <div className="grid grid-cols-4 gap-3 mt-3">
                            <Link href="account/profile">
                                <a className="text-center">
                                    <div className="relative mx-auto"
                                        style={{width:50}}>
                                        <Image src="/img/ico-document.png"
                                            alt="Profile"
                                            height="50"
                                            width="50" />
                                        {localState.customer.handphone &&
                                            <div className="absolute top-0">
                                                <Image src="/img/ico-check.png"
                                                    alt="Done"
                                                    height="15"
                                                    width="15" />
                                            </div>
                                        }
                                    </div>
                                    <div className="text-xs mt-1">Akun</div>
                                </a>
                            </Link>
                            <Link href="gold/ktp">
                                <a className="text-center">
                                    <div className="relative mx-auto"
                                        style={{width:50}}>
                                        <Image src="/img/ico-document.png"
                                            alt="KTP"
                                            height="50"
                                            width="50" />
                                        {(localState.ktp.status ?? null) == "verified" &&
                                            <div className="absolute top-0">
                                                <Image src="/img/ico-check.png"
                                                    alt="Done"
                                                    height="15"
                                                    width="15" />
                                            </div>
                                        }
                                    </div>
                                    <div className="text-xs mt-1">KTP</div>
                                </a>
                            </Link>
                            <Link href="gold/npwp">
                                <a className="text-center">
                                    <div className="relative mx-auto"
                                        style={{width:50}}>
                                        <Image src="/img/ico-document.png"
                                            alt="NPWP"
                                            height="50"
                                            width="50" />  
                                        {(localState.npwp.status ?? null) == "verified" &&
                                            <div className="absolute top-0">
                                                <Image src="/img/ico-check.png"
                                                    alt="Done"
                                                    height="15"
                                                    width="15" />
                                            </div>
                                        }                                   
                                    </div>
                                    <div className="text-xs mt-1">NPWP</div>
                                </a>
                            </Link>
                            <Link href="gold/mutation">
                                <a className="text-center">
                                    <div className="relative mx-auto"
                                        style={{width:50}}>
                                        <Image src="/img/ico-document.png"
                                            alt="NPWP"
                                            height="50"
                                            width="50" />                                                                    
                                    </div>
                                    <div className="text-xs mt-1">Mutasi</div>
                                </a>
                            </Link>
                            <Link href="#">
                                <a className="text-center">
                                    <div className="relative mx-auto"
                                        style={{width:50}}>
                                        <Image src="/img/ico-document.png"
                                            alt="KTP"
                                            height="50"
                                            width="50" />                                        
                                    </div>
                                    <div className="text-xs mt-1">Analisa</div>
                                </a>
                            </Link>
                        </div>
                    </div>

                    <div className="mt-5">
                        <div className="font-epilogue font-bold text-xl mb-2">
                            Grafik Harga Emas
                        </div>
                        <div style={{height:500}}>
                            <AdvancedRealTimeChart ColorTheme="light"
                                autosize
                                Interval="D"
                                symbol="XAUUSD"
                                hide_top_toolbar="true"
                                hide_side_toolbar="true" />
                        </div>
                    </div>        
                </div>
            }
        </>
    )
}
