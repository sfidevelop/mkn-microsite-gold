import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api";
import TotalAccount from "components/straits/account/totalAccount";
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import Powered from "components/straits/powered";
import FooterPage from "components/footerPage";

export default function FAQ() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            loadingPortfolio: true,
            account: {},
            handphone:"",
            watchlists: [],
            portfolios: []
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getAccount = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.account = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.account = response.data.straits_account;
            objLocalState.loadingPage = false;
            objLocalState.handphone = response.data.handphone;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const getWatchlist = async() => {
        let objLocalState = localState;
        objLocalState.watchlist = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/watchlists', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.watchlists = response.data.watchlists;
            setLocalState({...objLocalState, ...localState});
        }
    }
    const getPortfolio = async(range = "1") => {
        let objLocalState = localState;
        objLocalState.loadingPortfolio = true;
        objLocalState.portfolios = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/transaction/portfolio', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.portfolios = response.data.transactions;
            objLocalState.loadingPortfolio = false;
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }
    useEffect(() => {
        getAccount();
        getWatchlist();
        getPortfolio();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <HeaderPage title="FAQ" />
                    <div className="custom-pt-70 custom-pb-70">
                        <div className="max-w-6xl mx-auto px-1">
                            <h1 className="text-xl">
                                FREQUENTLY ASKED QUESTIONS
                            </h1>
                            <p><br></br></p>
                            <h1 className="text-xl text-rose-500">
                                Tentang KAYYA
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apakah KAYYA sebuah entitas hukum?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        KAYYA bukanlah suatu entitas. Dikelola oleh PT Straits Futures Indonesia, KAYYA adalah sebuah aplikasi trading komoditi pertama dunia yang menawarkan kepada investor retail Indonesia komoditi bursa global yang difraksionasi. 
                                        <p><br></br></p>
                                        Untuk informasi lebih lanjut tentang KAYYA, silakan kunjungi tautan berikut <a className="text-blue-600 underline" href="https://www.kayya.co.id">www.kayya.co.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apa saja produk yang ditawarkan di KAYYA? 
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Nasabah dapat bertransaksi berdasarkan kategori-kategori produk di bawah ini.
                                        <p><br></br></p>
                                        •	Indeks Global - Indeks Saham Hong Kong (ISHK) dan Indeks Saham China (ISCN)<br></br>
                                        •	Komoditas - Mini Crude Oil Futures (MIMM), Mini Crude Palm Oil (MIMS), dan Micro Silver (MIPK)<br></br>
                                        •	Mata Uang - EUR/USD, GBP/USD, AUD/USD
                                        <p><br></br></p>
                                        Untuk informasi lebih lanjut tentang produk-produk di atas, silakan klik pada tautan produk yang tercantum dalam Aplikasi KAYYA, selanjutnya Anda dapat menemukan deskripsi lebih lanjut dari produk-produk tersebut sebagai referensi Anda.
                                        <p><br></br></p>
                                        KAYYA akan terus mengeluarkan produk-produk baru yang menarik. Jika ada produk yang menarik bagi Anda, kami ingin mendengar masukan Anda. Silakan sampaikan masukan Anda tersebut kepada Customers Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Bagaimana jam trading dari produk-produk yang ditawarkan di KAYYA?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        <div class="relative overflow-x-auto content-center">
                                            <table class="justify-center border text-sm text-left text-black table-auto content-center">
                                                <thead class="text-xs uppercase border">
                                                    <tr>
                                                        <th scope="col" class="px-6 border py-3 w-50 text-center">
                                                            No
                                                        </th>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Produk
                                                        </th>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Jam Trading
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            1
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            Indeks Saham Hong Kong (IHSK)
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Sesi pagi: 8:15 - 11:00 WIB<br></br>
                                                            Sesi siang: 12:00 - 15:30 WIB<br></br>
                                                            Sesi malam: 16:15 - 02:00 WIB<br></br>
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            2
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            Indeks Saham China (ISCN)

                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Pembukaan sesi T: 08:00 - 15:30 WIB<br></br>
                                                            Pembukaan sesi T+1: 16:00 - 04:15 WIB
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            3
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            EUR/USD (EURM)

                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Senin - Jumat 06:00 - 05:00 (+1) WIB
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            4
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            GBP/USD (GBPM)
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Senin - Jumat 06:00 - 05:00 (+1) WIB
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            5
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            AUD/USD (AUDM)
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Senin - Jumat 06:00 - 05:00 (+1) WIB
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            6
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            Mini Crude Oil (MIMM)
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Senin - Jumat 06:00 - 05:00 (+1) WIB
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            7
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            Mini Palm Oil (MIMS)
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Sesi pagi: 09:30 - 11:30 WIB<br></br>
                                                            Sesi siang: 13:30 - 17:00 WIB<br></br>
                                                            Sesi malam: 20:00 - 22:30 WIB<br></br>
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <th scope="row" class="px-2 py-4 font-medium text-center border">
                                                            8
                                                        </th>
                                                        <td class="px-6 py-4 border">
                                                            Mini Silver (MIPK)
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Senin - Jumat 06:00 - 05:00 (+1) WIB
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Tentang Straits Futures Indonesia
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Siapakah PT Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                    PT Straits Futures Indonesia adalah suatu pialang berjangka yang mendapatkan izin dari Bappebti (Izin No.43/BAPPEBTI/SI/09/2015), dan merupakan Anggota Pialang pada Jakarta Futures Exchange (“JFX”) dan Kliring Berjangka Indonesia (Persero) (“KBI”).
                                    <p><br></br></p>
                                    Straits Futures Indonesia (SFI) adalah subsidiari dari Straits Financial Group dengan kehadiran secara global di pasar keuangan utama di Amerika Serikat, Singapura, China, Dubai, Indonesia, dan Vietnam.
                                    <p><br></br></p>
                                    Untuk informasi lebih lanjut tentang Straits Financial Group, silakan mengunjungi tautan berikut <a className="text-blue-600 underline" href="https://www.straitsfinancial.com">www.straitsfinancial.com</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Pembuatan Akun
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Saya adalah user yang terdaftar dengan FINKU. Mengapa akun trading komoditi saya dibuka di Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Produk komoditas dan indeks ditawarkan oleh PT Straits Futures Indonesia, pialang berjangka yang memperoleh izin dari dan diatur oleh BAPPEBTI. Sebagaimana pemegang izin usaha adalah PT Straits Futures Indonesia (berdasarkan izin No.43/BAPPEBTI/SI/09/2015), maka nasabah yang ingin bertransaksi produk-produk tersebut diwajibkan untuk menjalin hubungan pialang-nasabah langsung dengan nasabah sesuai dengan peraturan Bappebti.
                                        <p><br></br></p>
                                        Jika Anda memiliki pertanyaan lebih lanjut  tentang entitas hukum untuk bertransaksi produk KAYYA, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>
                            

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Bagaimana cara untuk memulai bertransaksi produk-produk KAYYA?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Untuk mulai bertransaksi produk KAYYA, Anda harus membuka akun di PT Straits Futures Indonesia dengan mengisi Standard Customer Due Diligence (SCDD) sebagaimana ditetapkan oleh Bappebti.
                                        <p><br></br></p>
                                        Minimum deposit adalah Rp 500.000,00 (Lima Ratus Ribu Rupiah)
                                        <p><br></br></p>
                                        Setelah kami menerima minimum deposit yang disyaratkan, Anda akan melihat dana tercermin dalam Portofolio Anda. Setelah itu, Anda dapat mulai bertransaksi produk KAYYA.
                                        <p><br></br></p>
                                        Jika Anda memiliki pertanyaan lebih lanjut tentang cara memulai bertransaksi produk KAYYA, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>


                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Berapa lama pembukaan akun dengan PT Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Akun trading Anda diproses dalam waktu 24 jam. Dengan syarat bahwa data pribadi yang dikirimkan lengkap dan memenuhi ketentuan Standard Customer Due Diligence (SCDD).
                                        <p><br></br></p>
                                        Sebagai bagian dari peraturan penerimaan nasabah dari Bappebti, Anda akan dihubungi oleh staf Customer Service kami untuk menyelesaikan proses verifikasi.
                                        <p><br></br></p>
                                        Jika Anda memiliki pertanyaan lebih lanjut mengenai prosedur pembukaan akun, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Mengapa saya tidak dapat mengeksekusi transaksi Beli?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Anda perlu memastikan bahwa akun trading Anda disetujui dan didanai sebelum Anda dapat melakukan transaksi Beli apa pun.
                                        <p><br></br></p>
                                        Jika Anda masih mengalami kegagalan dalam melakukan transaksi Beli meskipun Anda telah mendanai akun trading Anda yang telah disetujui tersebut, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Dokumen apa saja yang diperlukan untuk pengajuan pembukaan akun dengan PT Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Anda disyaratkan untuk menyampaikan dokumen berikut untuk pembukaan akun dengan PT Straits Futures Indonesia.
                                        <p><br></br></p>
                                        <div className="p-4 pt-0 pb-0">
                                            1.	Kartu identitas Anda yang sah dan terbaca dengan jelas (KTP untuk WNI dan Paspor untuk WNA),<br></br>
                                            2.	Foto selfie dengan kartu identitas Anda,<br></br>
                                            3.	Nomor NPWP Anda beserta foto kartu NPWP,<br></br>
                                            4.	Nomor rekening bank Anda, dan data/informasi pribadi lainnya yang diminta oleh PT Straits Futures Indonesia.<br></br>
                                        </div>
                                        <p><br></br></p>
                                        Jika Anda mengalami masalah dengan penyampaian dokumen, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email untuk <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Mengapa PT Straits Futures Indonesia mensyaratkan berbagai data pribadi?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Peraturan Bappebti terkait KYC di Indonesia mewajibkan lembaga keuangan untuk melakukan uji tuntas nasabah sebagai bagian dari proses penerimaan nasabah. Nasabah PT Straits Futures Indonesia termasuk dalam kategori Standard CDD yang mensyaratkan kepatuhan terhadap pengumpulan data pribadi tersebut sebagaimana ditetapkan oleh BAPPEBTI.
                                        <p><br></br></p>
                                        Jika Anda memiliki pertanyaan lebih lanjut tentang persyaratan data pribadi, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apakah yang akan terjadi jika input data pribadi saya salah dan bagaimana cara saya mengoreksinya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Input data pribadi yang salah dapat mengakibatkan penundaan atau penolakan aplikasi akun.
                                        <p><br></br></p>
                                        Anda dapat mengubah penyampaian data yang salah baik secara langsung pada Aplikasi KAYYA bagian Data Pribadi atau menghubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Saya tidak menerima kode verifikasi/OTP.
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Ada beberapa kemungkinan alasan mengapa kode verifikasi/OTP tidak diterima. Sebaiknya Anda:
                                        <p><br></br></p>
                                        <div className="p-4 pt-0 pb-0">
                                            a.	Pastikan bahwa Anda telah mengisi alamat email yang benar,<br></br>
                                            b.	Pastikan koneksi internet di perangkat Anda berfungsi dengan baik,<br></br>
                                            c.	Refresh kotak masuk email Anda, atau Anda dapat memeriksa folder junk/spam kotak surat Anda,<br></br>
                                            d.	Anda dapat mengajukan permintaan untuk mengirim ulang kode OTP lagi.
                                        </div>
                                        <p><br></br></p>
                                        Jika Anda masih belum menerima kode verifikasi/OTP walaupun Anda sudah memenuhi semua checklist di atas, silakan hubungi Customer Service Desk kami di +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a> untuk bantuan.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Saya tidak dapat mengunggah dokumen yang disyaratkan.
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Ada beberapa kemungkinan alasan mengapa dokumen Anda tidak dapat diunggah. Pastikan dokumen Anda dalam bentuk tipe file yang benar (.pdf atau .jpg) dan ukurannya kurang dari 2 MB. Pastikan juga konektivitas internet Anda lancar.
                                        <p><br></br></p>
                                        Jika Anda mengalami kesulitan mengunggah walaupun Anda telah memenuhi semua persyaratan, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Perlindungan Data Pribadi
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apakah data pribadi saya disimpan dengan aman dan rahasia di PT Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        PT Straits Futures Indonesia menyadari pentingnya data pribadi yang telah Anda percayakan kepada kami dan berkomitmen untuk melindungi data pribadi Anda sesuai dengan persyaratan, pedoman, arahan berdasarkan Undang-Undang Republik Indonesia No. 27 Tahun 2022 tentang Perlindungan Data Pribadi (“UU PDP”), dan peraturan perundang-undangan lain yang relevan.
                                        <p><br></br></p>
                                        Jika Anda memiliki pertanyaan lebih lanjut mengenai integritas data pribadi Anda dengan PT Straits Futures Indonesia, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Mengapa saya harus melakukan proses verifikasi?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Verifikasi oleh Customer Service Desk kami diperlukan untuk memastikan bahwa identitas Anda sah dan benar sesuai ketentuan yang berlaku dan untuk memastikan pengalaman bertransaksi Anda dengan PT Straits Futures Indonesia aman dari kejahatan dan pencurian.
                                        <p><br></br></p>
                                        Jika Anda memiliki masalah lebih lanjut dengan proses verifikasi, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Pengamanan Kredensial Akun
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apa yang terjadi jika terjadi kebocoran pada kredensial akun saya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Harap simpan dan kelola kata sandi atau password Anda dengan aman untuk menghindari potensi risiko kehilangan kepemilikan, peretasan dan phishing, serta pelanggaran privasi yang tidak diinginkan. Anda bertanggung jawab untuk merahasiakan kredensial akun Anda.
                                        <p><br></br></p>
                                        Jika terjadi kebocoran kredensial, Anda harus segera melakukan reset password atau segera menghubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Deposit Dana
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Berapa deposit minimum untuk memulai akun trading dengan PT Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Deposit minimum untuk memulai akun trading dengan PT Straits Futures Indonesia adalah Rp 500.000,00 (Lima Ratus Ribu Rupiah)
                                        <p><br></br></p>
                                        Deposit dalam mata uang Rupiah akan secara otomatis dikonversi ke USD menggunakan kurs counter bank.
                                        <p><br></br></p>
                                        Akun trading Anda hanya akan diaktifkan ketika deposit Anda memenuhi minimum USD100 yang ditentukan. Deposit akan tercermin dalam Portofolio Anda di Aplikasi trading KAYYA.
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan lebih lanjut mengenai deposit, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Berapa lama waktu yang diperlukan hingga deposit saya tercermin di akun saya sebelum saya dapat memulai trading?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Deposit dana akan difasilitasi melalui payment gateway, yang berarti Anda akan dapat melihat deposit dana tercermin dalam Portofolio Anda setelah dana dikreditkan ke akun Anda.
                                        <p><br></br></p>
                                        Jika deposit tidak tercermin dalam Portofolio Anda, silakan hubungi Customer Service Desk kami di WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a> untuk bantuan.
                                        <p><br></br></p>
                                        Anda akan diminta untuk mengirimkan bukti setoran berupa tanda terima transfer ke Akun Virtual.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Payment gateway untuk deposit dana tidak berfungsi. Adakah alternatif bagi saya untuk menyetor dana saya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Jika terjadi kegagalan teknis pada payment gateway, Anda dapat memilih untuk mendanai akun Anda secara langsung melalui Transfer Bank ke Akun Virtual Anda setelah melakukan pendaftaran akun trading Anda.
                                        <p><br></br></p>
                                        Silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a> jika Anda memerlukan panduan lebih lanjut.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Ke mana saya harus menyetor dana saya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Setelah akun trading Anda disetujui, Anda dapat memilih akun virtual pilihan Anda untuk deposit dana; baik ke CIMB Niaga atau BCA sebagai pilihan di Aplikasi trading KAYYA. Deposit dilakukan melalui payment gateway ke dalam rekening kustodian terpisah yang dimiliki oleh Lembaga Kliring Indonesia, PT Kliring Berjangka Indonesia (KBI).
                                        <p><br></br></p>
                                        Jika Anda memerlukan bantuan atau penjelasan mengenai deposit dana, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Bisakah saya mentransfer deposit saya dalam USD dan bukan IDR?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                    Berdasarkan peraturan Bank Indonesia terkait payment gateway saat ini, semua transfer dana dalam negeri melalui payment gateway harus dan hanya dapat diterima dalam mata uang Rupiah.
                                    <p><br></br></p>
                                    Setelah dana Anda diterima oleh bank, dana tersebut akan dikonversi ke USD dengan kurs counter bank dan dikreditkan ke akun trading Anda.
                                    <p><br></br></p>
                                    Jika Anda memerlukan penjelasan lebih lanjut mengenai deposit, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apakah dana saya aman di PT Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Sebagai pialang berjangka yang berizin dan diatur oleh Bappebti (Izin No 43/BAPPEBTI/SI/09/2015), dan merupakan Anggota Pialang Jakarta Futures Exchange (JFX) dan Anggota Kliring Kliring Berjangka Indonesia Persero (KBI), semua transaksi nasabah terdaftar di Jakarta Futures Exchange dan dana nasabah kami disimpan di rekening bank yang terpisah di Kliring Berjangka Indonesia Persero (KBI).
                                        <p><br></br></p>
                                        Jika Anda memiliki pertanyaan lebih lanjut mengenai keamanan dana Anda, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Mengapa saldo akun saya tidak terbarui bahkan setelah saya melakukan transfer deposit?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                    Ada beberapa kemungkinan penyebab keterlambatan tercerminnya deposit di saldo akun Anda, di antaranya termasuk:
                                    <p><br></br></p>
                                    <div className="p-4 pt-0 pb-0">
                                        1)	Keterlambatan pemberitahuan dari payment gateway mengenai transaksi deposit Anda, atau<br></br>
                                        2)	Dana yang ditransfer tidak memenuhi persyaratan setoran minimum.<br></br>
                                    </div>
                                    <p><br></br></p>
                                    Harap perhatikan bahwa akun trading Anda hanya akan diaktifkan ketika deposit Anda memenuhi minimum Rp 500.000,00 (Lima Ratus Ribu Rupiah) yang ditentukan. Hanya dengan demikian setoran akan tercermin dalam Saldo Akun Anda di Aplikasi Trading KAYYA.
                                    <p><br></br></p>
                                    Jika Anda memerlukan penjelasan lebih lanjut mengenai jumlah minimum deposit, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apakah ada biaya tambahan untuk menyimpan dana saya di PT Straits Futures Indonesia?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        <div class="relative overflow-x-auto content-center">
                                            <table class="justify-center border text-sm text-left text-black table-auto content-center">
                                                <thead class="text-xs uppercase border">
                                                    <tr>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Bank
                                                        </th>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Biaya
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            CIMB Niaga
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp2.500 termasuk PPN
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            BCA
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp3.500 termasuk PPN
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            KBI
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp1.655 termasuk PPN
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan tentang biaya-biaya ini, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Trading
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apakah Anda membebankan komisi untuk transaksi saya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Nasabah yang memperdagangkan produk-produk yang ditawarkan oleh PT Straits Futures Indonesia menikmati transaksi bebas komisi sehingga dana Anda pergunakan untuk trading daripada biaya.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Mengapa harga yang dieksekusi untuk transaksi berbeda dari harga yang saya lihat sebelumnya di layar?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Karena pergerakan harga produk yang konstan, semua transaksi dilakukan berdasarkan market order dengan harga yang sangat dekat dengan harga terakhir yang Anda lihat.
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan lebih lanjut mengenai harga produk, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Mengapa produk diperdagangkan dalam mata uang asing, bukan Rupiah?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Semua produk yang ditawarkan oleh PT Straits Futures Indonesia adalah kontrak yang diperdagangkan di bursa luar negeri dan tunduk pada spesifikasi kontrak termasuk mata uang denominasi kontrak.
                                        <p><br></br></p>
                                        PT Straits Futures Indonesia memiliki persetujuan untuk Penyaluran Amanat Nasabah ke Bursa Luar Negeri (PALN) oleh Bappebti.
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan lebih lanjut tentang kontrak luar negeri tersebut, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Saldo Transaksi
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Di mana saya dapat melihat saldo akun saya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Saldo akun dapat dilihat di halaman Portofolio Aplikasi trading KAYYA. Keuntungan/kerugian yang belum direalisasi dari transaksi Anda akan ditampilkan dan diperbarui secara real-time di Portofolio aplikasi.
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan mengenai saldo akun Anda, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Bagaimana saldo akun saya dihitung?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Saldo akun Anda berasal dari nilai Portofolio Anda + Ekuitas yang Tersedia.
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan mengenai perhitungan saldo akun Anda, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Penarikan Dana
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                        Apakah ada batasan dan biaya yang dikenakan untuk penarikan saya di akun saya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Anda dapat menarik saldo Ekuitas Tersedia (USD) dikurangi biaya payment gateway dan biaya kliring seperti yang diterapkan di bawah ini:
                                        <p><br></br></p>
                                        <div class="relative overflow-x-auto content-center">
                                            <table class="justify-center border text-sm text-left text-black table-auto content-center">
                                                <thead class="text-xs uppercase border">
                                                    <tr>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Bank
                                                        </th>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Biaya
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            CIMB Niaga
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp2.500 termasuk PPN
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            BCA
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp3.500 termasuk PPN
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            KBI
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp1.655 termasuk PPN
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan tentang penarikan dana, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Dapatkah saya menerima penarikan saya dalam USD?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Karena penarikan dana dilakukan melalui payment gateway, semua transfer dana harus mematuhi peraturan Bank Indonesia terkait payment gateway saat ini di mana semua transfer dana dalam negeri harus dan hanya dapat ditransfer dalam mata uang Rupiah.
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan tentang penarikan USD, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Dapatkah saya menerima penarikan saya dalam USD?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Penarikan dana dapat diproses pada hari kerja yang sama atau hingga T+1 hari kerja. Untuk meminimalkan keterlambatan dalam proses penarikan, harap pastikan bahwa transfer dana ke rekening bank atas nama yang sama dengan Akun Trading Anda.
                                        <p><br></br></p>
                                        Penarikan dana dibatasi hanya untuk bank domestik dan tidak dapat ditarik ke rekening bank orang lain, platform pembayaran pihak ketiga atau rekening bank gabungan.
                                        <p><br></br></p>
                                        Harap dicatat bahwa akan ada biaya payment gateway yang berlaku untuk semua transaksi deposit dan penarikan.
                                        <p><br></br></p>
                                        <div class="relative overflow-x-auto content-center">
                                            <table class="justify-center border text-sm text-left text-black table-auto content-center">
                                                <thead class="text-xs uppercase border">
                                                    <tr>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Bank
                                                        </th>
                                                        <th scope="col" class="px-6 border py-3 text-center">
                                                            Biaya
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            CIMB Niaga
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp2.500 termasuk PPN
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            BCA
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp3.500 termasuk PPN
                                                        </td>
                                                    </tr>
                                                    <tr class="text-black bg-white border">
                                                        <td class="px-6 py-4 border">
                                                            KBI
                                                        </td>
                                                        <td class="px-6 py-4 border">
                                                            Rp1.655 termasuk PPN
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <p><br></br></p>
                                        Jika Anda memerlukan penjelasan tentang penarikan dana, silakan hubungi Customer Service Desk kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Apa yang harus saya lakukan jika dana penarikan belum ditransfer ke rekening bank saya setelah 3x24 jam?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Jika dana Anda tidak masuk ke rekening Anda setelah 3x24 jam, silakan hubungi Customer Service kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Penarikan Dana
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Bagaimana cara memperbarui data saya jika ada perubahan?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Pada aplikasi KAYYA, Anda dapat membarui data pribadi Anda di bagian Data Pribadi.
                                        <p><br></br></p>
                                        Atau, Anda dapat menghubungi kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a> sehingga Customer Service Desk kami dapat membantu Anda untuk pembaruan data pribadi. 
                                    </div>
                                </div>
                            </div>

                            <h1 className="text-xl text-rose-500">
                                Penutupan Akun
                            </h1>

                            <div className="text-base">
                                <div className="border-solid p-3 container relative">
                                    <input className="peer cursor-pointer h-6 w-6 absolute right-0 mr-2 opacity-0" type="checkbox" />
                                    <FontAwesomeIcon icon={faAngleDown} className="fa-lg float-right" />
                                    <div className="flex justify-between border-b-2">
                                        <h1 className="font-bold mb-1">
                                            Bagaimana cara untuk menutup akun saya?
                                        </h1>
                                    </div>
                                    <div className="overflow-hidden mt-1 max-h-0 peer-checked:max-h-fit transition-all duration-500 ease">
                                        Kami menyayangkan bahwa Anda mempertimbangkan untuk menutup akun trading Anda. Kami menyambut feedback apa pun untuk menjaga hubungan akun dengan Anda.
                                        <p><br></br></p>
                                        Ya, jika Anda ingin menutup akun Anda, Anda dapat melakukannya setelah Anda menjual semua aset Anda dan menarik semua dana di akun trading Anda.
                                        <p><br></br></p>
                                        Jika Anda memerlukan bantuan untuk penutupan akun, silakan hubungi kami melalui WhatsApp +62882003491816 atau email ke <a className="text-blue-600 underline" href="mailto:customer.experience@finku.id">customer.experience@finku.id</a>.
                                    </div>
                                </div>
                            </div>

                            <div className="text-base mt-5">
                            <span className="font-bold">Contact Us for Any Inquiry:</span> <br/>
                            Email: cs@kayya.co.id <br/>
                            Phone: +62 21 5010 3599 <br/>
                            Working hours: Mon - Fri, 9.00 17.00 except for public holidays
                            </div>
                        </div>
                    </div>
                    <FooterPage selected="faq" />
                </>
            }
        </>
    )
}
