import React, {useState, useEffect} from "react";
import { useUser } from 'lib/hooks'
import Image from 'next/image';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import ErrorScreen from "components/errorScreen";

export default function Index() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true,
            loadingJual: false,
            transaction: {},
            kurs: 1
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getTransaction = async() => {
        setLocalState(initialLocalState);
        let objLocalState = localState;

        try {
            var response = await API.Get('/account/straits/transaction/sell/request', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}&transaction_code=${router.query.code}`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") {
            objLocalState.transaction = response.data.transaction;
            objLocalState.kurs = response.data.kurs;
            setLocalState({...objLocalState, ...localState});
        }
        else objLocalState.errorPageShow = true;
        return true;
    }
    const proceed = async() => {
        let objLocalState = localState;
        objLocalState.loadingJual = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/account/straits/transaction/sell/proceed', {
                "code" : process.env.NEXT_PUBLIC_CODE,
                "client_code" : process.env.NEXT_PUBLIC_CLIENTKEY,
                "token" : localStorage.getItem("logined_token"),
                "transaction_code" : router.query.code
            });
        }
        catch(error) {
            router.push('/logined/index/transaction/failed');
            return true;
        }

        if(response.code == "000") {
            if(response.data.success == "true") router.push('/logined/index/transaction/success');
            else router.push('/logined/index/transaction/failed');
        }
        else router.back();
        return true;
    }
    useEffect(() => {
        if(!router.isReady) return;
        getTransaction();
    }, [router.isReady]);

    return (
        <>
            {localState.errorPageShow && <ErrorScreen />}
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                    height="20"
                                    width="20"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Jual
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">                        
                        <div className="grid grid-cols-2">
                            <div className="flex">
                                <div>
                                    <Image src={localState.transaction.index.image} 
                                        height="24"
                                        width="24"
                                        alt={localState.transaction.index.name}
                                        className="rounded-full" />
                                </div>
                                <div className="ml-2">
                                    <div className="font-semibold">{localState.transaction.index.symbol}</div>
                                    <div>{localState.transaction.index.name}</div>
                                </div>
                            </div>
                        </div>   
                    </div> 

                    <div className="sm:container mx-auto p-4 bg-gray-2 mt-5">
                        <div className="subtitle2">{localState.transaction.code}</div>
                        <div>{new Date(localState.transaction.created_at).toLocaleString()}</div>
                        <div className="mt-10">
                            <div className="grid grid-cols-2">
                                <div>Harga Sekarang</div>
                                <div className="text-right">{localState.transaction.index.currency} {localState.transaction.index.price_bid}</div>
                            </div>
                            <div className="grid grid-cols-2 mt-2">
                                <div>Jumlah Unit</div>
                                <div className="text-right">{localState.transaction.volume}</div>
                            </div>
                            <div className="grid grid-cols-2 mt-2">
                                <div>Total Nilai</div>
                                <div className="text-right">{localState.transaction.index.currency} {(localState.transaction.volume * localState.transaction.index.price_bid * localState.transaction.ContractSize).toFixed(2)}</div>
                            </div>
                            <hr className="mt-2" />
                            <div className="grid grid-cols-2 mt-2">
                                <div>Modal Investasi</div>
                                <div className="text-right">{localState.transaction.index.currency} {(localState.transaction.open_price * localState.transaction.volume * localState.transaction.ContractSize).toFixed(2)}</div>
                            </div>
                            <div className="grid grid-cols-2 mt-2">
                                <div>Keuntungan</div>
                                <div className={"text-right " + (localState.transaction.change >= 0 ? "color-green" : "color-red")}>{localState.transaction.index.currency} {(localState.transaction.change * localState.transaction.volume * localState.transaction.ContractSize).toFixed(2)}</div>
                            </div>
                            {localState.transaction.index.currency != "USD" && 
                                <>
                                    <hr className="mt-2" />
                                    <div className="grid grid-cols-2 mt-2">
                                        <div>Kurs USD/{localState.transaction.index.currency}</div>
                                        <div className="text-right">{localState.transaction.index.currency} {(localState.kurs).toFixed(2)}</div>
                                    </div>
                                    <div className="grid grid-cols-2 mt-2">
                                        <div>Keuntungan USD</div>
                                        <div className={"text-right " + (localState.transaction.change >= 0 ? "color-green" : "color-red")}>USD {(localState.transaction.change * localState.transaction.volume / localState.kurs).toFixed(2)}</div>
                                    </div>
                                </>
                            }
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 mt-5">
                        <div className="bg-gray-10-75 text-white rounded p-4">
                            <div className="subtitle2 color-white">Lainnya</div>
                            <div>Harga Index dan Kurs tidak mengikat dan dapat berubah sewaktu-waktu. Dengan melanjutkan halaman ini berati anda telah menyetujui syarat dan ketentuan yang berlaku</div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 mt-5">
                        {localState.loadingJual
                            ?
                            <button className="button-primary w-full"
                                disabled="disabled">
                                <Image src="/img/loading.png"
                                    alt="loading"
                                    height={16}
                                    width={16}
                                    className="animate-spin" />
                                <span className="ml-3">Loading ...</span>
                            </button>
                            :                
                            <button className="button-primary w-full"
                                onClick={(e)=>proceed()}>
                                Selesaikan
                            </button>
                        }
                    </div>
                </>
            }
        </>
    )
}
