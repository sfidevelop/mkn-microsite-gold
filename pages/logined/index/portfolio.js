import React, {useState, useEffect} from "react";
import { useUser } from 'lib/hooks'
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import Powered from "components/straits/powered";
import ErrorScreen from "components/errorScreen";

export default function Index() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true,
            portfolios: [],
            calculation : {
                totalPortfolio : 0,
                totalPnL: 0,
                totalPnLPercent: 0
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getPortfolio = async() => {
        setLocalState(initialLocalState);

        let objLocalState = localState;
        try {
            var response = await API.Get('/account/straits/transaction/portfolio', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}&limit=all&usd_convertion=true`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") {
            objLocalState.portfolios = response.data.transactions;
            objLocalState.calculation.totalPortfolio = response.data.calculation.total;
            objLocalState.calculation.totalPnL = response.data.calculation.totalGain;
            objLocalState.calculation.totalPnLPercent = response.data.calculation.totalGainPercent;
            objLocalState.loadingPage = false;
            
        }
        else objLocalState.errorPageShow = true;
        setLocalState({...objLocalState, ...localState});
        return true;
    }
    useEffect(() => {
        getPortfolio();
    }, []);

    return (
        <>
            {localState.errorPageShow && <ErrorScreen />}
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="20"
                                        width="20"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Portofolio
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">                        
                        <div className="grid grid-cols-2 p-4 rounded bg-gray-2 drop-shadow">
                            <div className="">Total Portofolio</div>
                            <div className="text-right">
                                <span className="mr-1">USD</span>
                                <span className="subtitle2">{localState.calculation.totalPortfolio.toFixed(2)}</span>
                            </div>

                            <div className="mt-2">Total Keuntungan</div>
                            <div className="mt-2 text-right">
                                <span className={localState.calculation.totalPnL < 0 ? "text-red-400 mr-1 text-sm" : "text-green-400 mr-1 text-sm"}>
                                    {localState.calculation.totalPnL.toFixed(2)}
                                </span>
                                <span>
                                    (
                                    <span className={localState.calculation.totalPnLPercent < 0 ? "text-red-400 text-sm" : "text-green-400 text-sm"}>
                                        {localState.calculation.totalPnLPercent}
                                    </span>
                                    )%
                                </span>
                            </div>
                        </div>
                        
                        {localState.portfolios.length > 0
                            ?
                            <>
                                <div className="mt-5 grid grid-cols-2 font-semibold">
                                    <div>Index</div>
                                    <div className="text-right">Value/P&L</div>
                                </div>                                                    
                                <div className="divide-y">
                                    {localState.portfolios.map((item,index) => {
                                        return (
                                            <Link href={{
                                                pathname: '/logined/index/symbol/[symbol]',
                                                query: { symbol: item.index.symbol },
                                            }}
                                            key={index}>
                                                <a className="py-5 block">
                                                    <div className="grid grid-cols-2">
                                                        <div className="flex">
                                                            <div>
                                                                <Image src={item.index.image} 
                                                                    height="24"
                                                                    width="24"
                                                                    alt={item.index.name}
                                                                    className="rounded-full" />
                                                            </div>
                                                            <div className="ml-2">
                                                                <div className="font-semibold">{item.index.symbol}</div>
                                                                <div>{item.index.name}</div>
                                                            </div>
                                                        </div>
                                                        <div className="text-right">
                                                            <div className="font-semibold">{item.index.currency} {(item.open_price * item.volume * item.ContractSize).toFixed(2)}</div>
                                                            <div className={"font-semibold " + (item.change <= 0 ? "color-red" : "color-green")}>
                                                                <span className="mr-1">{(item.change * item.volume * item.ContractSize).toFixed(2)}</span>
                                                                <span>({Number(item.change_percent).toFixed(2)}%)</span>
                                                            </div>
                                                        </div>                                        
                                                    </div>                                        
                                                </a>
                                            </Link>
                                        )
                                    })}                        
                                </div>  
                            </>
                            :
                            <div className="mt-10 text-center">
                                Kamu belum memiliki portofolio index
                            </div>
                        }
                    </div>

                    <Powered />
                </>
            }
        </>
    )
}
