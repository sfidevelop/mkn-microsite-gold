import React, {useState, useEffect} from "react";
import { useUser } from 'lib/hooks'
import Link from 'next/link';
import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api";
import TotalAccount from "components/straits/account/totalAccount";
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import Powered from "components/straits/powered";
import FooterPage from "components/footerPage";
import ErrorScreen from "components/errorScreen";
import SymbolsScreen from "components/straits/index/dashboard/symbols";
import PortfoliosScreen from "components/straits/index/dashboard/portfolios";

export default function Index() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true, 
            account: {},
            handphone:"",
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getAccount = async() => {
        setLocalState(initialLocalState);

        let objLocalState = localState;
        try {
            var response = await API.Get('/account/straits', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") {
            objLocalState.account = response.data.straits_account;
            objLocalState.handphone = response.data.handphone;
        }
        else objLocalState.errorPageShow = true;

        setLocalState({...objLocalState, ...localState});
        return true;
    }

    const getCountNotifications = async() => {
        try {
            var response = await API.Get('/notification/count', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`
            );
        }
        catch(error) {
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.notificationCount = response.data.count;
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }

    useEffect(() => {
        getAccount();
        getCountNotifications();
    }, []);

    return (
        <>
            {localState.errorPageShow && <ErrorScreen />}
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                 <div className="grid grid-cols-3">
                    <div></div>
                    <div className="text-center pt-5">
                        <Image src="/img/Kayya-01.png" height="60%" width="158%" />
                    </div>
                    <div className="noti">
                            <Link href="/logined/notification">
                                <a>
                                    {localState.notificationCount > 0 &&
                                        <span className="bg-red-7 rounded-full color-white relative z-10"
                                            style={{
                                                padding:"1px 5px",
                                                top:-18,
                                                left:30,
                                            }}>
                                            {localState.notificationCount}
                                        </span>
                                    }
                                    <Image src="/img/NOTIFICON-03.png"
                                        height="24"
                                        width="24"
                                        alt="Notification" />    
                                </a>
                            </Link>                    
                    </div>
                    </div>
                    <div className="custom-pb-70">
                        <div className="sm:container mx-auto p-4">
                            {localState.handphone == "" &&
                                <div className="bg-banner border p-3 text-gray-500 rounded mb-2">
                                    Harap isi nomor handphone anda                           
                                </div>
                            }

                            {Object.keys(localState.account).length === 0 ? 
                                <div className="bg-banner border p-3 text-gray-500 rounded">
                                    Harap aktivasi akun anda untuk menggunakan fitur ini                             
                                </div>
                            : localState.account.isactive == "0" ?
                                <div className="bg-gray-10-75 p-3 text-white rounded">
                                    Harap tunggu, akun anda sedang diverifikasi. Silakan cek kembali kurang lebih dalam 60 menit ke depan
                                </div>
                            : localState.account.isactive == "-1" ?
                                <div className="bg-gray-10-75 p-3 text-white rounded">
                                    Harap validasi bank akun anda
                                </div>
                            : localState.account.isactive == "2" ?
                            <div className="bg-gray-10-75 p-3 text-white rounded">
                                Bank kamu sedang di verifikasi oleh tim terkait
                            </div>
                            :
                                <TotalAccount balance={localState.account.balance} />
                            }
                                                    
                            <div className="grid grid-cols-4 gap-3 mt-5">
                                {Object.keys(localState.account).length === 0
                                    ?
                                    <Link href="index/account/activation-information">
                                        <a className="text-center">
                                            <Image src="/img/icon_kayya_aktivasi.png"
                                                alt="Aktivasi akun"
                                                height="50"
                                                width="50" />
                                            <div className="text-center">Aktivasi Akun</div>
                                        </a>
                                    </Link>
                                    :
                                    <>
                                        {localState.account.isactive == "1" &&    
                                            <>                     
                                                <Link href="straits/topup">
                                                    <a className="text-center">
                                                        <Image src="/img/topup2.png"
                                                            alt="Topup"
                                                            height="50"
                                                            width="50" />
                                                        <div className="text-center">Top Up</div>
                                                    </a>
                                                </Link>                
                                                <Link href="straits/withdraw">
                                                    <a className="text-center">
                                                        <Image src="/img/withdraw2.png"
                                                            alt="Withdraw"
                                                            height="50"
                                                            width="50" />
                                                        <div className="text-center">Withdraw</div>
                                                    </a>
                                                </Link>
                                                <Link href="index/transaction">
                                                <a className="text-center">
                                                    <Image src="/img/transaction2.png"
                                                        alt="Transaksi"
                                                        height="50"
                                                        width="50" />
                                                    <div className="text-center">Transaction</div>
                                                </a>
                                                </Link>
                                                <Link href="index/portfolio">
                                                <a className="text-center">
                                                    <Image src="/img/portfolio2.png"
                                                        alt="Portofolio"
                                                        height="50"
                                                        width="50" />
                                                    <div className="text-center">Portfolio</div>
                                                </a>
                                                </Link>
                                            </>
                                        }                                        
                                    </>
                                }
                                {/* <Link href="/logined/index/faq">
                                    <a className="text-center">
                                        <Image src="/img/faq2.png"
                                            alt="FAQ"
                                            height="50"
                                            width="50" />
                                        <div className="text-center">FAQ</div>
                                    </a>
                                </Link> */}
                            </div>  
                        </div>
                        
                        <PortfoliosScreen />
                        <SymbolsScreen />
                        <Powered />
                    </div>

                    <FooterPage selected="home" />
                </>
            }
        </>
    )
}
