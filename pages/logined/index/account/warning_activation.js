import React, {useState, useEffect} from "react";
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import ErrorScreen from "components/errorScreen";
import { useUser } from 'lib/hooks'

export default function Index() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true,
            account: {}
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getAccount = async() => {
        setLocalState(initialLocalState);

        let objLocalState = localState;

        try {
            var response = await API.Get('/account/straits', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") objLocalState.account = response.data.straits_account;
        else objLocalState.errorPageShow = true;

        setLocalState({...objLocalState, ...localState});
        return true;
    }
    useEffect(() => {
        getAccount();
    }, []);

    return (
        <>
            {localState.errorPageShow && <ErrorScreen />}
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container mx-auto p-4">
                        <div className="grid grid-cols-3 items-center">
                            <Link href="/logined/index"
                                className="self-center">
                                <a>
                                    <Image src="/img/back.png" 
                                        height="12"
                                        width="6"
                                        alt="back" />
                                </a>
                            </Link>
                            <div className="font-epilogue font-bold text-xl text-center">
                                Akun
                            </div>
                        </div>

                        <div className="mt-20">
                            <div className="text-center">
                                <Image src="/img/pending.png" 
                                    height="128"
                                    width="128"
                                    alt="Success" />
                            </div>
                            <div className="mt-5 text-center color-red font-semibold text-2xl">
                                Oopss
                            </div>
                            <div className="mt-10 text-center">
                                {Object.keys(localState.account).length === 0
                                    ?
                                    "Harap verifikasikan akun anda untuk menggunakan fitur indeks secara lengkap"
                                    : localState.account.isactive == "0"
                                    ?
                                    <>
                                        Akun anda sedang diverifikasi.<br/>
                                        Kami akan menginformasikan anda dalam beberapa waktu mendatang
                                    </>
                                    : 
                                    "Selamat akun anda telah aktif"
                                }
                            </div>
                            <div className="mt-10">
                                <Link href="/logined/index">
                                    <a className="button-primary rounded-lg px-5 py-3 font-medium w-full block text-center">
                                        Kembali
                                    </a>
                                </Link>
                            </div>
                        </div>  
                    </div>
                </>
            }
        </>
    )
}
