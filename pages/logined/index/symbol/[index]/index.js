import React, {useState, useEffect} from "react";
import { useUser } from 'lib/hooks'
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import Powered from "components/straits/powered";
import ErrorScreen from "components/errorScreen";
import ChartScreen from "components/straits/index/symbol/chart";
import HoldingsScreen from "components/straits/index/symbol/holdings";

export default function Symbol() {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingSymbol: true,
            symbol:{},
            activePortfolio: "",
            account: {}
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getSymbol = async() => {
        setLocalState(initialLocalState);
        let objLocalState = localState;
        try {
            var response = await API.Get('/account/straits/symbol', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}&symbol=${router.query.index}`);
        }
        catch(error) {
            objLocalState.loadingSymbol = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingSymbol = false;
        if(response.code == "000") {
            let date = new Date(response.data.symbol.updated_at);

            objLocalState.symbol = response.data.symbol;
            objLocalState.symbol.updated_at = date.toLocaleString();
        }
        else objLocalState.errorPageShow = true;

        setLocalState({...objLocalState, ...localState});
        return true;
    }
    const toggleActivePortfolio = (value) => {
        let objLocalState = localState;
        objLocalState.activePortfolio = (localState.activePortfolio === value ? "" : value);                
        setLocalState({...objLocalState, ...localState});        
    }
    const getAccount = async() => {
        try {
            var response = await API.Get('/account/straits', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            return true;
        }
    
        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.account = response.data.straits_account;
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }
    useEffect(() => {
        if(!router.isReady) return;        
        getSymbol();
        getAccount();
    }, [router.isReady]);
    
    return (
        <>
            {localState.errorPageShow && <ErrorScreen />}
            {localState.loadingSymbol
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="20"
                                        width="20"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                {router.query.index}
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70"
                        style={{paddingBottom:75}}>                        
                        <div className="flex">                        
                            <div className="grow">
                                <div className="color-gray">Kayya</div>
                                <div className="font-semibold">{localState.symbol.name}</div>
                            </div>                        
                            <div className="mr-3">
                                <Image src={localState.symbol.image} 
                                    height="24"
                                    width="24"
                                    alt={localState.symbol.name}
                                    className="rounded-full" />
                            </div>
                        </div>

                        {/* <div className="mt-5">
                            <div>
                                <span className="mr-1">{localState.symbol.currency}</span>
                                <span className="font-semibold">{localState.symbol.price}</span>
                            </div>
                            <div className={"font-semibold  " + (localState.symbol.change >= 0 ? "color-green" : "color-red")}>
                                <span className="mr-1">{localState.symbol.change}</span> 
                                <span>({localState.symbol.change_percent}%)</span>
                                <span className="color-gray font-normal ml-2">{localState.symbol.updated_at}</span>
                            </div>
                        </div>                  */}
                        <div className="mt-5 font-bold text-xl">
                            Price Trend
                        </div>

                        <div className="mt-5 mb-10">
                            <ChartScreen />
                        </div>                        

                        <HoldingsScreen toggleActivePortfolio={toggleActivePortfolio}
                            activePortfolio={localState.activePortfolio} />

                        <div className="mt-5">
                            <div className="mb-1 subtitle1">
                                Tentang {localState.symbol.symbol}
                            </div>
                            <div dangerouslySetInnerHTML={{__html: localState.symbol.description}}></div>
                        </div>

                        <Powered />
                    </div>
                    
                    {localState.account.isactive == "1" && 
                        <div className="fixed bottom-0 left-0 right-0 p-3 bg-white">
                            <div className="grid grid-cols-2 gap-3">
                                <div>
                                    {localState.activePortfolio != "" &&
                                        <Link href={`/logined/index/transaction/sell?code=${localState.activePortfolio}`}>
                                            <a className="button-primary block bg-red">
                                                Jual
                                            </a>
                                        </Link>
                                    }
                                </div>
                                <div>
                                    <Link href={localState.symbol.symbol + "/buy"}>
                                        <a className="button-primary block">
                                            Beli
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    }                    
                </>
            }
        </>
    )
}
