export function getErrorCodes(code) {
    switch(code) {
        case "999" : return "Server error, harap hubungi customer service kami";

        case "R001" : return "Session telah habis, silahkan ulangi proses register kembali";
        case "R002" : return "Kode OTP kamu salah";
        case "R003" : return "Email sudah terdaftar";

        case "L001" : return "Username atau password tidak sesuai";
        default : return "";
    }
}