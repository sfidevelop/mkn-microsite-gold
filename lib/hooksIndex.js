import { useEffect } from 'react'
import Router from 'next/router'
import * as API from "services/api";

export function useIndexUser({redirect_to, redirect_if_found, redirect_if_not_found} = {}) {
    const check_user = async() => {
        try {
            var response = await API.Post('/account/straits/check', {
                "code" : process.env.NEXT_PUBLIC_CODE,
                "client_code" : process.env.NEXT_PUBLIC_CLIENTKEY,
                "token" : localStorage.getItem("logined_token"),
            });
        }
        catch(error) {
            return false;
        }
    
        if(response.code == "000") {
            if (response.data.exists == "true") return true;
            else return false;
        }
        return false;
    }

    const check_logined = async() => {
        if(redirect_if_found) {
            let is_exists = await check_user(); console.log(is_exists);
            if(is_exists == true) Router.push(redirect_to);
        }
        
        if (redirect_if_not_found) {
            let is_exists = await check_user();
            if(is_exists == false) Router.push(redirect_to);
        }
    }

    useEffect(() => {
        check_logined();
    }, [redirect_to, redirect_if_found, redirect_if_not_found]);
}