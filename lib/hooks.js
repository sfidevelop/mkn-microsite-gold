import { useEffect } from 'react'
import Router from 'next/router'
import * as API from "services/api";

export function useUser({redirect_to, redirect_if_found, redirect_if_not_found} = {}) {
    const check_user = async(logined_token, user_reference) => {
        try {
            var response = await API.Post('/auth/check', {
                "code" : process.env.NEXT_PUBLIC_CODE,
                "client_code" : process.env.NEXT_PUBLIC_CLIENTKEY,
                "token" : localStorage.getItem("logined_token"),
                "logined_token" : logined_token,
                "user_reference" : user_reference,
            });
        }
        catch(error) {
            return false;
        }
    
        if(response.code == "000") return true;
        return false;
    }

    const check_logined = async() => {
        let logined = localStorage.getItem("logined");
        let user_reference = localStorage.getItem("logined_user_reference");
        let logined_token = localStorage.getItem("logined_token");
        
        if(redirect_if_found) {
            if(logined == "true") {
                let is_authenticated = await check_user(logined_token, user_reference);
                if(is_authenticated) Router.push(redirect_to);
            }
        }
        
        if (redirect_if_not_found) {
            if(logined != "true" || user_reference == null || logined_token == null) Router.push(redirect_to);
            else {
                let is_authenticated = await check_user(logined_token, user_reference);
                if(!is_authenticated) Router.push(redirect_to);
            }
        }
    }

    useEffect(() => {
        check_logined();
    }, [redirect_to, redirect_if_found, redirect_if_not_found]);
}