import React, { Component } from "react";
import Image from "next/image";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default class CenterMode extends Component {
  render() {
    const settings = {
      infinite: true,
      centerPadding: "60px",
      slidesToShow: 1,
      speed: 1000,
      autoplay: true
    };
    return (
      <div>
        <Slider {...settings}>
            <div className="banner-item">
                <Image src="/img/slide1.jpg"
                    alt="News"
                    width="720"
                    height="459" />
            </div>
            
            <div className="banner-item">
                <Image src="/img/slide2.jpg"
                    alt="News"
                    width="720"
                    height="459" />
            </div>
            
            <div className="banner-item">
                <Image src="/img/slide3.jpg"
                    alt="News"
                    width="720"
                    height="459" />
            </div>
        </Slider>
      </div>
    );
  }
}
