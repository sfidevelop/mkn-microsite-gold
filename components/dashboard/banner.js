import React from "react";
import Image from 'next/image'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { useState } from "react";

export default function Banner() {
    const [selectedIndex, setSelectedIndex] = useState(0);
    return (
        <Carousel showThumbs={false}
        renderIndicator={(onClickHandler, isSelected, index) => {
            const defStyle = { marginLeft: 10};
            const style = isSelected
              ? { ...defStyle }
              : { ...defStyle };
            return (
                <div className="inline-block" onClick={() => setSelectedIndex(index)}>
                {isSelected ? (
                    <span
                    onClick={onClickHandler}
                    onKeyDown={onClickHandler}
                    value={index}
                    key={index}
                    role="button"
                    style={style}
                    >
                        <Image src="/img/active_carousel.png"
                        onClick={onClickHandler}
                        onKeyDown={onClickHandler}
                        width="18"
                        height="8" />
                    </span>
                ) : (
                    <span
                    onClick={onClickHandler}
                    onKeyDown={onClickHandler}
                    value={index}
                    key={index}
                    role="button"
                    style={style}
                    >
                        <Image src="/img/carousel.png"
                        onClick={onClickHandler}
                        onKeyDown={onClickHandler}
                        width="8"
                        height="8" />
                    </span>
                )}
              </div>
            );
          }}
          onChange={(index) => setSelectedIndex(index)}
          selectedItem={selectedIndex}
          >
            <div>
                <Image src="/img/straits1.png"
                    alt="News"
                    width="720"
                    height="459" />
                    <div className="pt-5 pb-20">
                        <h1 className="heading5">Tertarik sama investasi komoditas?</h1>
                        <p className="text-gray-400">Ragam produk keuangan, mulai dari pertanian, logam, energi, sampai valas dan indeks saham</p>
                    </div>
            </div>
            
            <div>
                <Image src="/img/straits2.png"
                    alt="News"
                    width="720"
                    height="459" />
                    <div className="pt-5 pb-20">
                        <h1 className="heading5">Investasi berlisensi bikin hati lebih tenang</h1>
                        <p className="text-gray-400">Berlisensi resmi dengan sistem pecah saham pertama di Indonesia, resiko rendah</p>
                    </div>
            </div>
            
            <div>
                <Image src="/img/straits3.png"
                    alt="News"
                    width="720"
                    height="459" />
                    <div className="pt-5 pb-20">
                        <h1 className="heading5">Sat-set sat-set, investasi tanpa ribet</h1>
                        <p className="text-gray-400">Investasi tanpa komisi, produk mudah dipahami tanpa istilah yang rumit</p>
                    </div>
            </div>
        </Carousel>
    )
}