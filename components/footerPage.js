import React, {useState, useEffect} from "react";
import Image from 'next/image'
import Link from 'next/link'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import * as API from "services/api";

export default function FooterPage(props) {
    const initialLocalState = () => {
        return {
            handphone: ""
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        setLocalState(initialLocalState);
        try {
            var response = await API.Get('/customer', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            console.log(error);
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.handphone = response.data.customer.handphone;
            setLocalState({...objLocalState, ...localState});
        }
    }
    useEffect(() => {
        getUser();
    }, []);
        
    return (
        <div className="footer fixed bottom-0 left-0 right-0 bg-white p-3">
            <div className="sm:container mx-auto grid grid-cols-3">
                <div className="text-center">
                    <Link href="/logined/dashboard">
                        <a>
                            <div>
                                <Image src="/img/home.png"
                                    height="20"
                                    width="20"
                                    alt="Home" />
                            </div>
                            <div className={(props.selected == "home" ? "font-semibold" : "")}>Home</div>
                        </a>
                    </Link>
                </div>
                <div className="text-center">
                    <Link href="/logined/index/faq">
                        <a className="text-center">
                            <Image src="/img/FAQ-03.png"
                                alt="FAQ"
                                height="20"
                                width="20" />
                            <div className={(props.selected == "faq" ? "font-semibold" : "")}>FAQ</div>
                        </a>
                    </Link>
                </div>
                <div className="text-center">
                    <Link href="/logined/account">
                        <a>
                            <div className="relative">
                                <Image src="/img/profil.png"
                                    height="20"
                                    width="20"
                                    alt="Akun" />
                                
                                {localState.handphone == "" &&
                                    <div className="absolute w-full top-0 ml-2"
                                        style={{marginTop:-8}}>
                                        <FontAwesomeIcon icon={faCircle}
                                            className="text-red-700 mx-auto text-xs" />
                                    </div>
                                }
                            </div>
                            <div className={(props.selected == "account" ? "font-semibold" : "")}>
                                Profile
                            </div>
                        </a>
                    </Link>                    
                </div>
            </div>
        </div>
    )
}