import React, {useState, useEffect} from "react";
import Image from 'next/image';
import Link from 'next/link'
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'

export default function WithdrawConfirmation(props) {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            loadingProcess: false,
            error: {
                isError: false,
                errorMessage: ""
            },
            rate: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getRate = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/rate/usdidr', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.back();
            return true;
        }

        if(response.code == "000") {
            objLocalState.rate = response.data.rate;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.back();
        return true;
    }
    const proceed = async() => {
        let objLocalState = localState;
        objLocalState.loadingProcess = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/account/straits/withdraw/', {
                "code": localStorage.getItem("merchantcode"),
                "client_code": localStorage.getItem("merchantClientcode"),
                "token": localStorage.getItem("logined_token"),
                "usd": props.propsState.usd
            });
        }
        catch(error) {
            objLocalState.error = {
                isError: true,
                errorMessage: "Koneksi ke server error"
            };
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {            
            objLocalState.loadingProcess = false;

            if(response.data.isSuccess == false) {
                objLocalState.error = {
                    isError: true,
                    errorMessage: response.data.message
                };
                setLocalState({...objLocalState, ...localState});
            }
            else router.push("/logined/straits/withdraw/success");
        }
        else objLocalState.error = {
            isError: true,
            errorMessage: response.data.message
        };
        setLocalState({...objLocalState, ...localState});;
        return true;
    }
    useEffect(() => {
        if(!router.isReady) return;
        getRate();
    }, [router.isReady]);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="fixed bg-white w-full z-10">
                        <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => props.changePage("withdrawInput", "0")}>
                                    <Image src="/img/back.webp" 
                                        height="20"
                                        width="20"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Konfirmasi
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">                        
                        <div className="grid grid-cols-2">
                            <div className="self-center">Nilai USD</div>
                            <div className="text-right">USD {props.propsState.usd}</div>
                        </div>

                        <div className="grid grid-cols-2 mt-2">
                            <div className="self-center">Rate USD/IDR</div>
                            <div className="text-right">IDR {Number(localState.rate.buy).toLocaleString()}</div>
                        </div>

                        <div className="grid grid-cols-2 mt-2">
                            <div className="self-center">Perkiraan nilai IDR</div>
                            <div className="text-right">IDR {Number(localState.rate.buy * props.propsState.usd).toLocaleString()}</div>
                        </div>

                        <div className="bg-gray-10-75 rounded p-4 mt-5">
                            <div className="subtitle2 color-white">Lainnya</div>
                            <div className="text-white">
                                Permintaan Anda akan dieksekusi dalam waktu 2x24 jam.<br />
                                Rate USD/IDR dapat berubah sewaktu-waktu.
                            </div>
                        </div>

                        {localState.error.isError && 
                            <div className="mt-2 bg-red-7 p-3 rounded text-white">
                                <FontAwesomeIcon icon={faCircleExclamation}
                                    className="mr-1" />
                                {localState.error.errorMessage},
                                Perbarui informasi bank akun anda sekarang
                            </div>
                        }

                        <div className="mt-5">
                            {localState.loadingProcess
                                ?
                                <button className="button-primary w-full"
                                    disabled="disabled">
                                    <Image src="/img/loading.png"
                                        alt="loading"
                                        height={16}
                                        width={16}
                                        className="animate-spin" />
                                    <span className="ml-3">Loading ...</span>
                                </button>
                                : localState.error.errorMessage == "" 
                                ?
                                <button className="button-primary w-full"
                                    onClick={(e)=>proceed()}>
                                    Selesaikan
                                </button>
                                :
                                <Link href="/logined/index">
                                    <a className="button-primary w-full block">
                                        Kembali
                                    </a>
                                </Link>
                            }
                        </div>
                    </div>
                </>
            }
        </>
    )
}
