import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement10(props) {
    const initialLocalState = () => {
        return {
            account: {},
            curr_date: {
                day: 0,
                date: 1,
                month: 1,
                year: 2023
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement10 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let d = new Date();
        let day = d.getDay();
        let date = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear();
        let array_day = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        let array_mon = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        let objLocalState = localState;
        objLocalState.account = props.account;
        objLocalState.curr_date = {
            day : array_day[day],
            date: date,
            month: array_mon[month],
            year: year
        };
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="text-center subtitle1">
                        S&K
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">PERSETUJUAN PENGGUNAAN DATA PRIBADI</div>
                <div className="mt-5">
                    <p>Yang mengisi formulir di bawah ini:</p>
                    <p className="mt-5">
                        Nama Lengkap : {localState.account.fullname}<br/>
                        Tempat & Tanggal Lahir : {localState.account.pob}, {new Date(localState.account.dob).toLocaleDateString()}<br/>
                        Alamat : {localState.account.ktp_address}<br/>
                        No. KTP/Passport : {localState.account.ktp}
                    </p>
                    <p className="mt-5">1. Saya setuju bahwa untuk memproses, mengelola dan/atau mengelola akun saya dengan PT Straits Futures Indonesia, PT Straits Futures Indonesia perlu mengumpulkan, menggunakan, mengungkapkan dan/atau memproses Data Pribadi saya. Data Pribadi tersebut mencakup informasi tentang saya yang tercantum dalam formulir dan dokumen aplikasi saya dan informasi pribadi lainnya yang diberikan oleh saya, yang dimiliki oleh PT Straits Futures Indonesia atau yang diperoleh PT Straits Futures Indonesia tentang saya, baik sekarang maupun di masa mendatang. Diantaranya, nama saya, nomor identifikasi, alamat, kontak pribadi, transaksi saya, dll.</p>
                    <p className="mt-5">2. Saya setuju bahwa kegagalan saya untuk memberikan Data Pribadi tertentu kepada PT Straits Futures Indonesia (atau jika PT Straits Futures Indonesia tidak diizinkan oleh saya untuk memproses Data Pribadi saya), maka akan mengakibatkan PT Straits Futures Indonesia tidak dapat memproses, mengelola dan/atau mengelola rekening transaksi saya dengan PT Straits Futures Indonesia. Apabila saya tidak mengizinkan PT Straits Futures Indonesia untuk memproses Data Pribadi saya atau sejauh mana Data Pribadi yang diperlukan namun saya tidak berikan kepada PT Straits Futures Indonesia, maka PT Straits Futures Indonesia tidak akan dapat mengelola rekening transaksi saya dengan PT Straits Futures Indonesia.</p>
                    <p className="mt-5">3. Saya setuju bahwa PT Straits Futures Indonesia akan/dapat mengumpulkan, menggunakan, mengungkapkan dan/atau mengolah Data Pribadi saya untuk satu atau lebih tujuan berikut ini:
                        <ul>
                            <li className="mt-2">3.1. mempertimbangkan dan/atau memproses aplikasi pembukaan rekening transaksi dengan PT Straits Futures Indonesia;</li>
                            <li className="mt-2">3.2. memfasilitasi, memproses, berurusan dengan, mengelola, mengelola dan/atau memelihara rekening transaksi saya dengan PT Straits Futures Indonesia, termasuk namun tidak terbatas pada pemutakhiran Data Pribadi saya, melaksanakan instruksi saya sehubungan dengan transaksi saya dengan PT Straits Futures Indonesia, memproses amanat saya, memproses pembayaran yang dilakukan ke dan dari rekening transaksi saya;</li>
                            <li className="mt-2">3.3. melaksanakan instruksi saya atau menanggapi setiap pertanyaan yang diberikan oleh (atau dimaksudkan untuk diberikan oleh) saya;</li>
                            <li className="mt-2">3.4. menghubungi saya atau berkomunikasi dengan saya melalui, termasuk namun tidak terbatas pada, telepon/panggilan suara, pesan teks dan/atau pesan faks, WhatsApp, email dan/atau surat pos untuk keperluan memfasilitasi, memproses, menangani, mengelola dan/atau mengelola rekening transaksi saya dengan PT Straits Futures Indonesia seperti, namun tidak terbatas pada, pengiriman Laporan Perdagangan Harian, dan/atau File Konfirmasi Perdagangan/Ringkasan dan pemberitahuan konfirmasi berkenaan dengan memperbarui Data Pribadi saya. Saya mengakui dan setuju bahwa komunikasi tersebut oleh PT Straits Futures Indonesia baik melalui surat, korespondensi, dokumen atau pemberitahuan kepada saya, yang dapat melibatkan pengungkapan Data Pribadi tertentu tentang saya untuk menghasilkan pengiriman yang sama maupun pada sampul luar amplop/surat;</li>
                            <li className="mt-2">3.5. menangani segala hal yang berkaitan dengan layanan yang menjadi hak saya berdasarkan rekening terpisah saya dengan PT Straits Futures Indonesia;</li>
                            <li className="mt-2">3.6. melakukan uji tuntas atau kegiatan penyaringan lainnya (termasuk anti pencucian uang, know your customer (KYC), pemeriksaan kredit dan latar belakang) sesuai dengan hukum, peraturan perundang-undangan yang berlaku, atau prosedur manajemen risiko PT Straits Futures Indonesia yang mungkin diwajibkan oleh hukum atau yang diberlakukan oleh PT Straits Futures Indonesia;</li>
                            <li className="mt-2">3.7. mencegah atau menyelidiki kecurangan, aktivitas yang melanggar hukum atau kelalaian atau kesalahan, baik yang berkaitan dengan rekening transaksi dengan PT Straits Futures Indonesia atau masalah lain yang timbul dari rekening transaksi dengan PT Straits Futures Indonesia, dan apakah ada atau tidak ada kecurigaan atas hal-hal tersebut di atas;</li>
                            <li className="mt-2">3.8. mematuhi atau sebagaimana disyaratkan oleh hukum yang berlaku, peraturan regulator atau peraturan dari yurisdiksi yang berwenang, termasuk memenuhi persyaratan untuk melakukan pengungkapan berdasarkan persyaratan hukum apa pun yang mengikat PT Straits Futures Indonesia dan/atau untuk tujuan pedoman apa pun yang dikeluarkan oleh regulator atau pihak berwenang lainnya, apakah di Indonesia atau di negara lain, di mana PT Straits Futures Indonesia diharapkan untuk mematuhinya;</li>
                            <li className="mt-2">3.9. mematuhi atau sebagaimana diminta oleh otoritas pemerintah mana pun; atau menanggapi permintaan informasi dari lembaga publik, kementerian, atau otoritas serupa lainnya (termasuk namun tidak terbatas pada Bappebti). Untuk menghindari keraguan, ini berarti bahwa PT Straits Futures Indonesia dapat/akan mengungkapkan Data Pribadi saya kepada pihak-pihak tersebut atas permintaan atau arahan mereka;</li>
                            <li className="mt-2">3.10. melakukan kegiatan penelitian, analisa dan pengembangan (termasuk tetapi tidak terbatas pada analisa data, survei, dan/atau pembuatan profil) untuk meningkatkan layanan dan fasilitas PT Straits Futures Indonesia untuk kepentingan rekening transaksi saya dengan PT Straits Futures Indonesia, atau untuk meningkatkan layanan PT Straits Futures Indonesia untuk kepentingan saya;</li> 
                            <li className="mt-2">3.11. menyimpan, hosting, mencadangkan (baik untuk pemulihan bencana atau lainnya) dari Data Pribadi saya, baik di dalam atau di luar Indonesia; dan</li>
                            <li className="mt-2">3.12. jika disetujui oleh saya, memberikan saya informasi pemasaran, periklanan dan promosi, materi dan/atau dokumen yang berkaitan dengan produk dan/atau layanan disediakan oleh PT Straits Futures Indonesia (termasuk produk dan/atau layanan dari pihak ketiga yang bekerja sama atau berkolaborasi dengan PT Straits Futures Indonesia) bahwa PT Straits Futures Indonesia dapat menjual, memasarkan, menawarkan atau mempromosikan, (apakah produk atau layanan tersebut telah ada atau akan dibuat di masa depan) yang menurut pendapat PT Straits Futures Indonesia mungkin menarik atau bermanfaat bagi saya (&quot;Tujuan Pemasaran&quot;) melalui, termasuk namun tidak terbatas pada, telepon/panggilan suara, pesan teks dan/atau pesan faks, WhatsApp, email dan/atau surat pos,</li>
                        </ul>
                    </p>
                    <p className="mt-5">4. PT Straits Futures Indonesia dapat/akan mengumpulkan dari sumber selain dari saya, Data Pribadi tentang saya, untuk satu atau lebih Tujuan di atas, dan setelah itu menggunakan, mengungkapkan dan/atau memproses Data Pribadi  tersebut untuk satu atau lebih Tujuan di atas.</p>
                    <p className="mt-5">5. PT Straits Futures Indonesia mungkin/akan perlu mengungkapkan Data Pribadi saya kepada pihak ketiga, baik yang berlokasi di dalam atau di luar Indonesia, untuk satu atau lebih Tujuan di atas, karena pihak ketiga tersebut, akan memproses Data Pribadi saya untuk satu atau lebih dari Tujuan di atas. Dalam hal ini, Nasabah dengan ini mengakui, menyetujui dan menyetujui bahwa PT Straits Futures Indonesia dapat/diizinkan untuk mengungkapkan Data Pribadi saya kepada pihak ketiga tersebut (baik berlokasi di dalam maupun di luar Indonesia) untuk satu atau lebih Tujuan di atas dan untuk pihak ketiga tersebut, untuk selanjutnya mengumpulkan, menggunakan, mengungkapkan dan/atau mengolah Data Pribadi saya untuk atau lebih dari Tujuan di atas. Tanpa membatasi sifat umum dari ketentuan di atas atau klausul ini, pihak ketiga tersebut meliputi:
                        <ul>
                            <li className="mt-2">5.1. setiap entitas yang terkait dengan PT Straits Futures Indonesia;</li>
                            <li className="mt-2">5.2. perantara, agen, atau penyedia layanan pihak ketiga yang memproses atau akan memproses Data Pribadi saya atas nama PT Straits Futures Indonesia termasuk tetapi tidak terbatas pada mereka yang menyediakan layanan administrasi atau lainnya untuk PT Straits Futures Indonesia seperti kantor pos, perusahaan telekomunikasi, perusahaan teknologi informasi dan pusat data;</li>
                            <li className="mt-2">5.3. setiap pemerintah atau pihak berwenang di Indonesia dan negara lain di mana pengungkapan diperlukan oleh hukum yang berlaku;</li>
                            <li className="mt-2">5.4. Auditor dan penasihat hukum PT Straits Futures Indonesia; dan / atau</li>
                            <li className="mt-2">5.5. penyedia atau agen layanan pihak ketiga, yang mungkin berlokasi di atau di luar Indonesia, untuk Tujuan pemasaran di atas; dan saya juga menyetujui penyedia layanan pihak ketiga atau agen PT Straits Futures Indonesia yang memproses Data Pribadi saya (termasuk mengirimkan kepada saya informasi pemasaran, periklanan dan promosi, materi dan/atau dokumen melalui pos, transmisi elektronik ke, termasuk namun tidak terbatas pada, email, panggilan suara/panggilan telepon, WhatsApp, SMS/MMS dan/atau faks) untuk Tujuan pemasaran di atas untuk PT Straits Futures Indonesia,</li>
                        </ul>
                    </p>
                    <p className="mt-5">6. Saya dapat meminta untuk mengakses dan/atau mengoreksi Data Pribadi saya yang saat ini dimiliki atau dikendalikan oleh PT Straits Futures Indonesia dengan mengirimkan permintaan tertulis kepada PT Straits Futures Indonesia. Silakan kirimkan permintaan tertulis kepada Petugas Perlindungan Data PT Straits Futures Indonesia di perincian kontak yang tercantum di bawah ini.</p>
                    <p className="mt-5">7. Saya dapat menarik persetujuan ini untuk pengumpulan, penggunaan dan/atau pengungkapan Data Pribadi saya dalam kepemilikan PT Straits Futures Indonesia atau di bawah kendali PT Straits Futures Indonesia dengan mengirimkan permintaan saya kepada Petugas Perlindungan Data PT Straits Futures Indonesia di rincian kontak yang tercantum di bawah ini.</p>
                    <p className="mt-5">8. PT Straits Futures Indonesia akan melakukan upaya yang wajar untuk memastikan bahwa Data Pribadi saya akurat dan lengkap, jika Data Pribadi saya kemungkinan akan digunakan oleh PT Straits Futures Indonesia untuk membuat  keputusan yang mempengaruhi saya, atau diungkapkan kepada organisasi lain. Namun, ini berarti bahwa saya juga harus emperbarui PT Straits Futures Indonesia dari setiap perubahan dalam Data Pribadi saya yang pada awalnya diberikan oleh PT Straits Futures Indonesia kepada saya. PT Straits Futures Indonesia tidak akan bertanggung jawab untuk mengandalkan Data Pribadi yang tidak akurat atau tidak lengkap yang timbul dari saya yang tidak memperbarui PT Straits Futures Indonesia dari setiap perubahan dalam Data Pribadi saya yang pada awalnya diberikan oleh saya kepada PT Straits Futures Indonesia.</p>
                    <p className="mt-5">9. PT Straits Futures Indonesia juga akan menerapkan pengaturan keamanan yang wajar untuk memastikan bahwa Data Pribadi saya dilindungi dan diamankan secara memadai. Pengaturan keamanan yang sesuai akan diambil untuk mencegah akses, pengumpulan, penggunaan, pengungkapan, penyalinan, modifikasi, kebocoran, kehilangan, kerusakan, dan/atau perubahan yang tidak sah dari saya.</p>
                    <p className="mt-5">10. PT Straits Futures Indonesia juga akan memberlakukan tindakan sedemikian rupa sehingga Data Pribadi saya yang dimiliki PT Straits Futures Indonesia atau di bawah kendali PT Straits Futures Indonesia dihancurkan dan / atau dianonimkan segera setelah beralasan untuk menganggap bahwa (i) tujuan pengumpulan Data Pribadi tidak lagi dilayani oleh penyimpanan Data Pribadi tersebut; dan (ii) penyimpanan tidak lagi diperlukan untuk tujuan hukum atau bisnis lainnya.</p>
                    <p className="mt-5">11. Di mana Data Pribadi saya akan ditransfer ke luar Indonesia, PT Straits Futures Indonesia akan mematuhi Peraturan Perlindungan Data Pribadi dalam melakukan hal tersebut, termasuk tanpa batasan mengadakan perjanjian kontrak yang mengikat dengan organisasi penerima yang mengharuskan organisasi penerima untuk menyediakan data pribadi yang ditransfer dengan suatu standar perlindungan yang setidaknya sebanding dengan yang ada di bawah Peraturan Perlindungan Data Pribadi.</p>
                    <p className="mt-5">12. Jika saya memiliki pertanyaan tentang bagaimana PT Straits Futures Indonesia menangani Data Pribadi saya atau tentang bagaimana PT Straits Futures Indonesia mematuhi Peraturan Perlindungan Data Pribadi, saya dapat menghubungi PT Straits Futures Indonesia.</p>
                    <p className="mt-5">13. Nasabah dapat menghubungi PT Straits Futures Indonesia melalui salah satu media berikut ini dengan:
                        <ul>
                            <li className="mt-2">a. No. Tel. : +62 21 5050 8877</li>
                            <li className="mt-2">b. E-mail : cs.id@straitsfinancial.com</li>                            
                            <li className="mt-2">c. Alamat : Gold Coast Office, Eiffel Tower, Level 1 Unit C, Jl. Pantai Indah Kapuk, Jakarta</li>
                        </ul>
                    </p>
                    <p className="mt-5">Saya telah membaca, mengerti dan setuju terhadap semua ketentuan yang tercantum dalam PERSETUJUAN PENGGUNAAN DATA PRIBADI ini</p>
                    <p className="mt-5">“Dengan mengisi kolom “Ya” di bawah, saya menyatakan bahwa saya telah menerima “PERSETUJUAN PENGGUNAAN DATA PRIBADI”, serta mengerti dan menyetujui isinya.</p>
                    <p className="mt-5">Pernyataan menerima / Tidak</p>
                    <p className="mt-2">Ya</p>
                    <p className="mt-5">Pernyataan pada tanggal:</p>
                    <p className="mt-2">{localState.curr_date.date} - {localState.curr_date.month} - {localState.curr_date.year}</p>
                </div>  

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
