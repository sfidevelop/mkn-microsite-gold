import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPage6(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const control_experience = (e) => {
        let objLocalState = localState;
        objLocalState.account.experience = e.target.value;
        if (e.target.value == "N") {
            objLocalState.account.field_of_trading = "Others";
            objLocalState.account.years_experience = "0";
        }
        else {
            objLocalState.account.field_of_trading = "";
            objLocalState.account.years_experience = "";
        }
        setLocalState({...objLocalState, ...localState});
    }
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(7);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(5)}>
                            <Image src="/img/back.webp" 
                                height="20"
                                width="20"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">6/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <form onSubmit={(e)=>saveAccount(e)}>                
                    <label>Apakah Anda Memiliki Pengalaman Investasi</label>
                    <div className="border rounded py-1 px-3 mb-2">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.experience ?? ""}
                            onInput={(e) => control_experience(e)}
                            required={true}>
                            <option value="">Pilih</option>
                            <option value="Y">Ya</option>
                            <option value="N">Tidak</option>
                        </select>
                    </div>

                    {localState.account.experience == "Y" &&
                    <div>
                        <label>Jenis Investasi</label>
                        <div className="mb-2 border rounded py-1 px-3">
                            <select className="w-full bg-transparent py-3"
                                disabled={localState.account.experience == "N" ? true : false}
                                value={localState.account.field_of_trading ?? ""}
                                onInput={(e) => setLocalState({...localState, account:{...localState.account, field_of_trading:e.target.value}})}
                                required={true}>
                                <option value="">Pilih</option>
                                <option value="Crypto">Crypto</option>
                                <option value="Stock">Saham</option>
                                <option value="Futures">Futures</option>
                                <option value="Others">Lainnya</option>
                            </select>
                        </div>
                    </div>
                    }

                    <label>Pengalaman Investasi (Tahun)</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        {/* 这里需不需要改为数字类型，而不是电话类型 */}
                        <input type="tel"
                            disabled={localState.account.experience == "N" ? true : false}
                            placeholder="Lama tahun pengalaman investasi"
                            className="w-full py-3"
                            maxLength={4}
                            value={localState.account.years_experience ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, years_experience:e.target.value}})}
                            required={true} />
                    </div>


                    <label>Sumber Dana</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.source ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, source:e.target.value}})}
                            required={true}>
                            <option value="">Pilih</option>
                            <option value="Salary">Gaji</option>
                            <option value="Business Profit">Keuntungan Perusahaan</option>
                            <option value="Others">Lainnya</option>
                        </select>
                    </div>

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            onClick={()=>props.changePage(5)}
                            type="button">
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
