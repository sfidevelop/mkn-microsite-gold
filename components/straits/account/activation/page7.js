import React, {useState, useEffect} from "react";
import Image from 'next/image';
import * as API from "services/api";

export default function StraitsAccountActivationPage7(props) {
    const initialLocalState = () => {
        return {
            account: {},
            banks: []
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getBanksCode = async() => {
        try {
            var response = await API.Get('/straits/bank', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;            
            objLocalState.banks = response.data.banks;
            setLocalState({...objLocalState, ...localState}); 
        }
    }
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});

        getBanksCode();
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(6)}>
                            <Image src="/img/back.webp" 
                                height="20"
                                width="20"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">7/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">                
                <form onSubmit={(e)=>saveAccount(e)}>
                <label>Nomor Rekening Bank</label>
                        <div className="border rounded py-1 px-3 mb-2">
                        <input type="tel"
                            placeholder="Nomor Rekening Bank"
                            className="w-full py-3"
                            maxLength={20}
                            value={localState.account.bank_account ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, bank_account:e.target.value}})}
                            required={true} />
                    </div>

                    <label>Nama Pemilik Rekening(sesuai KTP)</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="text"
                            placeholder="Nama Pemilik Rekening Bank"
                            className="w-full py-3"
                            maxLength={50}
                            value={localState.account.bank_owner ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, bank_owner:e.target.value}})}
                            required={true} />
                    </div>

                    <label>Nama Bank</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.bank ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, bank:e.target.value}})}
                            required={true}>
                            <option value="">Pilih</option>
                            {localState.banks.map((item,index) => {
                                return (
                                    <option key={index}
                                        value={item.code}>{item.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    
                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            onClick={()=>props.changePage(6)}
                            type="button">
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
