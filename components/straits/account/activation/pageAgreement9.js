import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement9(props) {
    const initialLocalState = () => {
        return {
            account: {},
            curr_date: {
                day: 0,
                date: 1,
                month: 1,
                year: 2023
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement9 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let d = new Date();
        let day = d.getDay();
        let date = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear();
        let array_day = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        let array_mon = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        let objLocalState = localState;
        objLocalState.account = props.account;
        objLocalState.curr_date = {
            day : array_day[day],
            date: date,
            month: array_mon[month],
            year: year
        };
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="text-center subtitle1">
                        S&K
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">PERATURAN PERDAGANGAN (TRADING RULES)</div>
                <div className="mt-5">
                    <p>1. Dalam rangka pembukaan akun transaksi, maka Nasabah wajib mendaftarkan dirinya ke PT Straits Futures Indonesia dengan membaca, memahami dan mengerti Dokumen Pemberitahuan Adanya Risiko dan Perjanjian Pemberian Amanat dan dokumen-dokumen pendukung lainnya serta mengisi Aplikasi Pembukaan Rekening Transaksi.</p>
                    <p className="mt-5">2. Proses pembukaan akun transaksi Nasabah dilakukan paling lambat dalam 1 x 24 jam.</p>
                    <p className="mt-5">3. Transaksi hanya dapat dilakukan oleh Nasabah apabila akun transaksi Nasabah telah dibuka dan dana untuk transaksi tersebut telah diterima oleh PT Straits Futures Indonesia dalam rekening terpisahnya (good fund) di PT Kliring Berjangka Indonesia (Persero) dan terkredit dalam sistem perdagangan yang disediakan oleh PT Straits Futures Indonesia.</p>
                    <p className="mt-5">4. Penerimaan Dana</p>
                    <p className="mt-2">a. Dana nasabah yang dimaksud dalam poin (3) diterima dalam bentuk IDR (Indonesia Rupiah).</p>
                    <p className="mt-2">b. Dana yang diterima akan dikurangi biaya-biaya pihak ketiga sebelum dikonversi ke dalam USD (US Dollar) sesuai dengan kurs yang berlaku.</p>
                    <p className="mt-2">c. Dana yang sudah dikonversikan ke dalam USD akan dikreditkan ke rekening transaksi Nasabah.</p>
                    <p className="mt-2">d. Untuk produk-produk yang mempunyai denominasi di luar USD, profit &amp; loss akan diperhitungkan dalam mata uang asli produk tersebut, yang selanjutnya akan dikonversikan ke USD untuk dikreditkan/didebitkan ke rekening transaksi Nasabah sesuai dengan kurs yang berlaku.</p>
                    <p className="mt-5">5. Penarikan Dana
                        <ul>
                            <li className="mt-2">a. Penarikan dana oleh Nasabah hanya dapat difasilitasi setelah mendapat instruksi dari Nasabah.</li>
                            <li className="mt-2">b. Nasabah menetapkan jumlah dana dari rekening transaksi yang akan ditarik (withdrawal
amount).</li>
                            <li className="mt-2">c. Withdrawal amount akan dikonversikan ke dalam IDR sesuai dengan kurs yang berlaku.</li>
                            <li className="mt-2">d. Dana hasil konversi dalam IDR tersebut akan ditransferkan ke rekening Bank Nasabah yang terdaftar di PT Straits Futures Indonesia, setelah dikurang biaya-biaya pihak ketiga.</li>
                            <li className="mt-2">e. Penarikan dana Nasabah diproses secara otomatis bagi penarikan dana dengan jumlah sama dengan dan kurang dari Rp.25.000.000,- (dua puluh lima juta Rupiah).</li>
                            <li className="mt-2">f. Penarikan dana Nasabah diproses secara manual bagi penarikan dana dengan jumlah lebih dari Rp.25.000.000,- (dua puluh lima juta Rupiah).</li>
                        </ul>
                    </p>
                    <p className="mt-5">6. Spesifikasi kontrak atas produk-produk yang diperdagangkan mengacu pada Lampiran A Spesifikasi Kontrak sebagaimana terlampir dalam Peraturan Perdagangan (Trading Rules) ini. Perubahan dan/atau penambahan produk oleh PT Straits Futures Indonesia dapat dilakukan di kemudian hari dan akan menyampaikan perubahan dan/atau penambahan tersebut kepada Nasabah.</p>
                    <p className="mt-5">7. Peraturan Perdagangan Produk:
                        <ul>
                            <li className="mt-2">a. Nasabah dapat melakukan transaksi pembelian produk sebesar dana yang tersedia di dalam rekening transaksi Nasabah.</li>
                            <li className="mt-2">b. Jenis amanat yang dapat dilakukan Nasabah adalah market order.</li>
                            <li className="mt-2">c. Nilai transaksi adalah nilai notional penuh dari produk yang dibeli.</li>
                            <li className="mt-2">d. Saldo dana tunai Nasabah akan didebit sebesar nilai transaksi.</li>
                            <li className="mt-2">e. Produk yang dimiliki dan saldo dana tunai yang dimiliki Nasabah ditampilkan secara real-time.</li>
                            <li className="mt-2">f. Nasabah hanya dapat menjual produk yang sudah dimiliki.</li>
                            <li className="mt-2">g. Produk yang telah dimiliki Nasabah dapat dijual kembali untuk mendapatkan dana tunai yang akan dikreditkan ke rekening transaksi Nasabah.</li>
                        </ul>
                    </p>
                    <p className="mt-5">“Dengan mengisi kolom “Ya” di bawah, saya menyatakan bahwa saya telah menerima PERATURAN PERDAGANGAN (TRADING RULES)”, serta mengerti dan menyetujui isinya.</p>
                    <p className="mt-5">Pernyataan menerima / Tidak</p>
                    <p className="mt-2">Ya</p>
                    <p className="mt-5">Pernyataan pada tanggal:</p>
                    <p className="mt-2">{localState.curr_date.date} - {localState.curr_date.month} - {localState.curr_date.year}</p>
                </div>     

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
