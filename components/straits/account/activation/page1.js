import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';

export default function StraitsAccountActivationPage1(props) {    
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(2);
    }

    const [today, setToday] = useState(new Date().toISOString().split('T')[0]);
    const maxDate = new Date();
    maxDate.setFullYear(maxDate.getFullYear() - 17);
    const minDate = new Date();
    minDate.setFullYear(minDate.getFullYear() - 100);
    const [maxDateStr, setMaxDateStr] = useState(maxDate.toISOString().split('T')[0]);
    const [minDateStr, setMinDateStr] = useState(minDate.toISOString().split('T')[0]);
    const [errorMessageDate, setErrorMessageDate] = useState('');
    const [abletoregdate, setAbletoregdate] = useState(false);
    const handleChangeDate = (e) => {
        if(e.target.value > maxDateStr || e.target.value < minDateStr) {
            setErrorMessageDate('Tanggal Lahir invlaid, masukkan Tanggal yang Benar.');
            setAbletoregdate(false);
        } 
        else {
            setErrorMessageDate('');
            setAbletoregdate(true);
        }

    };
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">            
                    <div className="text-left">
                        <Link href="/logined/index">
                            <a>
                                <Image src="/img/back.webp" 
                                    height="20"
                                    width="20"
                                    alt="back" />
                            </a>
                        </Link>                        
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">1/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <form onSubmit={(e)=>saveAccount(e)}>     
                    <label>Nama Lengkap(sesuai KTP)</label>               
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="text"
                            maxLength={50}
                            placeholder="Nama Lengkap"
                            className="w-full py-3"
                            disabled={true}
                            value={localState.account.fullname ?? ""} />
                    </div>

                    <label>Tempat Lahir</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="text"
                            placeholder="Cth: Jakarta"
                            maxLength={50}
                            required={true}
                            className="w-full py-3"
                            value={localState.account.pob ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, pob:e.target.value}})} />
                    </div>
                
                        <label>Tanggal Lahir</label>
                        <div className="font-bold ml-1 text-red-500">{errorMessageDate}</div>
                        <div className="mb-2 border rounded py-1 px-3">
                            <input type="date" 
                                placeholder="Masukan Tanggal Lahir"
                                className="w-full py-3" 
                                
                                value={localState.account.dob ?? ""}
                                required={true}
                                min={minDateStr}
                                max={maxDateStr}
                                onInput={(e) => {setLocalState({...localState, account:{...localState.account, dob:e.target.value}});
                                handleChangeDate(e);}} />
                        </div>          

                    <label>Jenis Kelamin</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <select className="w-full bg-white py-3"
                            value={localState.account.gender ?? ""}
                            disabled={true}>
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="male">Pria</option>
                            <option value="female">Wanita</option>
                        </select>
                    </div>

                    <label>Email</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="email"
                            placeholder="Alamat Email"
                            className="w-full py-3"
                            value={localState.account.email ?? ""}
                            disabled={true} />
                    </div>

                    <div className="mt-5">
                        <div className="float-left pr-2">
                            <Image src="/img/index.png" 
                                height="24"
                                width="24"
                                alt="Kayya" />
                        </div>
                        <div className="color-gray text-justify"
                            style={{fontSize:8}}>
                            Kayya difasilitasi oleh PT Straits Futures Indonesia, Pialang Berjangka yang diawasi oleh Bappebti berdasarkan izin nomor 43/BAPPEBTI/SI/09/2015. Transaksi Anda akan diregistrasikan ke Jakarta Futures Exchange dan dijamin penyelesaiannya oleh PT Kliring Berjangka Indonesia (Persero)
                        </div>
                    </div>

                    <div className="mt-5">
                        <button className="button-primary w-full"                        
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>        
    )
}
