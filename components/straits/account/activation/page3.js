import React, {useState, useEffect} from "react";
import Image from 'next/image';
import * as API from "services/api";
import Select from 'react-select'

export default function StraitsAccountActivationPage3(props) {
    const initialLocalState = () => {
        return {
            control_domicile_address: false,
            account: {},
            regencies: [],
            regency_options:[]
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());

    // the input code related variables
    const [ableCKtp, setAbleCKtp] = useState(false);
    const [ktpCodeError, setKtpCodeError] = useState('');

    // the input Kode Pos related variables
    const [ableCPosKode, setAbleCPosKode] = useState(true);
    const [posKodeError, setPosKodeError] = useState('');
    const handle_checkbox = (e) => {
        let objLocalState = localState;        
        objLocalState.control_domicile_address = e.target.checked;
        if (e.target.checked) {
            objLocalState.account.domicile_address = localState.account.ktp_address;
            objLocalState.account.domicile_postalcode = localState.account.ktp_postalcode;
        }
        else {
            objLocalState.account.domicile_address = "";
            objLocalState.account.domicile_postalcode = "";
        }
        setLocalState({...objLocalState, ...localState});
    }
    const getRegencyCode = async() => {
        try {
            var response = await API.Get('/straits/regency', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`
            );
        }
        catch(error) {    
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            response.data.regencies.map((item) => {
                objLocalState.regency_options.push({
                    "value": item.code,
                    "label": item.regency
                })
            })
            setLocalState({...objLocalState, ...localState}); 
        }
    }
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(4);
    }
    const handleInputCode = (e) => {
        const inputCode = e;
        if(/^\d{5}$/.test(inputCode)) {
            setAbleCKtp(true);
            setKtpCodeError('');
        } else{
            setKtpCodeError('Harus 5 digits!');
            setAbleCKtp(false);
        }
    }

    const handleInputPosKode = (e) => {
        const inputPos = e;
        if(/^\d{5}$/.test(inputPos)){
            setAbleCPosKode(true);
            setPosKodeError('');
        } else {
            setAbleCPosKode(false);
            setPosKodeError('Harus 5 digits!');
        }
    }

    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});

        getRegencyCode();
        if(objLocalState.account.ktp_postalcode){
            handleInputCode(objLocalState.account.ktp_postalcode);
        }
        if(objLocalState.account.domicile_postalcode){
            handleInputPosKode(objLocalState.account.domicile_postalcode);
        }
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(2)}>
                            <Image src="/img/back.webp" 
                                height="20"
                                width="20"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">3/8</div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <form onSubmit={(e)=>saveAccount(e)}>
                <label>Status Perkawinan</label>
                    <div className="border rounded py-1 px-3 mb-2">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.marital_status ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, marital_status:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Status Perkawinan</option>
                            <option value="Single">Belum Menikah</option>
                            <option value="Married">Menikah</option>
                            <option value="Widower">Janda/Duda</option>
                        </select>
                    </div>

                    {localState.account.marital_status == "Married" &&
                    <div>
                        <label>Nama Pasangan</label>
                        <div className="mb-2 border rounded py-1 px-3">
                            <input type="text"
                                placeholder="Nama Pasangan"
                                required={true}
                                className="w-full py-3"
                                maxLength={50}
                                value={localState.account.spouse ?? ""}
                                onInput={(e) => setLocalState({...localState, account:{...localState.account, spouse:e.target.value}})} />
                        </div>
                    </div>
                    }

                    <label>Alamat Rumah (sesuai KTP)</label>
                    <div className="border rounded py-1 px-3 mb-2">
                        <textarea placeholder="Alamat Rumah"
                            className="w-full"
                            maxLength={125}
                            required={true}
                            value={localState.account.ktp_address ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, ktp_address:e.target.value}})} />
                    </div>

                    <label>Kode Regensi (sesuai KTP)</label>
                    <Select options={localState.regency_options}
                        className="w-full bg-transparent border-hidden rounded py-1 mb-2"
                        onChange={(e) => setLocalState({...localState, account:{...localState.account, ktp_regency:e.value}})} />    

                    {/* 在这里改pos kode */}
                    <label>Kode Pos (sesuai KTP)</label>
                    <div className="font-bold ml-1 text-red-500">{ktpCodeError}</div>
                    <div className="border rounded py-1 px-3 mb-2">
                        <input type="number"
                            placeholder="Kode Pos"
                            className="w-full py-3"
                            value={localState.account.ktp_postalcode ?? ""}
                            onInput={(e) => {setLocalState({...localState, account:{...localState.account, ktp_postalcode:e.target.value}});handleInputCode(e.target.value);}}
                            required={true} />
                    </div>

                    <label>Status Kepemilikan Rumah</label>
                    <div className="border rounded py-1 px-3 mb-2">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.house_ownership ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, house_ownership:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Status Kepemilikan</option>
                            <option value="Owned">Milik sendiri</option>
                            <option value="Family House">Milik orang tua</option>
                            <option value="Rent">Kontrak</option>
                        </select>
                    </div>

                    <label>Alamat Domisili<br></br></label>
                    <label>
                        <input type="checkbox"                        
                            className="mt-2 mb-3 mr-3"
                            checked={localState.control_domicile_address ? "checked" : ""}
                            onChange={(e) => handle_checkbox(e)} />
                            <label>Sama dengan alamat rumah</label>
                    </label> 
                    <div className="border rounded py-1 px-3  mb-2">
                        <textarea placeholder="Alamat Domisili"
                            className="w-full"
                            maxLength={125}
                            value={localState.account.domicile_address ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, domicile_address:e.target.value}})}
                            required={true} />
                    </div>

                    <label>Kode Pos Domisili</label>
                    <div className="font-bold ml-1 text-red-500">{posKodeError}</div>
                    <div className="border rounded py-1 px-3  mb-2">
                        <input type="number"
                            placeholder="Kode Pos Domisili"
                            className="w-full py-3"
                            value={localState.account.domicile_postalcode ?? ""}
                            onInput={(e) => {setLocalState({...localState, account:{...localState.account, domicile_postalcode:e.target.value}});handleInputPosKode(e.target.value);}}
                            required={true} />
                    </div>         

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            type="button"
                            onClick={()=>props.changePage(2)}>
                            Kembali
                        </button>
                        <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                            type="submit" disabled={!ableCKtp || !ableCPosKode}>
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
