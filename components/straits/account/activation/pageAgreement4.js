import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement4(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement4 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="text-center subtitle1">
                        S&K
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">PEMBERITAHUAN ADANYA RISIKO</div>
                <div className="mt-5">
                    <p>Dokumen Pemberitahuan Adanya Risiko ini disampaikan kepada Anda sesuai dengan Pasal 50 ayat (2) Undang-Undang Nomor 32 Tahun 1997 tentang Perdagangan Berjangka Komoditi sebagaimana telah diubah dengan Undang-Undang Nomor 10 Tahun 2011 tentang Perubahan Atas Undang-Undang Nomor 32 Tahun 1997 Tentang Perdagangan Berjangka Komoditi.</p>
                    <p className="mt-2">Maksud dokumen ini adalah memberitahukan bahwa kemungkinan kerugian atau keuntungan dalam perdagangan Kontrak Berjangka bisa mencapai jumlah yang sangat besar. Oleh karena itu, Anda harus berhati-hati dalam memutuskan untuk melakukan transaksi, apakah kondisi keuangan Anda mencukupi.</p>
                    <p className="mt-2">1. Perdagangan Kontrak Berjangka belum tentu layak bagi semua investor. Anda dapat menderita kerugian dalam jumlah besar dan dalam jangka waktu singkat. Jumlah kerugian uang dimungkinkan dapat melebihi jumlah uang yang pertama kali Anda setor (Margin awal) ke Pialang Berjangka Anda. Anda mungkin menderita kerugian seluruh Margin dan Margin tambahan yang ditempatkan pada Pialang Berjangka untuk mempertahankan posisi Kontrak Berjangka Anda. Hal ini disebabkan Perdagangan Berjangka sangat dipengaruhi oleh mekanisme leverage, di mana dengan jumlah investasi dalam bentuk yang relatif kecil dapat digunakan untuk membuka posisi dengan aset yang bernilai jauh lebih tinggi. Apabila Anda tidak siap dengan risiko seperti ini, sebaiknya Anda tidak melakukan perdagangan kontrak Berjangka.</p>
                    <p className="mt-2">2. Perdagangan Kontrak Berjangka mempunyai risiko dan mempunyai kemungkinan kerugian yang tidak terbatas yang jauh lebih besar dari jumlah uang yang disetor (Margin) ke Pialang Berjangka. Kontrak Berjangka sama dengan produk keuangan lainnya yang mempunyai risiko tinggi, Anda sebaiknya tidak menaruh risiko terhadap dana yang Anda tidak siap untuk menderita rugi, seperti tabungan pensiun, dana kesehatan atau dana untuk keadaan darurat, dana yang disediakan untuk pendidikan atau kepemilikan rumah, dana yang diperoleh dari pinjaman pendidikan atau gadai, atau dana yang digunakan untuk memenuhi kebutuhan sehari-hari.</p>
                    <p className="mt-2">3. Berhati-hatilah terhadap pernyataan bahwa Anda pasti mendapatkan keuntungan besar dari perdagangan Kontrak Berjangka. Meskipun perdagangan Kontrak Berjangka dapat memberikan keuntungan yang besar dan cepat, namun hal tersebut tidak pasti, bahkan dapat menimbulkan kerugian yang besar dan cepat juga. Seperti produk keuangan lainnya, tidak ada yang dinamakan&quot;pasti untung&quot;.</p>
                    <p className="mt-2">4. Disebabkan adanya mekanisme leverage dan sifat dari transaksi Kontrak Berjangka, Anda dapat merasakan dampak bahwa anda menderita kerugian dalam waktu cepat. Keuntungan maupun kerugian dalam transaksi Kontrak Berjangka akan langsung dikredit atau didebet ke akun Anda, paling lambat secara harian. Apabila pergerakan di pasar terhadap Kontrak Berjangka menurunkan nilai posisi Anda dalam Kontrak Berjangka, Anda diwajibkan untuk menambah dana untuk pemenuhan kewajiban Margin ke Pialang Berjangka. Apabila akun Anda berada di bawah minimum Margin yang telah ditetapkan Lembaga Kliring Berjangka atau Pialang Berjangka, maka posisi Anda dapat dilikuidasi pada saat rugi, dan Anda wajib menyelesaikan defisit (jika ada) dalam akun Anda.</p>
                    <p className="mt-2">5. Pada saat pasar dalam keadaan tertentu, Anda mungkin akan sulit atau tidak mungkin melikuidasi posisi. Pada umumnya Anda harus melakukan transaksi offset jika ingin melikuidasi posisi dalam Kontrak Berjangka. Apabila Anda tidak dapat melikuidasi posisi Kontrak Berjangka, Anda tidak dapat merealisasikan keuntungan pada nilai posisi tersebut atau mencegah kerugian yang lebih tinggi. Kemungkinan tidak dapat melikuidasi dapat terjadi, antara lain: jika perdagangan berhenti dikarenakan aktivitas perdagangan yang tidak lazim pada Kontrak Berjangka atau Subjek Kontrak Berjangka, terjadi kerusakan sistem pada Bursa Berjangka atau Pialang Berjangka, atau posisi Anda berada dalam pasar yang tidak likuid. Bahkan apabila Anda dapat melikuidasi posisi tersebut, Anda mungkin terpaksa melakukannya pada harga yang menimbulkan kerugian besar.</p>
                    <p className="mt-2">6. Pada saat pasar dalam keadaan tertentu, Anda mungkin akan sulit atau tidak mungkin mengelola risiko atas posisi terbuka Kontrak Berjangka dengan cara membuka posisi dengan nilai yang sama namun dengan posisi yang berlawanan dalam kontrak bulan yang berbeda, dalam pasar yang berbeda, atau dalam &quot;Subjek Kontrak Berjangka&quot; yang berbeda. Kemungkinan untuk tidak dapat mengambil posisi dalam rangka membatasi risiko yang timbul, contohnya: jika perdagangan dihentikan pada pasar yang berbeda disebabkan aktivitas perdagangan yang tidak lazim pada Kontrak Berjangka atau &quot;Subjek Kontrak Berjangka&quot;.</p>
                    <p className="mt-2">7. Anda dapat diwajibkan untuk menyelesaikan Kontrak Berjangka dengan penyerahan fisik dari &quot;Subjek Kontrak Berjangka&quot;. Jika Anda mempertahankan posisi penyelesaian fisik dalam Kontrak Berjangka sampai hari terakhir perdagangan berdasarkan tanggal jatuh tempo Kontrak berjangka, Anda akan diwajibkan menyerahkan atau menerima penyerahan &quot;Subjek Kontrak Berjangka&quot; yang dapat mengakibatkan adanya penambahan biaya. Pengertian penyelesaian dapat berbeda untuk suatu Kontrak Berjangka dengan Kontrak Berjangka lainnya atau suatu Bursa Berjangka dengan Bursa Berjangka lainnya. Anda harus melihat secara teliti mengenai penyelesaian dan kondisi penyerahan sebelum membeli atau menjual Kontrak Berjangka.</p>
                    <p className="mt-2">8. Anda dapat menderita kerugian yang disebabkan kegagalan sistem transaksi elektronik. Sebagaimana yang terjadi pada setiap transaksi keuangan, Anda dapat menderita kerugian jika amanat untuk melaksanakan transaksi Kontrak Berjangka tidak dapat dilakukan karena kegagalan sistem transaksi elektronik/informasi di Bursa Berjangka, penyelenggara maupun sistem sistem transaksi elektronik/informasi di Pialang Berjangka yang mengelola posisi Anda. Kerugian Anda akan semakin besar jika Pialang Berjangka yang mengelola posisi Anda tidak memiliki sistem transaksi elektronik/informasi cadangan atau prosedur yang layak.</p>
                    <p className="mt-2">9. Semua Kontrak Berjangka mempunyai risiko, dan tidak ada strategi berdagang yang dapat menjamin untuk menghilangkan risiko tersebut. Strategi dengan menggunakan kombinasi posisi seperti spread, dapat sama berisiko seperti posisi long atau short. Melakukan Perdagangan Berjangka memerlukan pengetahuan mengenai Kontrak Berjangka dan pasar berjangka.</p>
                    <p className="mt-2">10. Strategi perdagangan harian dalam Kontrak Berjangka dan produk lainnya memiliki risiko khusus. Seperti pada produk keuangan lainnya, pihak yang ingin membeli atau menjual Kontrak Berjangka yang sama dalam satu hari untuk mendapat keuntungan dari perubahan harga pada hari tersebut (&quot;day traders”) akan memiliki beberapa risiko tertentu antara lain jumlah komisi yang besar, risiko terkena efek pengungkit (&quot;exposure to leverage&quot;), dan persaingan dengan pedagang profesional. Anda harus mengerti risiko tersebut dan memiliki pengalaman yang memadai sebelum melakukan perdagangan harian (&quot;day trading&quot;).</p>
                    <p className="mt-2">11. Menetapkan amanat bersyarat, seperti Kontrak Berjangka dilikuidasi pada keadaan tertentu untuk membatasi rugi (stop loss), mungkin tidak akan dapat membatasi kerugian Anda sampai jumlah tertentu saja. Amanat bersyarat tersebut mungkin tidak dapat dilaksanakan karena terjadi kondisi pasar yang tidak memungkinkan melikuidasi Kontrak Berjangka.</p>
                    <p className="mt-2">12. Anda harus membaca dengan seksama dan memahami Perjanjian Pemberian Amanat dengan Pialang Berjangka Anda sebelum melakukan transaksi Kontrak Berjangka.</p>
                    <p className="mt-2">13. Pernyataan singkat ini tidak dapat memuat secara rinci seluruh risiko atau aspek penting lainnya tentang Perdagangan Berjangka. Oleh karena itu Anda harus mempelajari kegiatan Perdagangan Berjangka secara cermat sebelum memutuskan melakukan transaksi.</p>
                    <p className="mt-2">14. Dokumen Pemberitahuan Adanya Risiko (Risk Disclosure) ini dibuat dalam Bahasa Indonesia.</p>
                </div>
                <div className="mt-5 font-bold">PERNYATAAN MENERIMA PEMBERITAHUAN ADANYA RISIKO</div>
                <div className="mt-2">Dengan mengisi kolom “Ya” di bawah, saya menyatakan bahwa saya telah menerima</div>
                <div className="mt-2">“DOKUMEN PEMBERITAHUAN ADANYA RISIKO”</div>
                <div className="mt-2">mengerti dan menyetujui isinya.</div>
                <div className="mt-5">Pernyataan menerima / Tidak</div>
                <div className="mt-2">Ya</div>

                <div className="mt-5">Pernyataan pada tanggal</div>
                <div className="mt-2">{new Date().toLocaleDateString()}</div>   

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
