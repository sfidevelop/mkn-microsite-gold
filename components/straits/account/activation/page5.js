import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPage5(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(6);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(4)}>
                            <Image src="/img/back.webp" 
                                height="20"
                                width="20"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">5/8</div>
                </div>
            </div>
            <div className="sm:container mx-auto p-4 custom-pt-70">                
                <form onSubmit={(e)=>saveAccount(e)}>
                    <label>Pekerjaan</label>
                    <div className="border rounded py-1 px-3 mb-2">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.occupation ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, occupation:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Pekerjaan</option>                  
                            <option value="private_employee">Karyawan Swasta</option>
                            <option value="government_employee">Pegawai Negri</option>
                            <option value="student">Pelajar/Mahasiswa</option>
                            <option value="enterpreneur">Wiraswasta</option>
                            <option value="others">Lainnya</option>
                        </select>
                    </div>

                    <label>Jabatan</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.job_title ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, job_title:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Jabatan</option>
                            <option value="Owner">Pemilik Usaha</option>                        
                            <option value="Director">Direktur</option>
                            <option value="Manager">Manager</option>
                            <option value="Executive">Eksekutif</option>                        
                            <option value="Others">Lainnya</option>
                        </select>
                    </div>

                    <label>Perusahaan</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="text"
                            placeholder="Perusahaan"
                            className="w-full py-3"
                            maxLength={50}
                            value={localState.account.company_name ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, company_name:e.target.value}})}
                            required={true} />
                    </div>

                    <label>Bidang Usaha</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.company_lob ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, company_lob:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Bidang Usaha</option>
                            <option value="Financial">Finansial</option>                        
                            <option value="Technology">Teknologi</option>
                            <option value="Hospitality">Pelayanan Jasa</option>
                            <option value="Logistics">Logistik</option>
                            <option value="Property">Properti</option>
                            <option value="Energy">Energi</option>
                            <option value="Education">Edukasi</option>
                            <option value="Retail">Retail</option>
                            <option value="Consulting">Konsultasi</option>
                            <option value="Others">Lainnya</option>
                        </select>
                    </div>

                    <label>Lama Bekerja (Tahun)</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="tel"
                            placeholder="Lama tahun bekerja"
                            className="w-full py-3"
                            maxLength={4}
                            value={localState.account.years_of_work ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, years_of_work:e.target.value}})}
                            required={true} />
                    </div>

                    <label>Alamat Perusahaan</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <textarea placeholder="Alamat Perusahaan"
                            className="w-full py-3"
                            maxLength={125}
                            value={localState.account.company_address ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, company_address:e.target.value}})}
                            required={true}>
                        </textarea>
                    </div>

                    <label>Penghasilan Tahunan</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <select className="w-full bg-transparent py-3"
                            value={localState.account.income ?? ""}
                            onChange={(e) => setLocalState({...localState, account:{...localState.account, income:e.target.value}})}
                            required={true}>
                            <option value="">Pilih Penghasilan</option>
                            <option value="1">0 - 50 Juta</option>                        
                            <option value="500000000">50 Juta - 100 Juta</option>
                            <option value="100000000">100 Juta - 250 Juta</option>
                            <option value="250000000">250 Juta - 500 Juta</option>                        
                            <option value="500000000">500 Juta - 1 Miliar</option>
                            <option value="1000000000">Diatas 1 Miliar</option>
                        </select>                        
                    </div>

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            onClick={()=>props.changePage(4)}
                            type="button">
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
