import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement8(props) {
    const initialLocalState = () => {
        return {
            account: {},
            curr_date: {
                day: 0,
                date: 1,
                month: 1,
                year: 2023
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement8 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let d = new Date();
        let day = d.getDay();
        let date = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear();
        let array_day = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        let array_mon = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        let objLocalState = localState;
        objLocalState.account = props.account;
        objLocalState.curr_date = {
            day : array_day[day],
            date: date,
            month: array_mon[month],
            year: year
        };
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="text-center subtitle1">
                        S&K
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">OTORISASI AUTO-KONVERSI MATA UANG UNTUK TRANSAKSI KONTRAK BERJANGKA DI BURSA BERJANGKA LUAR NEGERI YANG MEMBERLAKUKAN CONTROLLED CURRENCY</div>
                <div className="mt-5">
                    <p>Yang mengisi formulir di bawah ini:</p>
                    <p className="mt-5">
                        Nama Lengkap : {localState.account.fullname}<br/>
                        Tempat & Tanggal Lahir : {localState.account.pob}, {new Date(localState.account.dob).toLocaleDateString()}<br/>
                        Alamat : {localState.account.ktp_address}<br/>
                        No. KTP/Passport : {localState.account.ktp}
                    </p>
                    <p className="mt-5">Dalam perdagangan kontrak berjangka di bursa berjangka luar negeri yang menggunakan controlled currency, Saya dengan ini mengetahui, menerima dan menyetujui bahwa:</p>
                    <p className="mt-5">1. Pialang Berjangka memiliki kebijaksanaan mutlak untuk menerima Dolar US atau mata uang lainnya sebagaimana ditentukan oleh Pialang Berjangka dari waktu ke waktu sebagai margin untuk transaksi pada bursa berjangka luar negeri yang memberlakukan Controlled Currency.</p>
                    <p className="mt-5">2. Pialang Berjangka memiliki wewenang (tetapi tidak berkewajiban) setiap saat dan dari waktu ke waktu, tanpa pemberitahuan terlebih dahulu kepada Saya, untuk melakukan konversi sebagian atau seluruh dana dalam rekening Saya pada Pialang Berjangka ke dalam Controlled Currency yang ditransaksikan pada bursa berjangka yang bersangkutan (pada nilai konversi yang diperoleh Pialang Berjangka dari pihak ketiga yang bekerja sama dengan Pialang Berjangka dimana Pialang Berjangka memiliki kebijaksanaan mutlak untuk melakukan transaksi mata uang) yang bertujuan untuk:</p>
                    <p className="mt-5">a. Off-setting kekurangan saldo dalam Controlled Currency pada rekening Saya;</p>
                    <p className="mt-5">b. Mengkompensasi realized losses dan biaya-biaya yang berkaitan dengan rekening transaksi Saya; dan atau:</p>
                    <p className="mt-5">c. Memenuhi persyaratan margin seperti yang telah ditetapkan oleh Pialang Berjangka dari waktu ke waktu sehubungan dengan transaksi yang Saya lakukan melalui Pialang Berjangka di bursa berjangka yang memberlakukan Controlled Currency.</p>
                    <p className="mt-5">3. Pialang Berjangka tidak memiliki kewajiban apapun dan tidak wajib melakukan konversi ke dalam mata uang lain pada rekening transaksi Saya yang timbul karena alasan apapun (termasuk dari konversi yang dilakukan oleh Pialang Berjangka berdasarkan ayat 2 di atas). Pialang Berjangka mungkin, tapi tidak wajib untuk menginformasikan kepada Saya mengenai setiap saldo lebih dari Controlled Currency pada rekening transaksi Saya dan Saya menyetujui bahwa hal tersebut merupakan tanggung jawab Saya untuk memeriksa laporan transaksi harian dan/atau bertanya kepada Pialang Berjangka dari waktu ke waktu apakah Saya memiliki saldo lebih dari Controlled Currency pada rekening transaksi Saya. Saya menyadari risiko yang timbul dari fluktuasi mata uang dan menyetujui untuk bertanggung jawab atas kerugian yang Saya alami akibat adanya fluktuasi mata uang.</p>
                    <p className="mt-5">4. Pernyataan Otorisasi ini merupakan ketentuan tambahan dan tanpa mengurangi ketentuan-ketentuan yang terdapat dalam Perjanjian Pemberian Amanat (&quot;Perjanjian&quot;), yang Saya telah setujui. Dalam hal terjadi perselisihan atau inkonsistensi di antara ketentuan otorisasi ini dan ketentuan di dalam Perjanjian, maka ketentuan dalam pernyataan otorisasi ini yang dijadikan acuan. lstilah &quot;Controlled Currency&quot;, untuk keperluan formulir otorisasi ini, mengacu pada mata uang suatu negara di mana mata uang tersebut tidak dapat dikonversi secara bebas di luar negara dari mata uang tersebut.</p>
                    <p className="mt-5">“Saya telah membaca, mengerti dan setuju terhadap semua ketentuan yang tercantum dalam OTORISASI AUTO-KONVERSI MATA UANG UNTUK TRANSAKSI KONTRAK BERJANGKA DI BURSA BERJANGKA LUAR NEGERI YANG MEMBERLAKUKAN CONTROLLED CURRENCY ini”.</p>
                    <p className="mt-5">“Dengan mengisi kolom “Ya” di bawah, saya menyatakan bahwa saya telah menerima “OTORISASI AUTO-KONVERSI MATA UANG UNTUK TRANSAKSI KONTRAK BERJANGKA DI BURSA BERJANGKA LUAR NEGERI YANG MEMBERLAKUKAN CONTROLLED CURRENCY”, serta mengerti dan menyetujui isinya.</p>
                    <p className="mt-5">Pernyataan menerima / Tidak</p>
                    <p className="mt-2">Ya</p>
                    <p className="mt-5">Pernyataan pada tanggal:</p>
                    <p className="mt-2">{localState.curr_date.date} - {localState.curr_date.month} - {localState.curr_date.year}</p>
                </div>         

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
