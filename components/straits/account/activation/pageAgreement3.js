import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement3(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement3 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="text-center subtitle1">
                        S&K
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">APLIKASI PEMBUKAAN REKENING TRANSAKSI</div>
                <div className="mt-5">
                    <p className="font-bold">PERNYATAAN PENGUNGKAPAN</p>
                    <p className="mt-2">
                        1. Perdagangan Berjangka BERISIKO SANGAT TINGGI tidak cocok untuk semua orang. Pastikan bahwa anda SEPENUHNYA MEMAHAMI RISIKO ini sebelum melakukan perdagangan. 
                    </p>
                    <p className="mt-2">
                        2. Perdagangan Berjangka merupakan produk keuangan dengan leverage dan dapat menyebabkan KERUGIAN ANDA MELEBIHI setoran awal Anda. Anda harus siap apabila SELURUH DANA ANDA HABIS. 
                    </p>
                    <p className="mt-2">
                        3. TIDAK ADA PENDAPATAN TETAP (FIXED INCOME) dalam Perdagangan Berjangka. 
                    </p>
                    <p className="mt-2">
                        4. Apabila anda PEMULA kami sarankan untuk mempelajari mekanisme transaksinya, PERDAGANGAN BERJANGKA membutuhkan pengetahuan dan pemahaman khusus. 
                    </p>
                    <p className="mt-2">
                        5. ANDA HARUS MELAKUKAN TRANSAKSI SENDIRI, segala risiko yang akan timbul akibat transaksi sepenuhnya akan menjadi tanggung jawab Saudara.
                    </p>
                    <p className="mt-2">
                        6. user id dan password BERSIFAT PRIBADI DAN RAHASIA, anda bertanggung jawab atas penggunaannya, JANGAN SERAHKAN ke pihak lain terutama Wakil Pialang Berjangka dan pegawai Pialang Berjangka. 
                    </p>
                    <p className="mt-2">
                        7. ANDA berhak menerima LAPORAN ATAS TRANSAKSI yang anda lakukan. Waktu anda 2 X 24 JAM UNTUK MEMBERIKAN SANGGAHAN. Untuk transaksi yang TELAH SELESAI (DONE/SETTLE) DAPAT ANDA CEK melalui sistem informasi transaksi nasabah yang berfungsi untuk memastikan transaksi anda telah terdaftar di Lembaga Kliring Berjangka.
                    </p>
                </div>  

                <div className="mt-5 font-bold">APLIKASI PEMBUKAAN REKENING TRANSAKSI</div>
                <div className="mt-2">Data Pribadi</div>
                <div className="mt-2">Nama Lengkap : {localState.account.fullname}</div>
                <div className="mt-2">Tempat &amp; Tanggal Lahir : {localState.account.pob} {localState.account.dob}</div>
                <div className="mt-2">No. KTP : {localState.account.ktp}</div>
                <div className="mt-2">No. NPWP : {localState.account.npwp}</div>
                <div className="mt-2">Jenis Kelamin : {localState.account.gender}</div>
                <div className="mt-2">Alamat Sesuai KTP : {localState.account.ktp_address}</div>
                <div className="mt-2">Alamat Sekarang : {localState.account.domicile_address}</div>
                <div className="mt-2">Status Perkawinan : {localState.account.marital_status}</div>
                <div className="mt-2">Nama Istri/Suami : {localState.account.spouse}</div>
                <div className="mt-2">Nama Ibu Kandung : {localState.account.mother_name}</div>
                <div className="mt-2">No. Tel. Selular : {localState.account.handphone}</div>
                <div className="mt-2">Email : {localState.account.email}</div>
                <div className="mt-2">Status Kepemilikan Rumah : {localState.account.house_ownership}</div>
                <div className="mt-2">Tujuan Pembukaan Rekening : Investasi</div>
                <div className="mt-2">Pengalaman Investasi : {localState.account.experience}</div>
                <div className="mt-2">Bidang Investasi : {localState.account.field_of_trading}</div>
                <div className="mt-2">Apakah Anda Memiliki anggota keluarga di Bappebti/Bursa Berjangka/Kliring Berjangka : N</div>
                <div className="mt-2">Apakah Anda telah dinyatakan pailit oleh Pengadilan : N</div>

                <div className="mt-5 font-bold">Pihak yang Dihubungi dalam Keadaan Darurat</div>
                <div className="mt-2">Nama Lengkap : {localState.account.emergency_contact_name}</div>
                <div className="mt-2">No. Tel. Selular : {localState.account.emergency_contact_handphone}</div>
                <div className="mt-2">Hubungan dengan Anda : {localState.account.emergency_contact_relation}</div>

                <div className="mt-5 font-bold">Pekerjaan</div>
                <div className="mt-2">Pekerjaan : {localState.account.occupation}</div>
                <div className="mt-2">Nama Perusahaan : {localState.account.company_name}</div>
                <div className="mt-2">Bidang Usaha : {localState.account.company_lob}</div>
                <div className="mt-2">Jabatan : {localState.account.job_title}</div>
                <div className="mt-2">Lama Bekerja : {localState.account.years_of_work} Tahun</div>
                <div className="mt-2">Alamat Kantor : {localState.account.company_address}</div>

                <div className="mt-5 font-bold">Daftar Kekayaan</div>
                <div className="mt-2">Penghasilan per Tahun  : Rp. {localState.account.income}</div>
                <div className="mt-2">Sumber Dana Transaksi  : {localState.account.source}</div>

                <div className="mt-5 font-bold">Rekening Bank Nasabah Untuk Penyetoran dan Penarikan Margin</div>
                <div className="mt-2">Hanya Rekening di bawah ini yang dapat Saudara pergunakan untuk lalu lintas dana transaksi</div>
                <div className="mt-2">Nama Bank : {localState.account.bank}</div>
                <div className="mt-2">Nomor Rekening Bank : {localState.account.bank_account}</div>
                <div className="mt-2">Nama Pemilik Rekening Bank : {localState.account.bank_owner}</div>

                <div className="mt-5">Pernyataan Kebenaran dan Tanggung Jawab</div>
                <div className="mt-2">Dengan mengisi kolom “Ya” di bawah ini, saya menyatakan bahwa semua informasi dan semua dokumen
yang saya lampirkan dalam Aplikasi Pembukaan Rekening Transaksi adalah benar dan tepat, saya akan
bertanggung jawab penuh apabila di kemudian hari terhadi sesuatu hal sehubungan dengan
ketidakbenaran data dan informasi yang saya berikan.</div>

                <div className="mt-5">Pernyataan menerima / Tidak</div>
                <div className="mt-2">Ya</div>

                <div className="mt-5">Pernyataan pada tanggal</div>
                <div className="mt-2">{new Date().toLocaleDateString()}</div>

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
