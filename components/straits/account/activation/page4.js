import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPage4(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const [errorMessage, setErrorMessage] = useState('');
    const [abletoregphone, setAbletoregphone] = useState(false);

    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(5);
    }
    
    const handlePhone = (e) => {
        const handphone = e.replace(/\D/g, '');
        const consecutivePattern = /(.)\1{5,}/; // 匹配连续6位相同的数字
        if (handphone.length < 9 || consecutivePattern.test(handphone)) {
            setErrorMessage('Periksa kembali nomor ponsel Anda');
            setAbletoregphone(false);
        }
        else{
            setErrorMessage('');
            setAbletoregphone(true);
        }
    };
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});

        if(localState.account.emergency_contact_handphone){
            handlePhone(localState.account.emergency_contact_handphone);
        }
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(3)}>
                            <Image src="/img/back.webp" 
                                height="20"
                                width="20"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">4/8</div>
                </div>
            </div>
            <div className="sm:container mx-auto p-4 custom-pt-70">        
                <div className="subtitle1 mb-5">
                    Informasi Kontak Darurat
                </div>

                <form onSubmit={(e)=>saveAccount(e)}>
                    <label>Nama</label>
                    <div className="border rounded py-1 px-3 mb-2">
                        <input type="text"
                            placeholder="Nama"
                            maxLength={50}
                            className="w-full py-3"
                            value={localState.account.emergency_contact_name ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, emergency_contact_name:e.target.value}})}
                            required={true} />
                    </div>

                    <label>Nomor Handphone</label>
                    <div className="font-bold ml-1 text-red-500">{errorMessage}</div>
                    <div className="grid grid-cols-12 items-center">
                    <span className="font-bold p-1">+62</span>
                        <input type="tel"
                            placeholder="Cth: 8xxxxxxxx"
                            maxLength={15}
                            className="border p-3 rounded col-span-11"
                            value={localState.account.emergency_contact_handphone ?? ""}
                            onInput={(e) => {setLocalState({...localState, account:{...localState.account, emergency_contact_handphone:e.target.value}});handlePhone(e.target.value);}}
                            required={true} />
                    </div>

                    <label>Hubungan</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="text"
                            placeholder="Hubungan"
                            maxLength={20}
                            className="w-full py-3"
                            value={localState.account.emergency_contact_relation ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, emergency_contact_relation:e.target.value}})}
                            required={true} />
                    </div>

                    <label>Nama Gadis Ibu Kandung</label>
                    <div className="mb-2 border rounded py-1 px-3">
                        <input type="text"
                            placeholder="Nama Gadis Ibu Kandung"
                            className="w-full py-3"
                            maxLength={50}
                            value={localState.account.mother_name ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, mother_name:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            onClick={()=>props.changePage(3)}
                            type="button">
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit"
                            disabled={!abletoregphone}>
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
