import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement7(props) {
    const initialLocalState = () => {
        return {
            account: {},
            curr_date: {
                day: 0,
                date: 1,
                month: 1,
                year: 2023
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement7 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let d = new Date();
        let day = d.getDay();
        let date = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear();
        let array_day = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        let array_mon = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        
        let objLocalState = localState;
        objLocalState.account = props.account;
        objLocalState.curr_date = {
            day : array_day[day],
            date: date,
            month: array_mon[month],
            year: year
        };
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="text-center subtitle1">
                        S&K
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">PERNYATAAN BAHWA DANA YANG DIGUNAKAN ADALAH MILIK NASABAH SENDIRI</div>
                <div className="mt-5">
                    <p>Yang mengisi formulir di bawah ini:</p>
                    <p className="mt-5">
                        Nama Lengkap : {localState.account.fullname}<br/>
                        Tempat & Tanggal Lahir : {localState.account.pob}, {new Date(localState.account.dob).toLocaleDateString()}<br/>
                        Alamat : {localState.account.ktp_address}<br/>
                        No. KTP/Passport : {localState.account.ktp}
                    </p>
                    <p className="mt-5">Dengan mengisi kolom “Ya” di bawah ini, saya menyatakan bahwa dana yang saya gunakan untuk bertransaksi di PT Straits Futures Indonesia adalah milik saya pribadi dan bukan dana pihak lain, serta tidak diperoleh dari hasil kejahatan, penipuan, penggelapan, tindak pidana korupsi, tindak pidana narkotika, tindak pidana di bidang kehutanan, hasil pencucian uang, dan perbuatan melawan hukum lainnya serta dimaksudkan untuk melakukan pencucian uang dan/atau pendanaan terorisme.</p>
                    <p className="mt-5">Demikian Pernyataan Bahwa Dana Yang Digunakan Adalah Milik Nasabah Sendiri ini dibuat dengan sebenarnya dalam keadaan sadar, sehat jasmani dan rohani serta tanpa ada paksaan apapun dari pihak manapun.</p>
                    <p className="mt-5">Pernyataan menerima / Tidak</p>
                    <p className="mt-2">Ya</p>
                    <p className="mt-5">Pernyataan pada tanggal:</p>
                    <p className="mt-2">{localState.curr_date.date} - {localState.curr_date.month} - {localState.curr_date.year}</p>
                </div>  

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
