import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPageAgreement5(props) {
    const initialLocalState = () => {
        return {
            account: {},
            curr_date: {
                day: 0,
                date: 1,
                month: 1,
                year: 2023
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = () => {
        let objLocalState = localState;
        objLocalState.account.agreement5 = "true";
        setLocalState({...objLocalState, ...localState});

        props.saveAccount(localState.account);
        props.changePage(8);
    }
    useEffect(() => {
        let d = new Date();
        let day = d.getDay();
        let date = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear();
        let array_day = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        let array_mon = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        let objLocalState = localState;
        objLocalState.account = props.account;
        objLocalState.curr_date = {
            day : array_day[day],
            date: date,
            month: array_mon[month],
            year: year
        };
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center"> 
                    <div>
                        <button onClick={()=>props.changePage(8)}>
                            <Image src="/img/back.png" 
                                height="12"
                                width="6"
                                alt="back" />
                        </button>
                    </div>
                
                    <div className="text-center subtitle1">
                        S&K
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center font-bold">PERJANJIAN PEMBERIAN AMANAT</div>
                <div className="mt-5">
                    <p>1. Dalam menyajikan Dokumen Perjanjian Pemberian Amanat secara elektronik Online untuk transaksi Kontrak Berjangka, Pialang Berjangka wajib berpedoman pada formulir dokumen Perjanjian Pemberian Amanat secara elektronik Online untuk transaksi Kontrak Berjangka;</p>
                    <p className="mt-2">2. Seluruh data isian wajib diisi sendiri oleh Nasabah, Nasabah bertanggung jawab atas kebenaran informasi yang diberikan dalam mengisi dokumen ini:</p>
                    <p className="mt-2">3. Perjanjian Pemberian Amanat ini, wajib terdapat fitur pilihan tempat dalam rangka penyelesaian perselisihan yakni melalui Badan Arbitrase. Perdagangan Berjangka Komoditi (BAKTI) atau Pengadilan Negeri;</p>
                    <p className="mt-2">4. Perjanjian Pemberian Amanat ini, wajib terdapat fitur pilihan kantor atau kantor cabang Pialang Berjangka terdekat dengan domisili Nasabah untuk pelayanan pengaduan;</p>
                    <p className="mt-2">5. Pada akhir Perjanjian Pemberian Amanat ini, wajib terdapat kalimat “Dengan mengisi kolom &quot;Ya&quot; di bawah, saya menyatakan bahwa saya telah menerima &quot;PERJANJIAN PEMBERIAN AMANAT&quot; mengerti dan menyetujui isinya”.  ini membuktikan bahwa Nasabah telah membaca dan menyetujui seluruh isi Perjanjian ini, dan berlaku secara sah kepada kedua belah Pihak.</p>
                    <p className="mt-2">Pada hari ini {localState.curr_date.day}, tanggal {localState.curr_date.date} bulan {localState.curr_date.month}. tahun {localState.curr_date.year}, kami yang mengisi perjanjian
di bawah ini:</p>
                </div>
                <div className="flex mt-5">
                    <div>1.</div>
                    <div className="ml-3">
                        <div>Nama : {localState.account.fullname}</div>
                        <div className="mt-2">Pekerjaan / Jabatan : {localState.account.occupation} / {localState.account.job_title}</div>
                        <div className="mt-2">Alamat : {localState.account.domicile_address}</div>
                    </div>
                </div>
                <div className="mt-5">dalam hal ini bertindak untuk dan atas nama sendiri, yang selanjutnya disebut dengan Nasabah,</div>
                <div className="flex mt-5">
                    <div>2.</div>
                    <div className="ml-3">
                        <div>Nama : Andriana</div>
                        <div className="mt-2">Pekerjaan / Jabatan : Petugas Wakil Pialang yang Ditunjuk untuk Memverifikasi</div>
                        <div className="mt-2">Alamat : Gold Coast Office, Eiffel Tower, Level 1 Unit C, Jl. Pantai Indah Kapuk, Kel. Kamal Muara, Kec. Penjaringan, Jakarta 14470</div>
                    </div>
                </div>
                <div className="mt-5">
                    <p>dalam hal ini bertindak untuk dan atas nama PT Straits Futures Indonesia yang selanjutnya disebut Pialang Berjangka.</p>
                    <p className="mt-2">Nasabah dan Pialang Berjangka secara bersama-sama selanjutnya disebut Para Pihak.</p>
                    <p className="mt-2">Para Pihak sepakat untuk mengadakan Perjanjian Pemberian Amanat untuk melakukan transaksi penjualan maupun pembelian Kontrak Berjangka dengan ketentuan sebagai berikut:</p>                    
                </div>
                <div className="mt-5">
                    <div className="font-bold">1. Margin dan Pembayaran Lainnya</div>
                    <p className="mt-2">(1) Nasabah menempatkan sejumlah dana (Margin) ke Rekening Terpisah (Segregated Account) Pialang Berjangka sebagai Margin awal dan wajib mempertahankannya sebagaimana ditetapkan.</p>
                    <p className="mt-2">(2) Membayar biaya-biaya yang diperlukan untuk transaksi yaitu biaya transaksi, pajak, komisi, dan biaya bunga sesuai tingkat yang berlaku, dan biaya lainnya yang dapat dipertanggungjawabkan berkaitan dengan transaksi sesuai amanat Nasabah, maupun biaya akun Nasabah.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">2. Pelaksanaan Amanat</div>
                    <p className="mt-2">(1) Setiap amanat yang disampaikan oleh Nasabah atau kuasanya yang ditunjuk secara tertulis oleh Nasabah, dianggap sah apabila diterima oleh Pialang Berjangka sesuai dengan ketentuan yang berlaku, dapat berupa amanat tertulis yang ditandatangani oleh Nasabah atau kuasanya, amanat telepon yang direkam, dan/atau amanat transaksi elektronik lainnya.</p>
                    <p className="mt-2">(2) Setiap amanat Nasabah yang diterima dapat langsung dilaksanakan sepanjang nilai Margin yang tersedia pada akunnya mencukupi dan eksekusinya tergantung pada kondisi dan sistem transaksi yang berlaku yang mungkin dapat menimbulkan perbedaan waktu terhadap proses pelaksanaan amanat tersebut. Nasabah harus mengetahui posisi Margin dan posisi terbuka sebelum memberikan amanat untuk transaksi berikutnya.</p>
                    <p className="mt-2">(3) Amanat Nasabah hanya dapat dibatalkan dan/atau diperbaiki apabila transaksi atas amanat tersebut belum terjadi. Pialang Berjangka tidak bertanggung jawab atas kerugian yang timbul akibat tidak terlaksananya pembatalan dan/atau perbaikan sepanjang bukan karena kelalaian Pialang Berjangka.</p>
                    <p className="mt-2">(4) Pialang Berjangka berhak menolak amanat Nasabah apabila harga yang ditawarkan atau diminta tidak wajar.</p>
                    <p className="mt-2">(5) Nasabah bertanggung jawab atas keamanan dan penggunaan username dan password dalam transaksi Perdagangan Berjangka, oleh karenanya Nasabah dilarang memberitahukan, menyerahkan atau meminjamkan username dan password kepada pihak lain, termasuk pegawai Pialang Berjangka.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">3. Antisipasi Penyerahan Barang</div>
                    <p className="mt-2">(1) Untuk kontrak-kontrak tertentu penyelesaian transaksi dapat dilakukan dengan penyerahan atau penerimaan barang (delivery) apabila kontrak jatuh tempo. Nasabah menyadari bahwa penyerahan atau penerimaan barang mengandung risiko yang lebih besar daripada melikuidasi posisi dengan offset. Penyerahan fisik barang memiliki konsekuensi kebutuhan dana yang lebih besar serta tambahan biaya pengelolaan barang.</p>
                    <p className="mt-2">(2) Pialang Berjangka tidak bertanggung jawab atas klasifikasi mutu (grade), kualitas atau tingkat toleransi atas komoditi yang diserahkan atau akan diserahkan.</p>
                    <p className="mt-2">(3) Pelaksanaan penyerahan atau penerimaan barang tersebut akan diatur dan dijamin oleh Lembaga Kliring Berjangka.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">4. Kewajiban Memelihara Margin</div>
                    <p className="mt-2">(1) Nasabah wajib memelihara/memenuhi tingkat Margin yang harus tersedia di rekening pada Pialang Berjangka sesuai dengan jumlah yang telah ditetapkan baik diminta ataupun tidak oleh Pialang Berjangka.</p>
                    <p className="mt-2">(2) Apabila jumlah Margin memerlukan penambahan maka Pialang Berjangka akan memberitahukan dan memintakan kepada Nasabah untuk menambah Margin segera.</p>
                    <p className="mt-2">(3) Apabila jumlah Margin memerlukan tambahan (Call Margin) maka Nasabah wajib melakukan penyerahan Call Margin selambat-lambatnya sebelum dimulai hari perdagangan berikutnya. Kewajiban Nasabah sehubungan dengan penyerahan Call Margin tidak terbatas pada jumlah Margin awal.</p>
                    <p className="mt-2">(4) Pialang Berjangka tidak berkewajiban melaksanakan amanat untuk melakukan transaksi yang baru dari Nasabah sebelum Call Margin dipenuhi.</p>
                    <p className="mt-2">(5) Untuk memenuhi kewajiban Call Margin dan keuangan lainnya dari Nasabah, Pialang Berjangka dapat mencairkan dana Nasabah yang ada di Pialang Berjangka.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">5. Hak Pialang Berjangka Melikuidasi Posisi Nasabah</div>
                    <p className="mt-2">Nasabah bertanggung jawab memantau/mengetahui posisi terbukanya secara terus-menerus dan memenuhi kewajibannya. Apabila dalam jangka waktu tertentu dana pada akun Nasabah kurang dari yang dipersyaratkan, Pialang Berjangka dapat menutup posisi terbuka Nasabah secara keseluruhan atau sebagian, membatasi transaksi, atau tindakan lain untuk melindungi diri dalam pemenuhan Margin tersebut dengan terlebih dahulu memberitahu atau tanpa memberitahu Nasabah dan Pialang Berjangka tidak bertanggung jawab atas kerugian yang timbul akibat tindakan tersebut.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">6. Penggantian Kerugian Tidak Menyerahkan Barang</div>
                    <p className="mt-2">Apabila Nasabah tidak mampu menyerahkan komoditi atas Kontrak Berjangka yang jatuh tempo, Nasabah memberikan kuasa kepada Pialang Berjangka untuk meminjam atau membeli komoditi untuk penyerahan tersebut. Nasabah wajib membayar secepatnya semua biaya, kerugian dan premi yang telah dibayarkan oleh Pialang Berjangka atas tindakan tersebut. Apabila Pialang Berjangka harus menerima penyerahan komoditi atau surat berharga maka Nasabah bertanggung jawab atas penurunan nilai dari komoditi atas surat berharga tersebut.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">7. Penggantian Kerugian Tidak Adanya Penutupan Posisi</div>
                    <p className="mt-2">Apabila Nasabah tidak mampu melakukan penutupan atas transaksi yang jatuh tempo, Pialang Berjangka dapat melakukan penutupan atas transaksi di Bursa. Nasabah wajib membayar biaya-biaya, termasuk biaya kerugian dan premi yang telah dibayarkan oleh Pialang Berjangka, dan apabila Nasabah lalai untuk membayar biaya-biaya tersebut, Pialang Berjangka berhak untuk mengambil pembayaran dari dana Nasabah.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">8. Pialang Berjangka Dapat Membatasi Posisi</div>
                    <p className="mt-2">Nasabah mengakui hak Pialang Berjangka untuk membatasi posisi terbuka Kontrak Berjangka Nasabah dan Nasabah tidak melakukan transaksi melebihi batas yang telah ditetapkan tersebut.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">9. Tidak Ada Jaminan atas lnformasi atau Rekomendasi</div>
                    <p className="mt-2">Nasabah mengakui bahwa:</p>
                    <p className="mt-2">(1) lnformasi dan rekomendasi yang diberikan oleh Pialang Berjangka kepada Nasabah tidak selalu lengkap dan perlu diverifikasi.</p>
                    <p className="mt-2">(2) Pialang Berjangka tidak menjamin bahwa informasi dan rekomendasi yang diberikan merupakan informasi yang akurat dan lengkap.</p>
                    <p className="mt-2">(3) lnformasi dan rekomendasi yang diberikan oleh Wakil Pialang Berjangka yang satu dengan yang lain mungkin berbeda karena perbedaan analisa fundamental atau teknikal. Nasabah menyadari bahwa ada kemungkinan Pialang Berjangka dan pihak terafiliasinya memiliki posisi di pasar dan memberikan rekomendasi tidak konsisten kepada Nasabah.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">10. Pembatasan Tanggung Jawab Pialang Berjangka</div>
                    <p className="mt-2">(1) Pialang Berjangka tidak bertanggung jawab untuk memberikan penilaian kepada Nasabah mengenai iklim, pasar, keadaan politik dan ekonomi nasional dan internasional, nilai kontrak berjangka, kolateral, atau memberikan nasihat mengenai keadaan pasar. Pialang Berjangka hanya memberikan pelayanan untuk melakukan transaksi secara jujur serta memberikan laporan atas transaksi tersebut.</p>
                    <p className="mt-2">(2) Perdagangan sewaktu-waktu dapat dihentikan oleh pihak yang memiliki otoritas (Bappebti/Bursa Berjangka) tanpa pemberitahuan terlebih dahulu kepada Nasabah. Atas posisi terbuka yang masih dimiliki oleh Nasabah pada saat perdagangan tersebut dihentikan, maka akan diselesaikan (likuidasi) berdasarkan pada peraturan/ketentuan yang dikeluarkan dan ditetapkan oleh pihak otoritas tersebut, dan semua kerugian serta biaya yang timbul sebagai akibat dihentikannya transaksi oleh pihak otoritas perdagangan tersebut, menjadi beban dan tanggung jawab Nasabah sepenuhnya.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">11. Transaksi Harus Mematuhi Peraturan Yang Berlaku</div>
                    <p className="mt-2">Semua transaksi baik yang dilakukan sendiri oleh Nasabah maupun melalui Pialang Berjangka wajib mematuhi peraturan perundang-undangan di bidang Perdagangan Berjangka, kebiasaan dan interpretasi resmi yang ditetapkan oleh Bappebti atau Bursa Berjangka.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">12. Pialang Berjangka tidak Bertanggung Jawab atas Kegagalan Komunikasi</div>
                    <p className="mt-2">Pialang Berjangka tidak bertanggung jawab atas keterlambatan atau tidak tepat waktunya pengiriman amanat atau informasi lainnya yang disebabkan oleh kerusakan fasilitas komunikasi atau sebab lain di luar kontrol Pialang Berjangka.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">13. Konfirmasi</div>
                    <p className="mt-2">(1) Konfirmasi dari Nasabah dapat berupa surat, telex, media lain, secara tertulis ataupun rekaman suara.</p>
                    <p className="mt-2">(2) Pialang Berjangka berkewajiban menyampaikan konfirmasi transaksi, laporan akun, permintaan Call Margin, dan pemberitahuan lainnya kepada Nasabah secara akurat, benar dan secepatnya pada alamat Nasabah sesuai dengan yang tertera dalam akun Nasabah. Apabila dalam jangka waktu 2 x 24 jam setelah amanat jual atau beli disampaikan, tetapi Nasabah belum menerima konfirmasi tertulis, Nasabah segera memberitahukan hal tersebut kepada Pialang Berjangka melalui telepon dan disusul dengan pemberitahuan tertulis.</p>
                    <p className="mt-2">(3) Jika dalam waktu 2 x 24 jam sejak tanggal penerimaan konfirmasi tertulis tersebut tidak ada sanggahan dari Nasabah maka konfirmasi Pialang Berjangka dianggap benar dan sah.</p>
                    <p className="mt-2">(4) Kekeliruan atas konfirmasi yang diterbitkan Pialang Berjangka akan diperbaiki oleh Pialang Berjangka sesuai keadaan yang sebenarnya dan demi hukum konfirmasi yang lama batal.</p>
                    <p className="mt-2">(5) Nasabah tidak bertanggung jawab atas transaksi yang dilaksanakan atas akunnya apabila konfirmasi tersebut tidak disampaikan secara benar dan akurat.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">14. Kebenaran lnformasi Nasabah</div>
                    <p className="mt-2">Nasabah memberikan informasi yang benar dan akurat mengenai data Nasabah yang diminta oleh Pialang Berjangka dan akan memberitahukan paling lambat dalam waktu 3 (tiga) hari kerja setelah terjadi perubahan, termasuk perubahan kemampuan keuangannya untuk terus melaksanakan transaksi.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">15. Komisi Transaksi</div>
                    <p className="mt-2">Nasabah mengetahui dan menyetujui bahwa Pialang Berjangka berhak untuk memungut komisi atas transaksi yang telah dilaksanakan, dalam jumlah sebagaimana akan ditetapkan dari waktu ke waktu oleh Pialang Berjangka. Perubahan beban (fees) dan biaya lainnya harus disetujui secara tertulis oleh kedua belah Pihak.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">16. Pemberian Kuasa</div>
                    <p className="mt-2">(1) Nasabah memberikan kuasa kepada Pialang Berjangka untuk menghubungi bank, lembaga keuangan, Pialang Berjangka lain, atau institusi lain yang terkait untuk memperoleh keterangan atau verifikasi mengenai informasi yang diterima dari Nasabah. Nasabah mengerti bahwa investigasi mengenai data hutang pribadi dan bisnis dapat dilakukan oleh Pialang Berjangka apabila diperlukan. Nasabah diberikan kesempatan untuk memberitahukan secara tertulis dalam jangka waktu yang telah disepakati untuk melengkapi persyaratan yang diperlukan.</p>
                    <p className="mt-2">(2) Nasabah dapat memberikan kuasa kepada pihak lain (bukan Pengurus Pialang Berjangka, bukan Wakil Pialang Berjangka yang menandatangani Perjanjian Pemberian Amanat ini dan bukan pegawai Pialang Berjangka yang jabatannya satu tingkat di bawah Direksi) yang ditunjuk oleh Nasabah untuk menjalankan hak-hak yang timbul atas akun, termasuk memberikan instruksi kepada Pialang Berjangka atas akun yang dimiliki Nasabah, berdasarkan surat kuasa dalam bentuk dan isi sebagaimana terlampir.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">17. Komisi Transaksi</div>
                    <p className="mt-2">Pialang Berjangka dapat setiap saat mengalihkan dana dari satu akun ke akun lainnya sehubungan dengan kegiatan transaksi yang dilakukan Nasabah seperti Margin, pembayaran hutang, atau mengurangi defisit dalam akun Nasabah, tanpa terlebih dahulu memberitahukan kepada Nasabah. Transfer yang telah dilakukan akan segera diberitahukan secara tertulis kepada Nasabah.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">18. Pemberitahuan</div>
                    <p className="mt-2">(1) Semua komunikasi, uang, surat berharga, dan kekayaan lainnya harus dikirimkan langsung ke alamat Nasabah seperti tercantum dalam akunnya atau alamat lain yang ditetapkan/diberitahukan secara tertulis oleh Nasabah.</p>
                    <p className="mt-2">(2) Semua uang harus disetor atau ditransfer langsung oleh Nasabah ke Rekening Terpisah (Segregated Account) Pialang Berjangka:</p>
                    <div className="mt-2">
                        <div>Nama : PT Straits Futures Indonesia</div>
                        <div> Alamat : Gold Coast Office, Eiffel Tower, Level 1 Unit C, Jl. Pantai Indah Kapuk, Kel. Kamal Muara, Kec. Penjaringan, Jakarta 14470</div>
                        <div>No. Rekening Terpisah : Bank BCA KCU Sudirman 035-377-7111 (IDR) &amp; 035-327-1919 (USD) Bank CIMB Niaga Graha CIMB Niaga 800-1618-28000 (IDR) &amp; 800-1618-30940 (USD)</div>
                        <div>dan dianggap sudah diterima oleh Pialang Berjangka apabila sudah ada tanda terima bukti setor atau transfer dari pegawai Pialang Berjangka.</div>
                    </div>  
                    <p className="mt-2">(3) Semua surat berharga, kekayaan lainnya, atau komunikasi harus dikirim kepada Pialang Berjangka:</p>
                    <div className="mt-2">
                        <div>Nama : PT Straits Futures Indonesia</div>
                        <div> Alamat : Gold Coast Office, Eiffel Tower, Level 1 Unit C, Jl. Pantai Indah Kapuk, Kel. Kamal Muara, Kec. Penjaringan, Jakarta 14470</div>
                        <div>Telepon / Telephone : +62 21 5010 3599</div>
                        <div>E-mail : cs.id@straitsfinancial.com</div>
                        <div>dan dianggap sudah diterima oleh Pialang Berjangka apabila sudah ada tanda terima bukti setor atau transfer dari pegawai Pialang Berjangka.</div>
                    </div>  
                </div>
                <div className="mt-5">
                    <div className="font-bold">19. Dokumen Pemberitahuan Adanya Risiko</div>
                    <p className="mt-2">Nasabah mengakui menerima dan mengerti Dokumen Pemberitahuan Adanya Risiko.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">20. Jangka Waktu Perjanjian dan Pengakhiran</div>
                    <p className="mt-2">(1) Perjanjian Pemberian Amanat ini mulai berlaku terhitung sejak tanggal ditandatanganinya sampai disampaikannya pemberitahuan pengakhiran secara tertulis oleh Nasabah atau Pialang Berjangka.</p>
                    <p className="mt-2">(2) Nasabah dapat mengakhiri Perjanjian Pemberian Amanat ini hanya jika Nasabah sudah tidak lagi memiliki posisi terbuka dan tidak ada kewajiban Nasabah yang diemban oleh atau terhutang kepada Pialang Berjangka.</p>
                    <p className="mt-2">(3) Pengakhiran tidak membebaskan salah satu Pihak dari tanggung jawab atau kewajiban yang terjadi sebelum pemberitahuan tersebut.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">21. Perjanjian dapat berakhir dalam hal Nasabah:</div>
                    <p className="mt-2">(1) Dinyatakan pailit, memiliki hutang yang sangat besar, dalam proses peradilan, menjadi hilang ingatan, mengundurkan diri atau meninggal;</p>
                    <p className="mt-2">(2) Tidak dapat memenuhi atau mematuhi Perjanjian Pemberian Amanat ini dan/atau melakukan pelanggaran terhadapnya;</p>
                    <p className="mt-2">(3) Berkaitan dengan ayat (1) dan ayat (2) tersebut di atas, Pialang Berjangka dapat:</p>
                    <p className="mt-2">i. meneruskan atau menutup posisi Nasabah tersebut setelah mempertimbangkannya secara cermat dan jujur; dan</p>
                    <p className="mt-2">ii. menolak perintah dari Nasabah atau kuasanya.</p>
                    <p className="mt-2">(4) Pengakhiran perjanjian sebagaimana tersebut di atas tidak melepaskan kewajiban dari Para Pihak yang berhubungan dengan penerimaan atau kewajiban pembayaran atau pertanggungjawaban kewajiban lainnya yang timbul dari Perjanjian Pemberian Amanat ini.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">22. Force Majeur</div>
                    <p className="mt-2">Tidak ada satupun pihak di dalam Perjanjian Pemberian Amanat dapat diminta pertanggungjawabannya untuk suatu keterlambatan atau terhalangnya memenuhi kewajiban berdasarkan Perjanjian Pemberian Amanat yang diakibatkan oleh suatu sebab yang berada di luar kemampuannya atau kekuasaannya (force majeur), sepanjang pemberitahuan tertulis mengenai sebab itu disampaikannya kepada pihak lain dalam Perjanjian Pemberian Amanat ini dalam waktu tidak lebih dari 24 (dua puluh empat) jam sejak timbulnya sebab itu. Yang dimaksud dengan Force Majeur dalam perjanjian ini adalah peristiwa kebakaran, bencana alam (seperti gempa bumi, banjir, angin topan, petir), pemogokan umum, huru hara, peperangan, perubahan terhadap peraturan perundang-undangan yang berlaku dan kondisi di bidang ekonomi, keuangan dan Perdagangan Berjangka, pembatasan yang dilakukan oleh otoritas Perdagangan Berjangka dan Bursa Berjangka serta terganggunya sistem perdagangan, kliring dan penyelesaian transaksi Kontrak Berjangka di mana transaksi dilaksanakan yang secara langsung mempengaruhi pelaksanaan pekerjaan berdasarkan Perjanjian.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">23. Perubahan atas Isi Perjanjian Pemberian Amanat</div>
                    <p className="mt-2">Perubahan atas isi Perjanjian Pemberian Amanat ini hanya dapat dilakukan atas persetujuan Para Pihak, atau Pialang Berjangka telah memberitahukan secara tertulis perubahan yang diinginkan, dan Nasabah tetap memberikan perintah untuk transaksi dengan tanpa memberikan tanggapan secara tertulis atas usul perubahan tersebut. Tindakan Nasabah tersebut dianggap setuju atas usul perubahan tersebut.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">24. Penyelesaian Perselisihan</div>
                    <p className="mt-2">(1) Semua perselisihan dan perbedaan pendapat yang timbul dalam pelaksanaan Perjanjian Pemberian Amanat ini wajib diselesaikan terlebih dahulu secara musyawarah untuk mencapai mufakat antara Para Pihak.</p>
                    <p className="mt-2">(2) Apabila perselisihan dan perbedaan pendapat yang timbul tidak dapat diselesaikan secara musyawarah untuk mencapai mufakat, Para Pihak wajib memanfaatkan sarana penyelesaian perselisihan yang tersedia di Bursa Berjangka.</p>
                    <p className="mt-2">(3) Apabila perselisihan dan perbedaan pendapat yang timbul tidak dapat diselesaikan melalui cara sebagaimana dimaksud pada ayat (1) dan ayat (2), maka Para Pihak sepakat untuk menyelesaikan perselisihan melalui Badan Arbitrase Perdagangan Berjangka Komoditi (BAKTI) berdasarkan Peraturan dan Prosedur Badan Arbitrase Perdagangan Berjangka Komoditi (BAKTI) atau Pengadilan Negeri BAKTI.</p>
                </div>
                <div className="mt-5">
                    <div className="font-bold">25. Bahasa</div>
                    <p className="mt-2">Perjanjian Pemberian Amanat ini dibuat dalam Bahasa Indonesia.</p>
                </div>
                <p className="mt-5">Demikian Perjanjian Pemberian Amanat ini dibuat oleh Para Pihak dalam keadaan sadar, sehat jasmani rohani dan tanpa unsur paksaan dari pihak manapun.</p>
                <p className="mt-2">“Saya telah membaca, mengerti dan setuju terhadap semua ketentuan yang tercantum dalam Perjanjian Pemberian Amanat ini”.</p>
                <p className="mt-2">Dengan mengisi kolom “Ya” di bawah, saya menyatakan bahwa saya telah menerima “PERJANJIAN PEMBERIAN AMANAT TRANSAKSI KONTRAK BERJANGKA”, serta mengerti dan menyetujui isinya.</p>
                <p className="mt-5">Pernyataan menerima / Tidak</p>
                <p className="mt-2">Ya</p>
                <p className="mt-5">Pernyataan pada tanggal:</p>
                <p className="mt-2">{localState.curr_date.date} - {localState.curr_date.month} - {localState.curr_date.year}</p>     

                <div className="mt-10 grid grid-cols-2 gap-3">
                    <button className="button-secondary w-full"
                        onClick={()=>props.changePage(8)}>
                        Kembali
                    </button>
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>saveAccount()}>
                        Selanjutnya
                    </button>
                </div>
            </div>
        </>
    )
}
