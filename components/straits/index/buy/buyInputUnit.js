import React, {useState, useEffect} from "react";
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useUser } from 'lib/hooks'
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import { faDeleteLeft, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ErrorScreen from "components/errorScreen";

export default function BuyInputUnit(props) {
    useUser({ redirect_to: '/', redirect_if_not_found: true })

    const router = useRouter();
    const initialLocalState = () => {
        return {
            errorPageShow : false,
            loadingPage: true, 
            loadingSymbol: true,
            loadingProceed: false,
            account: {},
            symbol:{},
            value:"0",
            currencyValue:"0"
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getSymbol = async() => {
        let objLocalState = localState;
        objLocalState.loadingSymbol = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/symbol', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}&symbol=${router.query.index}`);
        }
        catch(error) {
            objLocalState.loadingSymbol = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingSymbol = false;
        if(response.code == "000") objLocalState.symbol = response.data.symbol;
        else objLocalState.errorPageShow = true;
        setLocalState({...objLocalState, ...localState});
    }
    const getAccount = async() => {
        setLocalState(initialLocalState);
        let objLocalState = localState;

        try {
            var response = await API.Get('/account/straits', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            if(response.data.straits_account.isactive == "1") {
                objLocalState.account = response.data.straits_account;
                objLocalState.loadingPage = false;    
            }
            else objLocalState.errorPageShow = true;
        }
        else objLocalState.errorPageShow = true;
        setLocalState({...objLocalState, ...localState});

        if (response.code == "000") getSymbol();
        return true;
    }
    const keyboardHandle = (val) => {
        let objLocalState = localState;  
        objLocalState.value = objLocalState.value.replace(/,/g, "");   
        if(val != "del") {
            objLocalState.value = (objLocalState.value == "0") ? val : objLocalState.value + val;
            if(objLocalState.value == "00") objLocalState.value = "0";
        }
        else {
            objLocalState.value = objLocalState.value.substring(0, objLocalState.value.length - 1);
            if(objLocalState.value == "") objLocalState.value = "0";
        }

        if(objLocalState.value.length > 6) objLocalState.value = objLocalState.value.substring(0, 6);
        objLocalState.currencyValue = (objLocalState.value * localState.symbol.contract_size * localState.symbol.price).toLocaleString();
        objLocalState.value = parseInt(objLocalState.value).toLocaleString();
        setLocalState({...objLocalState, ...localState});
    }
    const proceed = () => {
        props.changePage("confirmation", localState.value);
    }
    useEffect(() => {
        if(!router.isReady) return;
        getAccount();
    }, [router.isReady]);
    
    return (
        <>
            {localState.errorPageShow 
                ? 
                <ErrorScreen /> 
                :
                <>
                    {localState.loadingPage || localState.loadingSymbol 
                        ?
                        <LoadingPage />
                        :
                        <>
                            <div className="fixed bg-white w-full z-10">
                                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                                    <div className="text-left">
                                        <button type="button" 
                                            onClick={() => router.back()}>
                                            <Image src="/img/back.webp" 
                                                height="20"
                                                width="20"
                                                alt="back" />
                                        </button>
                                    </div>
                                    <div className="text-center subtitle1">
                                        {router.query.index}
                                    </div>
                                </div>
                            </div>

                            <div className="sm:container mx-auto p-4 custom-pt-70"
                                style={{paddingBottom:300}}>                        
                                <div className="mt-10 text-center">                        
                                    <div className="font-semibold">Saldo USD Tersedia USD {localState.account.balance}</div>
                                    <div className="mt-1">
                                        <Image src={localState.symbol.image} 
                                            height="12"
                                            width="12"
                                            alt={localState.symbol.name}
                                            className="rounded-full" />                            
                                        <span className="ml-1">1 {localState.symbol.symbol} = {localState.symbol.currency} {localState.symbol.price}</span>
                                    </div>
                                    <div className="mt-1">                                                            
                                        1 Unit = {localState.symbol.contract_size} Size Kontrak
                                    </div>
                                    <div className="mt-3 text-xs color-red">
                                        <FontAwesomeIcon icon={faInfoCircle} />
                                        <span className="ml-1">Harga tidak mengikat dan dapat berubah sewaktu-waktu</span>
                                    </div>
                                </div>
                                <div className="mt-20">
                                    <div className="text-center">UNIT</div>
                                    <div className="font-semibold text-center w-full text-4xl text-blue-600">{localState.value}</div>                           
                                </div>
                                <div className="mt-5 text-center">
                                    <span className="mr-1">{localState.symbol.currency}</span>
                                    <span>{localState.currencyValue}</span>                            
                                </div>                        
                            </div>
                            <div className="grid grid-cols-4 gap-2 bg-gray-2 p-2 fixed bottom-0 left-0 right-0">
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("1")}>1</button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("2")}>2</button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("3")}>3</button>
                                <button className="bg-white rounded py-1  drop-shadow"
                                    onClick={()=>keyboardHandle("del")}>
                                    <FontAwesomeIcon icon={faDeleteLeft} />
                                </button>

                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("4")}>4</button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("5")}>5</button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("6")}>6</button>                     
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    disabled="disabled"></button>

                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("7")}>7</button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("8")}>8</button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("9")}>9</button>   
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    disabled="disabled"></button>                                      

                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    disabled="disabled"></button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    onClick={()=>keyboardHandle("0")}>0</button>
                                <button className="text-3xl bg-white rounded py-1 drop-shadow"
                                    disabled="disabled"></button>
                                <button className="bg-white rounded py-1 font-semibold drop-shadow"
                                    onClick={()=>proceed()}>Done</button>
                            </div>             
                        </>
                    }
                </>
            } 
        </>
    )
}
