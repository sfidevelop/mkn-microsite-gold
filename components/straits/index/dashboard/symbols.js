import React, {useState, useEffect} from "react";
import Image from 'next/image';
import Link from 'next/link';
import * as API from "services/api";

export default function Symbols() {
    const initialLocalState = () => {
        return {
            loadingPage: true,
            isError: false,
            watchlists: [],
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getWatchlist = async() => {
        setLocalState(initialLocalState);

        let objLocalState = localState;
        try {
            var response = await API.Get('/account/straits/watchlists', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.isError = true;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") objLocalState.watchlists = response.data.watchlists;
        else objLocalState.isError = true;

        setLocalState({...objLocalState, ...localState});
        return true;
    }
    useEffect(() => {
        getWatchlist();
    }, []);
    
    return (
        <>
            {localState.isError && 
                <div className="text-center p-4">
                    <Image src="/img/error.png"
                        alt="error"
                        height={30}
                        width={30} />
                    <div className='mt-2 text-xs color-red'>
                        Gagal mengambil data
                    </div>                    
                </div>
            }

            {localState.loadingPage 
                ?
                <div className="text-center p-4">
                    <Image src="/img/loading.png"
                        alt="loading"
                        height={30}
                        width={30}
                        className="animate-spin" />
                    <div className='mt-2 text-xs'>
                        Loading
                    </div>
                </div>
                :
                <div className="sm:container mx-auto p-4">
                    <div className="refreshPrice"><button className="buttonClass" onClick={() => {
                        getWatchlist()
                    }}>Refresh Harga</button></div>
                    {localState.watchlists.map((group, index_group) => {
                        return (
                            <div className="mb-4"
                                key={index_group}>
                                <div className="font-bold">
                                    {group.group}
                                </div>
                                {group.tickers.map((item, index) => {
                                    return (
                                        <Link href={{
                                            pathname: '/logined/index/symbol/[symbol]',
                                            query: { symbol: item.symbol },
                                        }}
                                        key={index}>
                                            <a className="block">
                                                <div className="grid grid-cols-2 gap-3 py-3">
                                                    <div className="flex">
                                                        <div className="mr-2 self-center">
                                                            <Image src={item.image} 
                                                                height="24"
                                                                width="24"
                                                                alt={item.symbol}
                                                                className="rounded-full" />
                                                        </div>
                                                        <div>
                                                            <div className="font-semibold">{item.name}</div>
                                                            <div>{item.symbol}</div>
                                                        </div>
                                                    </div>
                                                    <div className="text-right">
                                                        <div className="font-semibold">
                                                            <span className="mr-1 color-gray">{item.currency}</span>
                                                            { group.group == "Mata Uang" ? 
                                                        <span className={(item.change >= 0 ? "color-green" : "color-red")}>{item.price}</span>
                                                        :
                                                        <span className={(item.change >= 0 ? "color-green" : "color-red")}>{parseFloat(item.price).toFixed(2)}</span>    
                                                        }
                                                            
                                                        </div>
                                                        <div className={(item.change >= 0 ? "color-green" : "color-red")}>
                                                        { group.group == "Mata Uang" ? 
                                                        <span className={(item.change >= 0 ? "color-green" : "color-red")}>{parseFloat(item.change).toFixed(4)}</span>
                                                        :
                                                        <span className={(item.change >= 0 ? "color-green" : "color-red")}>{parseFloat(item.change).toFixed(2)}</span>    
                                                        }
                                                            <span>({parseFloat(item.change_percent).toFixed(2)}%)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </Link>
                                    )
                                })}
                            </div>                            
                        )
                    })}
                </div>
            }
        </>
    )
}
