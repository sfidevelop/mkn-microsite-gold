import React, {useState, useEffect} from "react";
import Image from 'next/image';
import Link from 'next/link';
import * as API from "services/api";

export default function Portfolios() {
    const initialLocalState = () => {
        return {
            loadingPage: true,
            isError: false,
            portfolios: []
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getPortfolio = async(range = "1") => {
        setLocalState(initialLocalState);

        let objLocalState = localState;
        try {
            var response = await API.Get('/account/straits/transaction/portfolio', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            objLocalState.isError = true;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") objLocalState.portfolios = response.data.transactions;
        else objLocalState.isError = true;

        setLocalState({...objLocalState, ...localState});
        return true;
    }
    useEffect(() => {
        getPortfolio();
    }, []);
    
    return (
        <>
            {localState.isError && 
                <div className="text-center p-4">
                    <Image src="/img/error.png"
                        alt="error"
                        height={30}
                        width={30} />
                    <div className='mt-2 text-xs color-red'>
                        Gagal mengambil data
                    </div>                    
                </div>
            }

            {localState.loadingPage 
                ?
                <div className="text-center p-4">
                    <Image src="/img/loading.png"
                        alt="loading"
                        height={30}
                        width={30}
                        className="animate-spin" />
                    <div className='mt-2 text-xs'>
                        Loading
                    </div>
                </div>
                :
                <div className="bg-gray-2 sm:container mx-auto p-4">
                    <div className="grid grid-cols-2">
                        <div className="enlarge-title-portofolio">
                            Portfolio
                        </div>
                        {localState.portfolios.length > 0 &&
                            <div className="text-right self-center text-blue-600">
                                <Link href="/logined/index/portfolio">
                                    <a>
                                        Lihat
                                    </a>
                                </Link>
                            </div>
                        }
                    </div>

                    {localState.portfolios.length > 0
                        ?
                        <div className="mt-3 grid grid-cols-3 gap-3">                        
                            {localState.portfolios.map((item,index) => {
                                return (
                                    <Link href={{
                                            pathname: '/logined/index/symbol/[symbol]',
                                            query: { symbol: item.index.symbol },
                                        }}
                                        key={index}>
                                        <a className="bg-white rounded p-3 block drop-shadow">
                                            <div className="mb-2 flex">
                                                <div className="mr-2">
                                                    <Image src={item.index.image}
                                                        height="20"
                                                        width="20"
                                                        className="rounded-full"
                                                        alt={item.index.symbol}
                                                        title={item.index.symbol} />
                                                </div>
                                                <div>
                                                    <div className="font-semibold">{item.index.symbol}</div>
                                                    {/* <div>{item.change_percent}%</div> */}
                                                </div>                                
                                            </div>
                                            <div className={"text-white text-center rounded-full mb-3 py-1 " + (item.change <= "0" ? "bg-color-loss" : "bg-green-7")}>
                                                {(item.change * item.volume * item.ContractSize).toFixed(2).toLocaleString()}
                                            </div>
                                            <div className="grid grid-cols-2">
                                                <div>Unit</div>
                                                <div className="text-right small-items">{item.volume}</div>
                                            </div>
                                            <div className="grid grid-cols-2">
                                                <div>Beli</div>
                                                <div className="text-right small-items">{item.index.index_group_id == 2 ? Number(item.open_price).toFixed(4):Number(item.open_price).toFixed(2)}</div>
                                            </div>
                                            <div className="grid grid-cols-2">
                                                <div>Jual</div>
                                                <div className="text-right small-items">{item.index.index_group_id == 2 ? Number(item.index.price_bid).toFixed(4) : Number(item.index.price_bid).toFixed(2)}</div>
                                            </div>
                                        </a>
                                    </Link>
                                )
                            })}                        
                        </div>
                        :
                        <div className="mt-3">
                            Kamu belum mempunyai portofolio, Siapkan portofoliomu sekarang.
                        </div>
                    }
                </div>
            }
        </>
    )
}
