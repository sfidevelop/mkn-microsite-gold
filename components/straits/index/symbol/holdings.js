import React, {useState, useEffect} from "react";
import Image from 'next/image';
import * as API from "services/api";
import { useRouter } from 'next/router';

export default function Holdings(props) {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            isError: false,
            portfolios:[],
            activePortfolio:"",
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getHoldings = async() => {
        setLocalState(initialLocalState);

        let objLocalState = localState;
        try {
            var response = await API.Get('/account/straits/transaction/portfolio', 
            `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}&symbol=${router.query.index}`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") objLocalState.portfolios = response.data.transactions;
        else objLocalState.errorPageShow = true;
        setLocalState({...objLocalState, ...localState});
        return true;
    }
    
    useEffect(() => {
        getHoldings();
    }, [router.isReady]);
    
    return (
        <>
            {localState.isError && 
                <div className="text-center p-4">
                    <Image src="/img/error.png"
                        alt="error"
                        height={30}
                        width={30} />
                    <div className='mt-2 text-xs color-red'>
                        Gagal mengambil data
                    </div>                    
                </div>
            }

            {localState.loadingPage 
                ?
                <div className="text-center p-4">
                    <Image src="/img/loading.png"
                        alt="loading"
                        height={30}
                        width={30}
                        className="animate-spin" />
                    <div className='mt-2 text-xs'>
                        Loading
                    </div>
                </div>
                :
                <>
                    {localState.portfolios.length > 0 &&
                        <div className="bg-gray-2 rounded drop-shadow">
                            <div className="mb-1 font-epilogue font-medium text-sm text-black p-4">
                                <div className="text-xl font-bold">Your Holding</div>
                                <div className="mt-2"><sup className="text-base font-medium">KLIK posisi untuk menjual</sup></div>
                            </div>
                            <div className="divide-y">
                                {localState.portfolios.map((item,index) => {
                                    return (
                                        <button className={"py-3 px-4 block w-full border-white border-4 rounded transition duration-500" + (item.code == props.activePortfolio ? " bg-green-2" : "")}
                                            key={index}
                                            onClick={()=>props.toggleActivePortfolio(item.code)}>
                                            <div className="grid grid-cols-2">
                                                <div className="text-left">
                                                    <div>Beli</div>
                                                    <div>Value P&L</div>
                                                </div>
                                                <div className="text-right">
                                                    <div className="font-semibold">{item.index.currency} {(item.open_price * item.volume * item.ContractSize).toFixed(4)}</div>
                                                    <div className={"font-semibold " + (item.change <= 0 ? "color-red" : "color-green")}>
                                                        <span className="mr-1">{(item.change * item.volume  * item.ContractSize).toFixed(2)}</span>
                                                        <span>({Number(item.change_percent).toFixed(2)}%)</span>

                                                    </div>
                                                </div>                                        
                                            </div>                                        
                                        </button>
                                    )
                                })}                        
                            </div> 
                        </div>
                    }
                </>
            }
        </>
    )
}
