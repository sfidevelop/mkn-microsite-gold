import React, {useState, useEffect} from "react";
import Image from 'next/image';
import * as API from "services/api";
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';

export default function Chart() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            isError: false,
            charts:[]
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getChart = async() => {
        setLocalState(initialLocalState);
        let objLocalState = localState;

        try {
            var response = await API.Get('/account/straits/symbol/chart', 
                `code=${process.env.NEXT_PUBLIC_CODE}&client_code=${process.env.NEXT_PUBLIC_CLIENTKEY}&token=${localStorage.getItem("logined_token")}&symbol=${router.query.index}`);
        }
        catch(error) {
            objLocalState.loadingPage = false;
            objLocalState.errorPageShow = true;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        objLocalState.loadingPage = false;
        if(response.code == "000") {
            objLocalState.charts = response.data.charts;
            setLocalState({...objLocalState, ...localState});            
        }
        else objLocalState.errorPageShow = true;
        setLocalState({...objLocalState, ...localState});
        return true;
    }
    const ContainerChart = () => {
        return (
            <>
                {!localState.loadingPage && <CandleStickChart data={localState.charts} />}
            </>
        );
    };
    const CandleStickChart = dynamic(
        () => import('components/chart'),
        { ssr: false }
    );
    useEffect(() => {
        getChart();
    }, [router.isReady]);
    
    return (
        <>
            {localState.isError && 
                <div className="text-center p-4">
                    <Image src="/img/error.png"
                        alt="error"
                        height={30}
                        width={30} />
                    <div className='mt-2 text-xs color-red'>
                        Gagal mengambil data
                    </div>                    
                </div>
            }

            {localState.loadingPage 
                ?
                <div className="text-center p-4">
                    <Image src="/img/loading.png"
                        alt="loading"
                        height={30}
                        width={30}
                        className="animate-spin" />
                    <div className='mt-2 text-xs'>
                        Loading
                    </div>
                </div>
                :
                <div style={{height:300}}>   
                    <ContainerChart />                                 
                </div>
            }
        </>
    )
}
