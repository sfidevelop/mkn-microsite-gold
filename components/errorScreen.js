import React from "react";
import Link from 'next/link'
import Image from 'next/image'

export default function ErrorScreen() {
    return (
        <div className="grid place-items-center h-screen">
            <div className="sm:container mx-auto p-4">
                <div className="text-center">
                    <Image src="/img/maintenance.webp"
                        alt="maintenance"
                        height={300}
                        width={300} />
                </div>
                <div className="text-center subtitle1">
                    Maaf, Halaman ini sedang dalam perbaikan
                </div>
                <div className="text-center subtitle2">
                    Harap hubungi customer service kami untuk informasi lebih lanjut
                </div>
                <div className="text-center mt-10">
                    <Link href="/logined/dashboard">
                        <a className="button-primary block">
                            Kembali
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    )
}